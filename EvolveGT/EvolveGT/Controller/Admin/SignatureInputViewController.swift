//
//  SignatureInputViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 10/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import SwiftSignatureView

class SignatureInputViewController: UIViewController {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var canvasView: SwiftSignatureView!
    @IBOutlet weak var termsCheckBox: EVCheckBox!
    var eventUser: AdminEventUser?
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCanvas()
        addTitle(with: "Signature")
        termsCheckBox.delegate = self
        saveButton.isEnabled = false
        saveButton.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
    }
    
    private func setUpCanvas() {
        canvasView.delegate = self
        canvasView.layer.borderColor = UIColor.black.cgColor
        canvasView.layer.borderWidth = 2.0
        canvasView.layer.cornerRadius = 5.0
    }

    private func saveSignature() {
        guard let signature = canvasView.signature,
            let signatureID = eventUser?.signId,
            isNetWorkAvailable else { return }
        addLoadingIndicator()
        let serviceManager = AdminServiceManager()
        serviceManager.managerDelegate = self
        serviceManager.saveSignatureForEvent(sign: signature, for: signatureID)
    }
    
    @IBAction func clearInputAction(_ sender: UIButton) {
        canvasView.clear()
    }
    
    @IBAction func saveInputAction(_ sender: UIButton) {
        saveSignature()
    }
    
    @IBAction func close(_ sender: UIButton) {
        navigationController?.popViewController(animated: false)
    }
    @IBAction func termsCheckBoxAction(_ sender: UIButton) {
        
    }
    
    @objc func dismissAlert() {
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func presentAlertForSignatureSaveAction(isSuccess: Bool) {
        let title = isSuccess ? "Success" : "Failed"
        let message = isSuccess ? "Your signature is saved successfully": "Something went wrong"
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        self.perform(#selector(dismissAlert), with: nil, afterDelay: 2)
    }
}

extension SignatureInputViewController: WebServiceTaskManagerProtocol {
    func didFinishTask(from manager: AnyObject, response: (data: RestResponse?, error: String?)) {
        removeLoadingIndicator()
        if response.error != nil {
            self.showErrorAlert(message: response.data?.message ?? " ")
        } else {
            if let data = response.data?.responseModel as? UserSignature {
                presentAlertForSignatureSaveAction(isSuccess: data.status == 1)
            }
        }
    }
}

extension SignatureInputViewController: SwiftSignatureViewDelegate {
    func swiftSignatureViewDidPanInside(_ view: SwiftSignatureView, _ pan: UIPanGestureRecognizer) {
        
    }
    
    func swiftSignatureViewDidTapInside(_ view: SwiftSignatureView) {
        //
    }
    
    func swiftSignatureViewDidPanInside(_ view: SwiftSignatureView) {
        //
    }
    
}

extension SignatureInputViewController: EVCheckBoxDelegate {
    func tappedOnBox(checkBox: EVCheckBox, selected: Bool) {
        saveButton.isEnabled = selected
        saveButton.backgroundColor = saveButton.isEnabled ? #colorLiteral(red: 0.03021821566, green: 0.6054252386, blue: 0.2137703896, alpha: 1) : #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
    }
}
