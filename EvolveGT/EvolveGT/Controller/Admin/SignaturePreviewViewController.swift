//
//  SignaturePreviewViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 13/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class SignaturePreviewViewController: UIViewController {

    @IBOutlet weak var singaturePreviewImgVIew: UIImageView!
    var image: UIImage?
    var eventUser: AdminEventUser?
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSignPreview()
        addTitle(with: "Signature")
    }
    
    private func setUpSignPreview() {
        singaturePreviewImgVIew.layer.borderColor = UIColor.gray.cgColor
        singaturePreviewImgVIew.layer.borderWidth = 1.0
        singaturePreviewImgVIew.image = image
    }

    @IBAction func closePreview(_ sender: UIButton) {
        navigationController?.popViewController(animated: false)
    }
}
