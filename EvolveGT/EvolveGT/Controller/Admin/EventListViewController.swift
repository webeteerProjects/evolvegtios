

//
//  EventListViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 09/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher
import RSSelectionMenu

let heightOfHeader: CGFloat = 50

class EventListViewController: UITableViewController {
//    @IBOutlet weak var tableView: UITableView!
    var eventsData : EventListRes?
    var eventList : [Result]?
    var initialArrayOfEvents: [Result]?
    var searchController: UISearchController?
    var selectedEvent: Result?
    var searchBar: UISearchBar?
    var isSearchActive: Bool = false
    var isDismissingSortView = false

    enum FilterType {
        case trainingType
        case month
        case eventType
        case none
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        getEventsListata()
        customiseNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "",
                                                           style: .plain,
                                                           target: nil,
                                                           action: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isDismissingSortView{
            isDismissingSortView = false
        }else{

        }
        
    }
        
    func customiseNavigationBar() {
        
        addTitle(with: "Events")
        let logoutItem = UIBarButtonItem(image: #imageLiteral(resourceName: "logout_icon"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(EventListViewController.logout))
        let filterItem = UIBarButtonItem(image: #imageLiteral(resourceName: "filter"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(EventListViewController.tappedOnFilterItems))
        let switchItem = UIBarButtonItem(image: UIImage(named:"SwictUserWhite"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(EventListViewController.tappedOnSwitchItem))

        let searchItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Search"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(EventListViewController.searchEvents))
        
        self.navigationItem.rightBarButtonItems = [logoutItem, filterItem,switchItem, searchItem]
    }
    
    
    @objc func logout(){
        UserInfo.currentUser()?.clearSession()
        AppSharedData.sharedInstance.isLoggedIn = false
        AppSharedData.sharedInstance.userDetails = nil
        AppSharedData.sharedInstance.isCurrentSwitchModuleUser = false
        AppSharedData.sharedInstance.isCurrentSwitchModuleAdmin = false
        let storboard = UIStoryboard.init(name: "Main", bundle: nil)
        let navVC = storboard.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = navVC;
    }
    
    @objc func tappedOnSwitchItem() {
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.showUserView()
    }

    
    
    @objc func tappedOnFilterItems() {
        let filterActionSheet = UIAlertController(title: "Select filter", message: "", preferredStyle: .actionSheet)
        let month         = UIAlertAction(title: "By Month", style: .default, handler: { _ in
            self.filterItems(with: .month)
        })
        let eventType     = UIAlertAction(title: "By Event Type", style: .default, handler: { _ in
            self.filterItems(with: .eventType)
        })
        let trainingType  = UIAlertAction(title: "By Training Type", style: .default, handler: { _ in
            self.filterItems(with: .trainingType)
        })
        let clear         = UIAlertAction(title: "Clear", style: .cancel, handler: { _ in
            self.filterItems(with: .none)
        })
        filterActionSheet.addAction(month)
        filterActionSheet.addAction(eventType)
        filterActionSheet.addAction(trainingType)
        filterActionSheet.addAction(clear)
        if Constant.DeviceType.IS_IPAD {
            filterActionSheet.popoverPresentationController?.permittedArrowDirections = []
            filterActionSheet.popoverPresentationController?.sourceView = self.view
            filterActionSheet.popoverPresentationController?.sourceRect = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH / 2, y: Constant.ScreenSize.SCREEN_HEIGHT, width: 1.0, height: 1.0)
        }
        present(filterActionSheet, animated: true, completion: nil)
    }
    
    func filterItems(with filterType: FilterType) {
        switch filterType {
        case .trainingType:
//            eventList = initialArrayOfEvents
            let simpleSelectedArray = [String]()
            let simpleDataArray = initialArrayOfEvents!.compactMap { $0.trainingType }
            var allItems = Array(Set(simpleDataArray.flatMap { $0 }))
            allItems = allItems.sorted(by: <)
            let selectionMenu = RSSelectionMenu(dataSource: allItems) { (cell, item, indexPath) in
                cell.textLabel?.text = item
            }
            selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
                self?.eventList = self?.initialArrayOfEvents?.filter { $0.trainingType?.contains(selectedItems.first ?? "") ?? false }
                self?.tableView.reloadData()
            }
            selectionMenu.maxSelectionLimit = 1
            selectionMenu.cellSelectionStyle = .checkbox
            selectionMenu.title = "Filter by Training Type"
            self.isDismissingSortView = true
            selectionMenu.show(style: .present, from: self)
        case .month:
//            eventList = initialArrayOfEvents
            let simpleSelectedArray = [String]()
            let monthSortArray = initialArrayOfEvents?.sorted(by: { $0.monthValue > $1.monthValue })

            var simpleDataArray = Array(Set(monthSortArray!.compactMap { $0.monthYear }))
            simpleDataArray = simpleDataArray.sorted(by: <)
            let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
                cell.textLabel?.text = item
            }
            selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
                self?.eventList = self?.initialArrayOfEvents?.filter { $0.monthYear == selectedItems.first }
                self?.tableView.reloadData()
            }
            selectionMenu.maxSelectionLimit = 1
            selectionMenu.cellSelectionStyle = .checkbox
            selectionMenu.title = "Filter by Month"
            self.isDismissingSortView = true
            selectionMenu.show(style: .present, from: self)
            
        case .eventType:
//            eventList = initialArrayOfEvents
            let simpleSelectedArray = [String]()
            var simpleDataArray = Array(Set(initialArrayOfEvents!.compactMap { $0.eventType }))
            simpleDataArray = simpleDataArray.sorted(by: <)
            let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
                cell.textLabel?.text = item
            }
            selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
                self?.eventList = self?.initialArrayOfEvents?.filter { $0.eventType == selectedItems.first }
                self?.tableView.reloadData()
            }
            selectionMenu.maxSelectionLimit = 1
            selectionMenu.cellSelectionStyle = .checkbox
            selectionMenu.title = "Filter by Event Type"
            self.isDismissingSortView = true
            selectionMenu.show(style: .present, from: self)
        case .none:
            eventList = initialArrayOfEvents
            tableView.reloadData()
        }
    }
    
    @objc func searchEvents() {
        isSearchActive = !isSearchActive
        if isSearchActive{
            
        }else{
            self.searchBar?.resignFirstResponder()
            eventList = initialArrayOfEvents
        }
        tableView.reloadData()
    }
    
    func getEventsListata(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = EventServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getEventList()
        }
    }

    func showListData(){
        self.tableView.reloadData()
    }
}

extension EventListViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.eventList?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"EventListTableViewCell",
                                                 for: indexPath) as! EventListTableViewCell
        cell.titleLbl.text = self.eventList?[indexPath.row].title
        let dateStr = self.eventList?[indexPath.row].eventDateDMY ?? ""
        cell.dateLbl.text =  "Event Date: " + dateStr
        if let imgUrl = self.eventList?[indexPath.row].eventLogo {
            cell.eventImage.kf.setImage(with: URL(string : imgUrl),
                                        placeholder: nil,
                                        options: [.transition(ImageTransition.fade(1))])
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let event = eventList?[indexPath.row] else { return }
        selectedEvent = event
        let eventUserListVC = self.storyboard?.instantiateViewController(withIdentifier: "EventAdminDetailViewController") as? EventAdminDetailViewController
        eventUserListVC?.event = selectedEvent
        navigationController?.pushViewController(eventUserListVC!, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isSearchActive {
            searchBar = UISearchBar()
            searchBar?.delegate = self
            searchBar?.placeholder = "Search events here"
            searchBar?.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60)
            searchBar?.becomeFirstResponder()
            searchBar?.showsCancelButton = true
            return searchBar
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return isSearchActive ? 60 : 0
    }
}
extension EventListViewController: WebServiceTaskManagerProtocol {
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
        } else {
            if let data = response.data?.responseModel as? EventListRes {
                self.eventsData =  data
                self.eventList = self.eventsData?.results
                initialArrayOfEvents = self.eventsData?.results
                showListData()
            }
        }
    }
}

extension EventListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isBlank {
            eventList = initialArrayOfEvents
            tableView.reloadData()
        } else {
            eventList = initialArrayOfEvents?.filter { $0.title.lowercased().starts(with: searchText.lowercased()) }
        }
        tableView.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        searchBar.text = ""
        self.searchBar?.resignFirstResponder()
        eventList = initialArrayOfEvents
        tableView.reloadData()
    }
}

extension EventListViewController {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
      //  searchBar?.resignFirstResponder()
    }
}
