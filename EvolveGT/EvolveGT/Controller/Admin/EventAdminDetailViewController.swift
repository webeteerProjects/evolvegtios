//
//  EventAdminDetailViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 10/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher
import RSSelectionMenu

class EventAdminDetailViewController: UIViewController {

    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var eventDateLabel: UILabel!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    var isDismissingSortView = false
    var isNeedToShowTrainingSort = false
    var isNeedToShowRentalSort = false
    
    var isCallingFromSetupView = true

    var event: Result?
    var eventUsersList: [AdminEventUser]?
    var initialArrayOfUsersList: [AdminEventUser]?
    var selectedIndex = 0
    enum FilterType {
        case rental
        case training
        case skillLevel
        case none
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isDismissingSortView{
            isDismissingSortView = false
        }else{
            getUsersListForEvent(with: event)
        }
    }
    
    private func setUpUI() {
        searchBar.showsCancelButton = false
        emptyMessageLabel.text = "There are currently no users enrolled for this event"
        emptyMessageLabel.isHidden = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 177
        eventTitleLabel.text = event?.title
        if let imgUrl = event?.eventLogo {
            eventImageView.kf.setImage(with: URL(string : imgUrl),
                                       options: [.transition(ImageTransition.fade(1))])
        }
        
        eventDateLabel.text = event?.eventDateDMY
        
        customiseNavigationBar()
        searchEvents()
    }
    
    func customiseNavigationBar() {
        
        addTitle(with: "Users")
        let filterItem = UIBarButtonItem(image: #imageLiteral(resourceName: "filter"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(EventAdminDetailViewController.tappedOnFilterItems))
        let searchItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Search"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(EventAdminDetailViewController.searchEvents))
        
        self.navigationItem.rightBarButtonItems = [filterItem, searchItem]
    }
    
    func getUsersListForEvent(with event: Result?) {
        guard isNetWorkAvailable, let event = event else { return }
        addLoadingIndicator()
        let serviceManager = EventServiceManager()
        serviceManager.managerDelegate = self
        serviceManager.getUsersListForEventToAdmin(for: event)
    }

    @objc func tappedOnFilterItems() {
        if initialArrayOfUsersList == nil{
            return
        }
        let filterActionSheet = UIAlertController(title: "Select filter", message: "", preferredStyle: .actionSheet)
        let month         = UIAlertAction(title: "By Skill Level", style: .default, handler: { _ in
            self.filterItems(with: .skillLevel)
        })
        let eventType     = UIAlertAction(title: "By Training", style: .default, handler: { _ in
            self.filterItems(with: .training)
        })
        let rental     = UIAlertAction(title: "By Rental", style: .default, handler: { _ in
            self.filterItems(with: .rental)
        })

        let clear         = UIAlertAction(title: "Clear", style: .cancel, handler: { _ in
            self.filterItems(with: .none)
        })
        var isNeedtoshowFilter = false
        if isNeedToShowTrainingSort {
            isNeedtoshowFilter = true
            filterActionSheet.addAction(eventType)
        }
        let allRentalItems = initialArrayOfUsersList!.compactMap { $0.rentals }
        let allRentalArray = allRentalItems.flatMap { $0 }
        let arrayCount = allRentalArray.count
        if arrayCount > 0{
            isNeedtoshowFilter = true

            filterActionSheet.addAction(rental)
        }
        let simpleDataArray = Array(Set(initialArrayOfUsersList!.compactMap { $0.skillLevel }))
        if simpleDataArray.count > 0{
            isNeedtoshowFilter = true
            filterActionSheet.addAction(month)
        }
        filterActionSheet.addAction(clear)
        if isNeedtoshowFilter == true{
            if Constant.DeviceType.IS_IPAD {
                filterActionSheet.popoverPresentationController?.permittedArrowDirections = []
                filterActionSheet.popoverPresentationController?.sourceView = self.view
                filterActionSheet.popoverPresentationController?.sourceRect = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH / 2, y: Constant.ScreenSize.SCREEN_HEIGHT, width: 1.0, height: 1.0)
            }
            present(filterActionSheet, animated: true, completion: nil)
        }
    }
    
    func filterItems(with filterType: FilterType) {
        switch filterType {
        case .training:
//            eventUsersList = initialArrayOfUsersList
            let simpleDataArray = initialArrayOfUsersList!.compactMap { $0.training }
            var allItems = Array(Set(simpleDataArray.flatMap { $0 }))
            allItems = allItems.sorted(by: <)

            let simpleSelectedArray = [String]()

            let selectionMenu = RSSelectionMenu(dataSource: allItems) { (cell, item, indexPath) in
                cell.textLabel?.text = item
            }
            selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
                self?.eventUsersList = self?.initialArrayOfUsersList?.filter { $0.training?.contains(selectedItems.first ?? "") ?? false }
                self?.tableView.reloadData()
            }
            selectionMenu.maxSelectionLimit = 1
            selectionMenu.cellSelectionStyle = .checkbox
            selectionMenu.title = "Filter by Training Type"
            self.isDismissingSortView = true
            selectionMenu.show(style: .present, from: self)
        case .skillLevel:
//            eventUsersList = initialArrayOfUsersList
            var simpleDataArray = Array(Set(initialArrayOfUsersList!.compactMap { $0.skillLevel }))
            simpleDataArray = simpleDataArray.sorted(by: <)
            let simpleSelectedArray = [String]()
            let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
                cell.textLabel?.text = item
            }
            selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
                self?.eventUsersList = self?.initialArrayOfUsersList?.filter { $0.skillLevel?.contains(selectedItems.first ?? "") ?? false }
                self?.tableView.reloadData()
            }
            selectionMenu.maxSelectionLimit = 1
            selectionMenu.cellSelectionStyle = .checkbox
            selectionMenu.title = "Filter by Training Type"
            self.isDismissingSortView = true
            selectionMenu.show(style: .present, from: self)
        case .none:
            eventUsersList = initialArrayOfUsersList
            tableView.reloadData()
            handleEmptyMessage()
        case .rental:
            let allRentalItems = initialArrayOfUsersList!.compactMap { $0.rentals }
            let allRentalArray = allRentalItems.flatMap { $0 }
            var simpleDataArray = Array(Set(allRentalArray.compactMap { $0.name }))
            simpleDataArray = simpleDataArray.sorted(by: <)
            let simpleSelectedArray = [String]()
            let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
                cell.textLabel?.text = item
            }
            selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
                self?.eventUsersList = self?.initialArrayOfUsersList?.filter { $0.skillLevel?.contains(selectedItems.first ?? "") ?? false }
                self?.tableView.reloadData()
            }
            selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
                self?.eventUsersList = self?.initialArrayOfUsersList?.filter { $0.rentals.contains(where: { $0.name == selectedItems.first}) }
                self?.tableView.reloadData()
            }
            selectionMenu.maxSelectionLimit = 1
            selectionMenu.cellSelectionStyle = .checkbox
            selectionMenu.title = "Filter by Rental"
            self.isDismissingSortView = true
            selectionMenu.show(style: .present, from: self)
        }
    }
    
    @objc func searchEvents() {
        searchBarTopConstraint.constant = searchBarTopConstraint.constant == 0 ? -56 : 0
        view.setNeedsLayout()
        if isCallingFromSetupView{
            isCallingFromSetupView = false
        }else{
            if searchBarTopConstraint.constant == -56{
                searchBar.resignFirstResponder()
            }else{
                searchBar.becomeFirstResponder()
            }
        }

    }

    func handleEmptyMessage() {
        if eventUsersList?.isEmpty ?? true {
            emptyMessageLabel.isHidden = false
            view.bringSubviewToFront(emptyMessageLabel)
        } else {
            emptyMessageLabel.isHidden = true
        }
    }
    
    func getAdminSignature() {
        guard isNetWorkAvailable else { return }
        addLoadingIndicator()
        let serviceManager = AdminServiceManager()
        serviceManager.managerDelegate = self
        serviceManager.getSignatureForAdmin(with: eventUsersList?[selectedIndex].signId ?? "")
    }
}

extension EventAdminDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        handleEmptyMessage()
        return eventUsersList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventParticipantTableViewCell",
                                                 for: indexPath) as! EventParticipantTableViewCell
        cell.delegate = self
        cell.indePathForRef = indexPath
        cell.eventUser = eventUsersList?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 177
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let count = eventUsersList?.count ?? 0
        if count > 0 {
            return "Showing \(count) users"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let count = eventUsersList?.count ?? 0
        if count > 0 {
            return 30
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.backgroundView?.backgroundColor = .white
    }
}

extension EventAdminDetailViewController: EventParticipantDelegate {
    func clickedOnActionItemForSign(_ cell: EventParticipantTableViewCell, event: AdminEventUser?) {
        guard let event = event else { return }
        
        selectedIndex = cell.indePathForRef.row
        let storyboard = UIStoryboard(name: "Admin", bundle: nil)
        if event.hasSign ?? false {
            getAdminSignature()
        } else {
            let controller = storyboard.instantiateViewController(withIdentifier: "SignatureInputViewController") as! SignatureInputViewController
            controller.eventUser = event
            navigationController?.pushViewController(controller,
                                                     animated: true)
        }
    }
    
    func clickedOnActionItemForTraining(_ cell: EventParticipantTableViewCell, trainingInfo: [String]) {
        let allTrainingInfo = trainingInfo.joined(separator: "\n")
        let alertController = UIAlertController(title: "Training info",
                                                message: allTrainingInfo,
                                                preferredStyle: .alert)
        let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func clickedOnUpgradeSkill(_ cell: EventParticipantTableViewCell, event: AdminEventUser?){
        self.showUpdateSkillLevel(user: event)
    }

}

extension EventAdminDetailViewController: WebServiceTaskManagerProtocol {
    func didFinishTask(from manager: AnyObject, response: (data: RestResponse?, error: String?)) {
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
        } else {
            if let data = response.data?.responseModel as? UserSignature {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "SignaturePreviewViewController") as! SignaturePreviewViewController
                controller.image = data.signatureAsImage
                navigationController?.pushViewController(controller, animated: true)
            } else if let data = response.data?.responseModel as? AdminEventUserList {
                self.eventUsersList = data.results.sorted { $0.displayName < $1.displayName }
                self.initialArrayOfUsersList = data.results.sorted { $0.displayName < $1.displayName }

                setupSortOption()
                tableView.reloadData()
            } else if let data = response.data?.responseModel as? CommonResponse {
                self.showfloatingAlert(message: data.msg ?? "")
            }
            
        }
    }
}

extension EventAdminDetailViewController{
    
    func showUpdateSkillLevel(user:AdminEventUser?){
        
        var simpleDataArray = ["GT1","E1","E2","E3","E4","COACHES"]
        simpleDataArray = simpleDataArray.sorted(by: <)
        var simpleSelectedArray = [String]()
        simpleSelectedArray.append(user?.skillLevel ?? "")
        let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
            cell.textLabel?.text = item
        }
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
        
            self?.upgradeEvent(userID: user?.userId ?? "", skillVal: selectedItems.first ?? "")
//            self?.eventUsersList = self?.eventUsersList?.filter { $0.skillLevel?.contains(selectedItems.first ?? "") ?? false }
        }
    
        selectionMenu.dismissAutomatically = true
        selectionMenu.maxSelectionLimit = 1
        selectionMenu.cellSelectionStyle = .checkbox
        selectionMenu.title = "Upgrade Skill Level"
//        self.isDismissingSortView = true
        selectionMenu.show(style: .present, from: self)
    }
    
    func upgradeEvent(userID:String,skillVal:String){
        guard isNetWorkAvailable, let event = event else { return }
        addLoadingIndicator()
        let serviceManager = EventServiceManager()
        serviceManager.managerDelegate = self
        serviceManager.upgradeSkill(userId: userID, skill: skillVal)
    }
    
    func setupSortOption(){
        var simpleDataArray = Array(Set(initialArrayOfUsersList!.compactMap { $0.training }))
        if simpleDataArray.count > 0 {
            self.isNeedToShowTrainingSort = true;
        }

//        var trainigDataArray = Array(Set<Element: Hashable>(initialArrayOfUsersList!.compactMap { $0.rentals }))
//        if simpleDataArray != nil{
//            if simpleDataArray.count > 0 {
//                self.isNeedToShowRentalSort = true;
//            }
//        }

    }
}

extension EventAdminDetailViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        eventUsersList = initialArrayOfUsersList
        tableView.reloadData()
//        searchBar.isHidden = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isBlank {
            eventUsersList = initialArrayOfUsersList
        } else {
            eventUsersList = initialArrayOfUsersList?.filter { $0.displayName?.lowercased().starts(with: searchText.lowercased()) ?? false }
        }
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension EventAdminDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        searchBar.resignFirstResponder()
    }
}
