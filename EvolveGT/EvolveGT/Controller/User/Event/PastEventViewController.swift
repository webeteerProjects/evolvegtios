

//
//  PastEventViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 24/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Kingfisher


class PastEventViewController: UIViewController ,IndicatorInfoProvider{
    @IBOutlet var tableView: UITableView!
    var eventsData : PastEventListeRes?
    var eventList : [EventInfo]?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getPastEventsListata()
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "PAST EVENTS")
    }
    
    func getPastEventsListata(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = EventServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getPastEventList()
        }
    }
    
    func showListData(){
        self.tableView.reloadData()
    }

}


extension PastEventViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
        }else{
            //            showHomeSreen()
            if let data = response.data?.responseModel as? PastEventListeRes{
                self.eventsData =  data
                self.eventList = self.eventsData?.events
                showListData()
            }
        }
    }
    
}

extension PastEventViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
    
        let count = self.eventList?.count ?? 0
        if count == 0 {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "Sorry, No event has been found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }else{
            tableView.backgroundView = nil
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"EventListTableViewCell",for: indexPath) as! EventListTableViewCell
        cell.titleLbl.text = self.eventList?[indexPath.row].productName
        let dateStr = self.eventList?[indexPath.row].eventDate ?? ""
        cell.dateLbl.text =  "Event Date: " + dateStr
        let orderDateStr = self.eventList?[indexPath.row].orderDate ?? ""
        cell.orderDateLbl.text =  "Order Date: " + orderDateStr
        if let imgUrl = self.eventList?[indexPath.row].eventImage{
            cell.eventImage.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
        }
        
        return cell
    }
    
}

