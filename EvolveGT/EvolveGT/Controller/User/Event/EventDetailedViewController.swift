//
//  EventDetailedViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 27/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher

enum SectionOrder {
    case InfoSection
    case TrainingSection
    case RentalSection
    case AboutSection
}


class EventDetailedViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addtoCartBtn: UIButton!
    var eventDetails : EventDetails?
    var isGiftCard = false
    var userSelectedQuanity = 1
    var eventSlug : String = ""
    var eventName : String?
    var sectionOrders = [SectionOrder]()
    var totalPrice : Float = 0.0


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = eventName
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        getEventDetail()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func addToCartBtnClicked(){
        if self.isLoggedInUser{

        if self.isNetWorkAvailable{
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            var dict = [String : AnyObject]()
            dict.updateValue(UserInfo.currentUser()?.userID as AnyObject, forKey: "serial")
            dict.updateValue(eventDetails?.eventDate as AnyObject , forKey: "event_date")
            dict.updateValue(totalPrice.description as AnyObject, forKey: "event_price")
            dict.updateValue(eventDetails?.slug as AnyObject, forKey: "event")
            dict.updateValue(UserInfo.currentUser()?.role as AnyObject, forKey: "role")
            var rentalSelectedSizeList = [[String : String]]()

            if let rentalList = eventDetails?.rentalData {
                var dictArray = [[String : String]]()
                for item in rentalList{
                    if item.isSelected{
                        var dict = [String : String]()
                        var sizeDict = [String : String]()

                        dict.updateValue(item.slug, forKey: "slug")
                        dict.updateValue(item.productId, forKey: "id")
                        dict.updateValue(item.price, forKey: "price")
                        var isSelectedSize = false
                        for varItem in item.variations{
                            if varItem.isSelected{
                                sizeDict.updateValue(varItem.size, forKey: item.slug)
                                rentalSelectedSizeList.append(sizeDict)
                                isSelectedSize = true
                                dict.updateValue(varItem.size, forKey: "selectedSize")
                                dictArray.append(dict)
                            }
                        }
                        if isSelectedSize == false{
                            var msg = item.title ?? ""
                            msg.append(" size not selected")
                            self.removeLoadingIndicator()
                            self.showfloatingAlert(message: msg)
                            return
                        }
                    }
                }
                dict.updateValue(dictArray as AnyObject, forKey: "rental_list")
                dict.updateValue(rentalSelectedSizeList as AnyObject, forKey: "rental_size_list")

                
            }
            
            if let rentalList = eventDetails?.trainingData {
                var dictArray = [[String : String]]()
                for item in rentalList{
                    if item.isSelected{
                        var dict = [String : String]()
                        dict.updateValue(item.slug, forKey: "slug")
                        dict.updateValue(item.price, forKey: "price")
                        dict.updateValue(item.trainingId, forKey: "id")
                        dictArray.append(dict)
                    }
                }
                dict.updateValue(dictArray as AnyObject, forKey: "training_list")
            }
            self.addLoadingIndicator()
            serviceManager.addEventToCart(info: dict)
            }
            
        }
    }

    
    
    func getEventDetail(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = EventServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getEventDetails(eventSlug: eventSlug)
        }
    }
    
    func getCartListData(){
        if self.isNetWorkAvailable{
            let serviceManager = ShopWebServiceManager()
            serviceManager.getCardItems()
        }
    }

}

extension EventDetailedViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
                self.showfloatingAlert(message:response.error!)
        }else{
            
            if let serviceMngr = manager as? EventServiceManager{
                if let data = response.data?.responseModel as? EventDetails{
                    self.eventDetails = data
                    if eventDetails?.roleBasedPrice != nil{
                        self.totalPrice = Float(self.eventDetails?.roleBasedPrice.currentUserTypePrice ?? "0.00") ?? 0.0
                    }else{
                        self.totalPrice = Float(self.eventDetails?.price ?? "0.00") ?? 0.00
                    }
                    self.createTableSctionOrder()
                    self.tableView.reloadData()
                }
            }
            else if let serviceMngr = manager as? ShopWebServiceManager{
                if serviceMngr.serviceType == .AddEventToCart{
                    if let data = response.data?.responseModel as? ApiResponse{
                        self.getCartListData()
                        self.showfloatingAlert(message: data.msg)
                    }
                }
            }
            
        }
    }
    
    func createTableSctionOrder(){
        sectionOrders = [SectionOrder]()
        sectionOrders.append(.InfoSection)
        let tCount =  self.eventDetails?.trainingData.count ?? 0
        if tCount > 0 {
            sectionOrders.append(.TrainingSection)
        }
        let rCount =  self.eventDetails?.rentalData.count ?? 0
        if rCount > 0 {
            sectionOrders.append(.RentalSection)
        }
        sectionOrders.append(.AboutSection)

    }
    
}

extension EventDetailedViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionOrders.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if .InfoSection == sectionOrders[section]{
            return 2
        }
        else if .TrainingSection == sectionOrders[section]{
            return self.eventDetails?.trainingData.count ?? 0
        }
        else if .RentalSection == sectionOrders[section]{
            return self.eventDetails?.rentalData.count ?? 0
        }
        else if .AboutSection == sectionOrders[section]{
            return 1
        }
        else{
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if .TrainingSection == sectionOrders[section]{
            return 50
        }
        else if .RentalSection == sectionOrders[section]{
            return 50
        }
        else{ return 0}
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if .TrainingSection == sectionOrders[section]{
            return "Select Training"
        }
        else if .RentalSection == sectionOrders[section]{
            return "Select Rentals"
        }
        else{ return nil}
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if .InfoSection == sectionOrders[indexPath.section]{
            if indexPath.row == 0 {
                return 220;
            }
            else {
                return 80;
            }
        }
        else if .TrainingSection == sectionOrders[indexPath.section]{
            return 60;
        }
        else if .RentalSection == sectionOrders[indexPath.section]{
            if let info = eventDetails?.rentalData[indexPath.row]{
                if info.isSelected{
                    return 180;
                }else{
                    return 62
                }
            }
            return 50;
        }
        else{
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if .InfoSection == sectionOrders[indexPath.section]{
            if indexPath.row == 0 {
                let imgCell = tableView.dequeueReusableCell(withIdentifier:"ImageTableViewCell",for: indexPath) as! ImageTableViewCell
                if let imgUrl = eventDetails?.eventBanner{
                    imgCell.imgView.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
                }
                return imgCell;
            }
            else if indexPath.row == 1 {
                let infoCell = tableView.dequeueReusableCell(withIdentifier:"ItemInfoTableViewCell",for: indexPath) as! ItemInfoTableViewCell
                infoCell.titleLbl.text = "Date: "
                if let dateStr = eventDetails?.eventDate{
                    let joinDate = Date(fromString: dateStr, format: .custom("yyyy-MM-dd"))
                    let string = joinDate?.toString(format: .custom("dd MMM yyyy"))
                    infoCell.titleLbl.text?.append(string ?? "")
                }
                infoCell.priceLbl.text = "Total: $"
                let userTypeTitle = UserInfo.currentUser()?.role ?? "Guest"
                infoCell.geustPriceLbl.text = userTypeTitle.capitalized + ": $"
                if eventDetails?.roleBasedPrice != nil{
                    infoCell.geustPriceLbl.text?.append(eventDetails?.roleBasedPrice.currentUserTypePrice ?? "")
                }else{
                    infoCell.geustPriceLbl.text = userTypeTitle.capitalized + ": $0.00"
                }
                let priceValStr = String(format: "%.2f", self.totalPrice)
                infoCell.priceLbl.text?.append(priceValStr)
                return infoCell;
            }
            let overViewCell = tableView.dequeueReusableCell(withIdentifier:"OverviewTableViewCell",for: indexPath) as! OverviewTableViewCell
            overViewCell.descLbl.text = eventDetails?.productInfo
            return overViewCell
        }
        
        else if .RentalSection == sectionOrders[indexPath.section]{
            let rentalCell = tableView.dequeueReusableCell(withIdentifier:"SizeSelectionTableViewCell",for: indexPath) as! SizeSelectionTableViewCell
            rentalCell.indexPathTableView = indexPath
            rentalCell.delegate = self
            rentalCell.rentalInfo = eventDetails?.rentalData[indexPath.row]
            if let info = eventDetails?.rentalData[indexPath.row]{
                if info.isSelected{
                    rentalCell.bottomLayout.constant = 12
                    rentalCell.szieLabelHeightLayout.constant = 25
                }else{
                    rentalCell.bottomLayout.constant = 0
                    rentalCell.szieLabelHeightLayout.constant = 0
                }
            }
            rentalCell.showData()
            return rentalCell
        }
            
        else if .TrainingSection == sectionOrders[indexPath.section]{
            let rentalCell = tableView.dequeueReusableCell(withIdentifier:"TrianingSelectionTableViewCell",for: indexPath) as! TrianingSelectionTableViewCell
            rentalCell.indexPathTableView = indexPath
            rentalCell.delegate = self
            rentalCell.trainingData = eventDetails?.trainingData[indexPath.row]
            rentalCell.showData()
            return rentalCell
        }

            
        else{
            let overViewCell = tableView.dequeueReusableCell(withIdentifier:"OverviewTableViewCell",for: indexPath) as! OverviewTableViewCell
            overViewCell.descLbl.text = eventDetails?.productInfo
            overViewCell.separatorInset = UIEdgeInsets(top: 0, left: overViewCell.bounds.size.width, bottom: 0, right: 0)
            return overViewCell
        }
    }
    
}
extension EventDetailedViewController: TrainigSelectionCellDelegate,SizeSelectionCellDelegate {

    func refreshSelectedItem(indexPath:IndexPath){
        let rentalInfo = eventDetails?.rentalData[indexPath.row]
        if let val = Float(rentalInfo?.price ?? "0.00"){
            if rentalInfo?.isSelected ?? false{
                totalPrice = totalPrice + val
            }else{
                totalPrice = totalPrice - val
            }
        }
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
        
        self.tableView.reloadData()
    }
    
    func didSelectedSize(sizeInfo: Variation){
//            if let val = Float(sizeInfo?.price ?? "0.0"){
//                if sizeInfo?.isSelected ?? false{
//                    totalPrice = totalPrice + val
//                }else{
//                    totalPrice = totalPrice - val
//                }
//            }
    }


    func refreshSelectedTrianingItem(indexPath:IndexPath){
//        tableView.reloadRows(at: [indexPath], with: .automatic)
        let trainingData = eventDetails?.trainingData[indexPath.row]
        if let val = Float(trainingData?.price ?? "0.00"){
            if trainingData?.isSelected ?? false{
                totalPrice = totalPrice + val
            }else{
                totalPrice = totalPrice - val
            }
        }
       
        self.tableView.reloadData()
    }

}


