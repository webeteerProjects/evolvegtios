


//
//  PastEventViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 24/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Kingfisher


class UpComingEventViewController: UIViewController ,IndicatorInfoProvider{
    @IBOutlet var tableView: UITableView!
    var eventsData : PastEventListeRes?
    var eventList : [EventInfo]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUpComingEventsListata()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "UPCOMING EVENTS")
    }
    
    func getUpComingEventsListata(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = EventServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getUpComingEventList()
        }
    }
    
    func showListData(){
        self.tableView.reloadData()
    }
    
    @IBAction func cancelBtnClicked(sender:UIButton){
       self.showCancelConfirmAlert(itemIndex: sender.tag)
    }
    
    func showCancelConfirmAlert(itemIndex:Int) {
        let alerController = UIAlertController(title: "", message: "Are you sure you want to cancel this event?", preferredStyle: .alert)
        alerController.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: {(action:UIAlertAction) in
            self.cancelEvent(listIndex: itemIndex)
//            self.logout()
        }));
        alerController.addAction(UIAlertAction(title: "No", style: .cancel, handler: {(action:UIAlertAction) in
            
        }));
        if Constant.DeviceType.IS_IPAD {
            alerController.popoverPresentationController?.permittedArrowDirections = []
            alerController.popoverPresentationController?.sourceView = self.view
            alerController.popoverPresentationController?.sourceRect = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH / 2, y: Constant.ScreenSize.SCREEN_HEIGHT, width: 1.0, height: 1.0)
        }
        present(alerController, animated: true, completion: nil)
    }
    
    func cancelEvent(listIndex:Int){
        if let eventId = self.eventList?[listIndex].orderItemId{
            if self.isNetWorkAvailable{
                self.addLoadingIndicator()
                let serviceManager = EventServiceManager()
                serviceManager.managerDelegate = self
                serviceManager.cancelEvent(eventId: eventId)
            }
        }
    }
}


extension UpComingEventViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
//            showListData()
            self.showErrorAlert(message: response.error ?? " ")
        }else{
            //            showHomeSreen()
            if let serviceManager = manager as? EventServiceManager{
                if serviceManager.serviceType == .UpComingEvent{
                    if let data = response.data?.responseModel as? PastEventListeRes{
                        self.eventsData =  data
                        self.eventList = self.eventsData?.events
                        showListData()
                    }
                }
                else if serviceManager.serviceType == .CancelEvent{
                    self.getUpComingEventsListata()
//                    if let data = response.data?.responseModel as? CommonResponse{
//
//                    }
                }
            }
            
        }
    }
    
}

extension UpComingEventViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        let count = self.eventList?.count ?? 0
        if count == 0 {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "Sorry, No event has been found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
        }else{
            tableView.backgroundView = nil
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if AppSharedData.sharedInstance.userDetails?.eventCancel == true{
            return 150
        }
        return 120
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : EventListTableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier:"EventListTableViewCell",for: indexPath) as? EventListTableViewCell
        if AppSharedData.sharedInstance.userDetails?.eventCancel == true{
            cell = tableView.dequeueReusableCell(withIdentifier:"EventListCancelTableViewCell",for: indexPath) as? EventListTableViewCell
            cell.cancelBtn.tag = indexPath.row
        }
        cell.titleLbl.text = self.eventList?[indexPath.row].productName
        let dateStr = self.eventList?[indexPath.row].eventDate ?? ""
        cell.dateLbl.text =  "Event Date: " + dateStr
        let orderDateStr = self.eventList?[indexPath.row].orderDate ?? ""
        cell.orderDateLbl.text =  "Order Date: " + orderDateStr
        if let imgUrl = self.eventList?[indexPath.row].eventImage{
            cell.eventImage.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
        }
        
        return cell
    }
    
}

