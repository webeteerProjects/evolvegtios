//
//  EventsTabsViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 24/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class EventsTabsViewController: BaseButtonBarPagerTabStripViewController<YoutubeIconWithLabelCell> {
    
    let redColor = UIColor(red: 221/255.0, green: 0/255.0, blue: 19/255.0, alpha: 1.0)
    let unselectedIconColor = UIColor(red: 73/255.0, green: 8/255.0, blue: 10/255.0, alpha: 1.0)
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        buttonBarItemSpec = ButtonBarItemSpec.nibFile(nibName: "YoutubeIconWithLabelCell", bundle: Bundle(for: YoutubeIconWithLabelCell.self), width: { _ in
            return self.view.frame.size.width/3
        })
    }
    
    override func viewDidLoad() {
        self.title = "Upcoming Events"
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.init(hexString: "#28AC10")
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
        settings.style.selectedBarHeight = 4.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 30)
        changeCurrentIndexProgressive = { [weak self] (oldCell: YoutubeIconWithLabelCell?, newCell: YoutubeIconWithLabelCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
//            oldCell?.iconImage.tintColor = self?.unselectedIconColor
            //            newCell?.iconImage.tintColor = .white
            oldCell?.iconLabel.textColor = self?.unselectedIconColor
            newCell?.iconLabel.textColor = .white
            newCell?.iconLabel.font = UIFont.systemFont(ofSize: 12)
        }
        super.viewDidLoad()
//        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        if  AppSharedData.sharedInstance.selectedTabIndex == 0 {
//        }else{
//
//        }

//        customiseNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.moveToViewController(at: AppSharedData.sharedInstance.selectedTabIndex)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewWillDisappear(animated)
    }
    
    func customiseNavigationBar(){
        let clearBtn: UIButton = UIButton()
        clearBtn.setTitle("CLEAR", for: .normal)
        clearBtn.titleLabel?.textColor = UIColor.white
        //        clearBtn.contentMode = .left
        clearBtn.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 13)!
        clearBtn.addTarget(self, action:  #selector(clearNotifiactions), for: .touchUpInside)
        clearBtn.frame = CGRect(x: 0, y: 0, width:60, height: 40)
        let barButton = UIBarButtonItem(customView: clearBtn)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func clearNotifiactions(){
//        sideMenuController?.()
    }
    // MARK: - PagerTabStripDataSource
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return [self.storyboard?.instantiateViewController(withIdentifier: "UpComingEventViewController") as! UpComingEventViewController,  self.storyboard?.instantiateViewController(withIdentifier: "PastEventViewController") as! PastEventViewController,self.storyboard?.instantiateViewController(withIdentifier: "AllEventViewController") as! AllEventViewController]

//        let child_1 = UpComingEventViewController(style: .plain, itemInfo: IndicatorInfo(title: " HOME", image: UIImage(named: "home")))
//        let child_2 = PastEventViewController(style: .plain, itemInfo: IndicatorInfo(title: " TRENDING", image: UIImage(named: "trending")))
//        let child_3 = AllEventViewController(itemInfo: IndicatorInfo(title: " ACCOUNT", image: UIImage(named: "profile")))
//        return [child_1, child_2, child_3]
    }
    
    override func configure(cell: YoutubeIconWithLabelCell, for indicatorInfo: IndicatorInfo) {
//        cell.iconImage.image = indicatorInfo.image?.withRenderingMode(.alwaysTemplate)
        cell.iconLabel.text = indicatorInfo.title?.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        if indexWasChanged && toIndex > -1 && toIndex < viewControllers.count {
            let child = viewControllers[toIndex] as! IndicatorInfoProvider // swiftlint:disable:this force_cast
            UIView.performWithoutAnimation({ [weak self] () -> Void in
                guard let me = self else { return }
                me.title = child.indicatorInfo(for: me).title
//                me.navigationItem.leftBarButtonItem?.title =  child.indicatorInfo(for: me).title
            })
        }
    }
    
    // MARK: - Actions
    
    
}
