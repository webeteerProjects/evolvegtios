//
//  HomeViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 03/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var isNoUpComingEvent = false
    var isNoPastEvent = false
    var isNoCreditHistory = false
    
    
    
    var isExpandUpComingEvent = false
    var isExpandPastEvent = false
    var isExpandCreditHistory = false
    var excutedSummryApiCall = false


//    var userDetiilsResponse:UserDetailsRes?
    var userDetais : UserDetails?
    var eventSummary : EventSummary?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Dashboard"
        isNoUpComingEvent = true
        isNoPastEvent = true
        isNoCreditHistory = true

        self.setupTableView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        excutedSummryApiCall = false
        isNoUpComingEvent = true
        isNoPastEvent = true
        isNoCreditHistory = true

        isExpandUpComingEvent = true
        isExpandPastEvent = false
        isExpandCreditHistory = false
        callUserDetailsApi()
        getCartListData()
        callSummaryApi()
        
        if AppSharedData.sharedInstance.country == nil{
            callCountryApi()
        }
        
//        if AppSharedData.sharedInstance.state == nil{
//            callStateApi()
//        }
    }
    
    func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 900
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "HMenu"), for: UIControl.State.normal)
        button.addTarget(self, action:#selector(self.menuBtnClicked) , for: .touchUpInside)
        button.frame =  CGRect.init(x: 0, y: 0, width: 45, height: 45)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
    }

    @objc func menuBtnClicked(){
        self.sideMenuController?.revealMenu()
    }
    
    func callUserDetailsApi() {
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = LoginServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.userDetails()
        }
    }
    
    
    func callCountryApi(){
        if self.isNetWorkAvailable{
            let serviceManager = LoginServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getCountryList()
        }
    }
    
  
    
    func callSummaryApi(){
        if self.isNetWorkAvailable{
            let serviceManager = LoginServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.eventSummery()
        }
    }
    
    
    func getCartListData(){
        if self.isNetWorkAvailable{
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getCardItems()
        }
    }
    
    @IBAction func expandUpcomingEventCell(){
        isExpandUpComingEvent = !isExpandUpComingEvent
        self.tableView.reloadData()
    }
    
    @IBAction func expandPastEventCell(){
        isExpandPastEvent = !isExpandPastEvent
        self.tableView.reloadData()
    }
    
    @IBAction func expandCreditHistoryCell(){
        isExpandCreditHistory = !isExpandCreditHistory
        self.tableView.reloadData()
    }
    
    
    @IBAction func seeMoreUpcomingEventCell(){
        AppSharedData.sharedInstance.selectedTabIndex = 0
        let storyboard = UIStoryboard.init(name: "User", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EventsTabsViewController") as? EventsTabsViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    @IBAction func seeMorePastEventCell(){
        AppSharedData.sharedInstance.selectedTabIndex = 1
        let storyboard = UIStoryboard.init(name: "User", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EventsTabsViewController") as? EventsTabsViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    @IBAction func seeAllEventBTnClicked(){
        AppSharedData.sharedInstance.selectedTabIndex = 2
        let storyboard = UIStoryboard.init(name: "User", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EventsTabsViewController") as? EventsTabsViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    @IBAction func seeMoreCreditHistoryCell(){
        let storyboard = UIStoryboard.init(name: "User", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CreditHistoryViewController") as? CreditHistoryViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    func showBadgeCount(){
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3] as! UITabBarItem
            if AppSharedData.sharedInstance.cartCount > 0{
                tabItem.badgeValue = String(AppSharedData.sharedInstance.cartCount)
            }else{
                tabItem.badgeValue = nil
            }
        }
    }
    
}

extension HomeViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            if let serviceManager = manager as? LoginServiceManager{
                print(serviceManager.serviceType)
            }
            self.showErrorAlert(message: response.error ?? "went something wrong")
        }else{
            //            showHomeSreen()
            if let serviceManager = manager as? ShopWebServiceManager{
                showBadgeCount()
            }else{
                if let serviceManager = manager as? LoginServiceManager{
                    if let data = response.data?.responseModel as? UserDetailsRes{
                        AppSharedData.sharedInstance.userDetails = data.result
                        self.userDetais =  data.result
                        self.tableView.reloadData()
                    }
                    else {
                        excutedSummryApiCall = true
                        if let data = response.data?.responseModel as? EventSummary{
                            self.eventSummary =  data
                            if self.eventSummary?.upcomingEvents.count ?? 0 > 0 {
                                isNoUpComingEvent = false;
                            }
                            if self.eventSummary?.pastEvents.count ?? 0 > 0 {
                                isNoPastEvent = false;
                            }
                            if self.eventSummary?.creditHistory.count ?? 0 > 0 {
                                isNoCreditHistory = false;
                            }
                            self.tableView.reloadData()
                        }
                    }
                }
                
            }
            
        }
    }
}


extension HomeViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if excutedSummryApiCall{
            return 4;
            
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 410;
        }
        else if indexPath.row == 1 {
            if isNoUpComingEvent {
                return 90
            }else {
                if isExpandUpComingEvent{
                    return 200
                }else{
                    return 55
                }
            }
        }
        else if indexPath.row == 2 {
            if isNoPastEvent {
                return 90
            }else {
                if isExpandPastEvent{
                    return 200
                }else{
                    return 55
                }
            }
        }
        else if indexPath.row == 3 {
            if isNoCreditHistory{
                return 90
            }else {
                if isExpandCreditHistory{
                    return 200
                }else{
                    return 55
                }
            }
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0 {
            let profileCell = tableView.dequeueReusableCell(withIdentifier:"ProfileTableViewCell",for: indexPath) as! ProfileTableViewCell
            profileCell.userInfo = self.userDetais
            profileCell.eventSummary = self.eventSummary
            profileCell.showData()
            return profileCell;
        }
        else if indexPath.row == 1 {
            if isNoUpComingEvent {
                let emptyInfoCell = tableView.dequeueReusableCell(withIdentifier:"EmptyInfoCell",for: indexPath) as! EmptyDataTableViewCell
                emptyInfoCell.titleLbl.text = "Upcoming Events"
                emptyInfoCell.msgLbl.text = "Sorry, no event has been found."
                return emptyInfoCell;

            }else {
                let evntInfoCell = tableView.dequeueReusableCell(withIdentifier:"EventInfoTableViewCell",for: indexPath) as! EventInfoTableViewCell
                evntInfoCell.expandBtn.setImage(UIImage(named:"plus_green"), for: .normal)
                if isExpandUpComingEvent{
                    evntInfoCell.expandBtn.setImage(UIImage(named:"minus_green"), for: .normal)
                }
                evntInfoCell.titleLbl.text = "Upcoming Events"
                evntInfoCell.eventInfo = self.eventSummary?.upcomingEvents.first
                evntInfoCell.showData()
                return evntInfoCell
            }
        }
        else if indexPath.row == 2 {
            if isNoPastEvent {
                let emptyInfoCell = tableView.dequeueReusableCell(withIdentifier:"EmptyInfoCell",for: indexPath) as! EmptyDataTableViewCell
                emptyInfoCell.titleLbl.text = "Past Events"
                emptyInfoCell.msgLbl.text = "Sorry, no event has been found."
                return emptyInfoCell;

            }else {
                let evntInfoCell = tableView.dequeueReusableCell(withIdentifier:"PastEventInfoTableViewCell",for: indexPath) as! EventInfoTableViewCell
                evntInfoCell.expandBtn.setImage(UIImage(named:"plus_green"), for: .normal)
                if isExpandPastEvent{
                    evntInfoCell.expandBtn.setImage(UIImage(named:"minus_green"), for: .normal)
                }
                evntInfoCell.titleLbl.text = "Past Events"
                evntInfoCell.eventInfo = self.eventSummary?.pastEvents.first
                evntInfoCell.showData()
                return evntInfoCell
            }
        }else{
            if isNoCreditHistory {
                let emptyInfoCell = tableView.dequeueReusableCell(withIdentifier:"EmptyInfoCell",for: indexPath) as! EmptyDataTableViewCell
                emptyInfoCell.titleLbl.text = "Credit History"
                emptyInfoCell.msgLbl.text = "Sorry, no credit history has been found."
                return emptyInfoCell;
            }else {
                let historyInfoCell = tableView.dequeueReusableCell(withIdentifier:"CreditHistoryTableViewCell",for: indexPath) as! CreditHistoryTableViewCell
                historyInfoCell.expandBtn.setImage(UIImage(named:"plus_green"), for: .normal)
                if isExpandCreditHistory{
                    historyInfoCell.expandBtn.setImage(UIImage(named:"minus_green"), for: .normal)
                }
                historyInfoCell.titleLbl.text = "Credit History"
                historyInfoCell.creditInfo = self.eventSummary?.creditHistory.first
                historyInfoCell.showData()
                return historyInfoCell
            }
        }
    }
    
}

