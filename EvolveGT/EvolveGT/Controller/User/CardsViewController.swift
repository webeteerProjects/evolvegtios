
//
//  GiftCardsViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 18/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class CardsViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var cardListRes : CardListRes?
    var cardList : [CardInfo]?
    var isGrid = false
    var isGiftCard = false

    override func viewDidLoad() {
        super.viewDidLoad()
        if isGiftCard{
            self.title = "Gifts"
        }else{
            self.title = "Archie Cards"
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCardListata()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func getCardListata(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            if isGiftCard{
                serviceManager.getGiftCards()
            }else {
                serviceManager.getArchieCards()
            }
        }
    }
    
    func showListData(){
        self.collectionView.reloadData()
    }

}

extension CardsViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
        }else{
            //            showHomeSreen()
            if let data = response.data?.responseModel as? CardListRes{
                self.cardListRes =  data
                self.cardList = self.cardListRes?.data
                showListData()
            }
        }
    }
    
}


extension CardsViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return self.cardList?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isGrid {
            return CGSize(width: Constant.ScreenSize.SCREEN_WIDTH - 20, height: 80)
        }else{
            return CGSize(width: Constant.ScreenSize.SCREEN_WIDTH - 20, height: 80)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if isGiftCard{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCardListCollection", for: indexPath as IndexPath) as! CardListCollectionViewCell
            cell.cardData = self.cardList?[indexPath.row]
            cell.showData()
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArchieveCardListCollection", for: indexPath as IndexPath) as! CardListCollectionViewCell
            cell.cardData = self.cardList?[indexPath.row]
            cell.showData()
            return cell
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let itemDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "AddtoCartViewController") as! AddtoCartViewController
        itemDetailsVC.itemInfo = self.cardList?[indexPath.row]
        itemDetailsVC.isGiftCard = isGiftCard

        self.navigationController?.pushViewController(itemDetailsVC, animated: true)
    }
}
