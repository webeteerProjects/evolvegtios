//
//  CartViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 10/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkOutBtn: UIButton!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var totalTextLbl: UILabel!
    @IBOutlet weak var cartEmptyLbl: UILabel!

    var cartListRes : CartResponse?
    var cartList : [CartItem]?
    var isGuestUser = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Cart"
        if isGuestUser == false{
            addHamburgerMenu()
        }
        setupTableView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCartListData()
    }

    
    func showBadgeCount(){
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3] as! UITabBarItem
            if AppSharedData.sharedInstance.cartCount > 0{
                tabItem.badgeValue = String(AppSharedData.sharedInstance.cartCount)
            }else{
                tabItem.badgeValue = nil
            }
        }
    }
    
    
    func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 130
        checkOutBtn.showRoundBorder()
    }

   
    func addHamburgerMenu(){
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "HMenu"), for: UIControl.State.normal)
        button.addTarget(self, action:#selector(self.menuBtnClicked) , for: .touchUpInside)
        button.frame =  CGRect.init(x: 0, y: 0, width: 45, height: 45)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
    }
    
    @objc func menuBtnClicked(){
        self.sideMenuController?.revealMenu()
    }
    
    func getCartListData(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getCardItems()
        }
    }
    
    func hideOrShowNoCartItemMsg(hide:Bool){
        checkOutBtn.isHidden = hide
        totalLbl.isHidden = hide
        totalTextLbl.isHidden = hide
        cartEmptyLbl.isHidden = !hide
    }
    
    @IBAction func paymentBtnClicked(sender: UIButton){
        
        guard let itemCount = self.cartList?.count, itemCount > 0 else {
            return
        }
//        let paymentVC = self.storyboard?.instantiateViewController(withIdentifier:"PlaceOrderViewController" ) as! PlaceOrderViewController
//        paymentVC.totalAmount =  "100"
        let proceedPaymentVC = self.storyboard?.instantiateViewController(withIdentifier:"ProceedToPaymentViewController" ) as! ProceedToPaymentViewController
        proceedPaymentVC.cartListRes =  self.cartListRes
        proceedPaymentVC.cartList =  self.cartList
        proceedPaymentVC.cartTotalPrice = totalLbl.text ?? ""
        proceedPaymentVC.isWalletEnabled = self.cartListRes?.walletEnabled ?? false
        self.navigationController?.pushViewController(proceedPaymentVC, animated: true)
    }
    
    @IBAction func deleteBtnClicked(sender: UIButton){
        let alerController = UIAlertController(title: "", message: "Are you sure you want to delete?", preferredStyle: .alert)
        alerController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {(action:UIAlertAction) in
            self.deleteCartItem(index:sender.tag)
        }));
        alerController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action:UIAlertAction) in
            
        }));
        present(alerController, animated: true, completion: nil)

    }

    func deleteCartItem(index:Int){
        if self.isNetWorkAvailable{
            let itemInfo = self.cartList?[index]
            var dict = [String : AnyObject]()
            dict.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "id")
            dict.updateValue(itemInfo?.objectId as AnyObject? ?? "" as AnyObject, forKey: "itemid")
            dict.updateValue(itemInfo?.cartId as AnyObject? ?? "" as AnyObject, forKey: "cartid")
            dict.updateValue(itemInfo?.source as AnyObject, forKey: "method")
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.deleteCart(info: dict)
        }
    }

}

extension CartViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            if UserInfo.currentUser()?.token != nil{
                self.showErrorAlert(message: response.error ?? " ")
            }else{
                if self.cartList != nil{
                    if cartList!.count < 1{
                        self.hideOrShowNoCartItemMsg(hide: true)
                    }
                }else{
                    self.hideOrShowNoCartItemMsg(hide: true)
                }
            }
        }else{
            if let data = response.data?.responseModel as? CartResponse{
                showBadgeCount()
                AppSharedData.sharedInstance.userDetails?.walletAmount = data.wallet
                self.cartListRes =  data
                let quantityStr = self.cartListRes?.wallet ?? " "
                self.cartList = self.cartListRes?.data
                var totalPrice : Float = 0.00
                if self.cartList != nil{
                    if cartList!.count < 1{
                        self.totalLbl.text =  "0.00"
                        self.hideOrShowNoCartItemMsg(hide: true)
                    }else{
                        self.hideOrShowNoCartItemMsg(hide: false)
                    }
                    for item in self.cartList! {
                        if let priceVal = Float(item.price ?? "0.00"){
                            if item.source == "archie"{
                                let qnty = Int(item.quantity ?? "1")
                                let itemPrice = priceVal * Float(qnty ?? 1)
                                totalPrice = totalPrice + itemPrice
                            }else{
                                totalPrice = totalPrice + priceVal
                            }
                        }
                    }
                }else{
                    self.hideOrShowNoCartItemMsg(hide: true)
                }
                self.totalLbl.text = "$" + String(format: "%.2f", totalPrice)  //totalPrice.description
                self.tableView.reloadData()
            }
            if let data = response.data?.responseModel as? ApiResponse{
                self.getCartListData()
            }
        }
    }
    
}

extension CartViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartList?.count ?? 0;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.cartList?[indexPath.row].source == "giftcard"{
            return 130
        }
        else if self.cartList?[indexPath.row].source == "rentals"{
            return 130
        }
        else{
            return 120
        }
//        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.cartList?[indexPath.row].source == "giftcard"{
            let cartItemCell = tableView.dequeueReusableCell(withIdentifier:"CartGiftItemTableViewCell",for: indexPath) as! CartItemTableViewCell
            cartItemCell.deleteBtn.tag = indexPath.row
            cartItemCell.itemInfo = self.cartList?[indexPath.row]
            cartItemCell.showData()
            return cartItemCell;
        }
        else if self.cartList?[indexPath.row].source == "rentals"{
            let cartItemCell = tableView.dequeueReusableCell(withIdentifier:"CartGiftItemTableViewCell",for: indexPath) as! CartItemTableViewCell
            cartItemCell.deleteBtn.tag = indexPath.row
            cartItemCell.itemInfo = self.cartList?[indexPath.row]
            cartItemCell.showData()
            return cartItemCell;
        }
        else{
            let cartItemCell = tableView.dequeueReusableCell(withIdentifier:"CartItemTableViewCell",for: indexPath) as! CartItemTableViewCell
            cartItemCell.deleteBtn.tag = indexPath.row
            cartItemCell.itemInfo = self.cartList?[indexPath.row]
            cartItemCell.showData()
            return cartItemCell;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.cartList?[indexPath.row].source == "giftcard"{
            let itemDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "AddtoCartViewController") as! AddtoCartViewController
            var cardObj = CardInfo()
            cardObj.slug =  self.cartList?[indexPath.row].slug
            cardObj.image =  self.cartList?[indexPath.row].image
            cardObj.price =  self.cartList?[indexPath.row].price
            cardObj.title =  self.cartList?[indexPath.row].title
            itemDetailsVC.itemInfo = cardObj
            itemDetailsVC.isGiftCard = true
            if let giftInfo =  self.cartList?[indexPath.row].unAttributes.first{
                itemDetailsVC.email = giftInfo.email
                itemDetailsVC.name = giftInfo.name
            }
            self.navigationController?.pushViewController(itemDetailsVC, animated: true)

        }
        else if self.cartList?[indexPath.row].source == "archie"{
            let itemDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "AddtoCartViewController") as! AddtoCartViewController
            let cardObj = CardInfo()
            cardObj.slug =  self.cartList?[indexPath.row].slug
            cardObj.image =  self.cartList?[indexPath.row].image
            cardObj.price =  self.cartList?[indexPath.row].price
            cardObj.title =  self.cartList?[indexPath.row].title
            cardObj.content =  self.cartList?[indexPath.row].title
            itemDetailsVC.itemInfo = cardObj
            itemDetailsVC.isGiftCard = false
            self.navigationController?.pushViewController(itemDetailsVC, animated: true)

        }else{
            
            let detailsView = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailedViewController") as! EventDetailedViewController
            detailsView.eventName = self.cartList?[indexPath.row].title
            detailsView.eventSlug = self.cartList?[indexPath.row].slug ?? ""
            if let pSlug = self.cartList?[indexPath.row].parentSlug{
                if pSlug.isValidString{
                    detailsView.eventSlug = pSlug
                }
            }
//            if let type = self.cartList?[indexPath.row].source{
//                if type == "rentals"{
//
//                }
//                else if type == "training"{
//
//                }
//            }

            self.navigationController?.pushViewController(detailsView, animated: true)
        }
    }


}
