

//
//  ChangePasswordViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 07/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordViewController: SideMenuAccessibleController {
    @IBOutlet weak var crntPwTextfld: SkyFloatingLabelTextField!
    @IBOutlet weak var newPwTextfld: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPwTextfld: SkyFloatingLabelTextField!
    @IBOutlet weak var doneBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Change Password"
        doneBtn.showRoundCorner(roundCorner: 3.0)
//        crntPwTextfld.text = "Evgt2019!"
//        newPwTextfld.text = "Qwerty1!"
//        confirmPwTextfld.text = "Qwerty1!"

        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func doneBtnClicked(){
        callService()
    }
   

    func validate() -> Bool{
        guard let currentPw = self.crntPwTextfld.text, !currentPw.isBlank else {
            self.showfloatingAlert(message:"Please enter current password")
            return false
        }
        guard let newPassword = self.newPwTextfld.text, newPassword.isBlank else {
            self.showfloatingAlert(message:"Please new current password")
            return false
        }
        guard let confirmNewPassword = self.confirmPwTextfld.text, !confirmNewPassword.isBlank else {
            self.showfloatingAlert(message:"Please enter confirm password")
            return false
        }
        if newPassword == confirmNewPassword{
            
        }else{
            self.showfloatingAlert(message:"Password mismatch!")
            return false
        }
        
        //        AppSharedData.sharedInstance.email = email
        //        AppSharedData.sharedInstance.password = password
        
        return true
    }
    
    func callService(){
        guard let currentPw = self.crntPwTextfld.text, !currentPw.isBlank else {
            self.showfloatingAlert(message:"Please enter current password")
            return
        }
        guard let newPassword = self.newPwTextfld.text, !newPassword.isBlank else {
            self.showfloatingAlert(message:"Please new password")
            return
        }
        guard let confirmNewPassword = self.confirmPwTextfld.text, !confirmNewPassword.isBlank else {
            self.showfloatingAlert(message:"Please enter confirm password")
            return
        }
        if newPassword == confirmNewPassword{
            
        }else{
            self.showfloatingAlert(message:"Password mismatch!")
            return
        }
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = LoginServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.resetPassword(oldPassword: currentPw, newPassword: newPassword)
        }
    }
    
    
    func showLogoutConfirmAlert() {
        let alerController = UIAlertController(title: Constant.AppSetupConstant.KAppName, message: "Your password has been updated, Please login with new password", preferredStyle: .alert)
        alerController.addAction(UIAlertAction(title: "Ok", style: .default, handler: {(action:UIAlertAction) in
                self.logout()
        }));
        if Constant.DeviceType.IS_IPAD {
            alerController.popoverPresentationController?.permittedArrowDirections = []
            alerController.popoverPresentationController?.sourceView = self.view
            alerController.popoverPresentationController?.sourceRect = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH / 2, y: Constant.ScreenSize.SCREEN_HEIGHT, width: 1.0, height: 1.0)
        }
        present(alerController, animated: true, completion: nil)
    }
    
    func logout(){
        UserInfo.currentUser()?.clearSession()
        let storboard = UIStoryboard.init(name: "Main", bundle: nil)
        let navVC = storboard.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = navVC;
    }
}

extension ChangePasswordViewController:WebServiceTaskManagerProtocol,UITextFieldDelegate{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? "")
        }
        else{
           self.showLogoutConfirmAlert()
        }
        
    }
    
    
}
