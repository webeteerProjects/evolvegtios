

//
//  ResetPasswordViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 25/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import  SkyFloatingLabelTextField
import IQKeyboardManagerSwift

class ResetPasswordViewController: UIViewController {
    @IBOutlet weak var pwTextfld: SkyFloatingLabelTextField!
    @IBOutlet weak var oldPwTextfld: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPwTextfld: SkyFloatingLabelTextField!

    @IBOutlet weak var submitBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    func validate() {
       
        guard let password = self.pwTextfld.text, !password.isBlank else {
            //            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterPassword)
            self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterPassword)
            return
        }
        
        guard let newpassword = self.oldPwTextfld.text, !password.isBlank else {
            //            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterPassword)
            self.showWarningAlert(message: "Please Enter New Password")
            return
        }
        guard let confirmpassword = self.oldPwTextfld.text, !password.isBlank else {
            //            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterPassword)
            self.showWarningAlert(message: "Password Mismatch!")
            return
        }
        
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = LoginServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.resetPassword(oldPassword: password, newPassword: newpassword)
        }
    }
    

}


extension ResetPasswordViewController:WebServiceTaskManagerProtocol,UITextFieldDelegate{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            //            self.showErrorAlert(message: response.error!)
            self.showErrorAlert(message: response.error ?? " ")
            
        }else{
//            self.dismiss(animated: true, completion: nil)
            //            self.registerPushNotification()
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0{
//            passwordTextfld.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
        
        
    }
    
    func showHomeScreen(){
        appDelegate.showHomeView()
    }
    
}
