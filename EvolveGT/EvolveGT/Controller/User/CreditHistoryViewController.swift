


//
//  CreditHistoryViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 26/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class CreditHistoryViewController: SideMenuAccessibleController {
    @IBOutlet weak var tableView: UITableView!
    var creditRes : CreditHistoryRes?
    var creditHistoryList : [CreditHistory]?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Credit History"
        self.tableView.tableFooterView = UIView()
        getCreditListata()

        // Do any additional setup after loading the view.
    }
    

    func getCreditListata(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = EventServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getCreditHistoryList()
        }
    }
    
    func showListData(){
        self.tableView.reloadData()
    }
    
    
    

}

extension CreditHistoryViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
        }else{
            //            showHomeSreen()
            if let data = response.data?.responseModel as? CreditHistoryRes{
                self.creditRes =  data
                self.creditHistoryList = self.creditRes?.result
                showListData()
            }
        }
    }
    
}


extension CreditHistoryViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.creditHistoryList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"CreditHistoryTableViewCell",for: indexPath) as! CreditHistoryTableViewCell
        cell.creditInfo = self.creditHistoryList?[indexPath.row]
        cell.borderContainerView.showRoundBorder()
        cell.showData()
        return cell
    }
    
}
