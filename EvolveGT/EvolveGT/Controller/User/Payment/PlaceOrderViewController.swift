

//
//  PlaceOderViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 20/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import BraintreeDropIn
import Braintree



class PlaceOrderViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkOutBtn: UIButton!
    var clienbtAccessTokenInfo: PaymentClientAccessToken?
    var paymentResultRes:PaymentResponse?
    var finalPaymentStatus:PaymentDone?
    var totalAmount: String!
    var subTotalAmount: String!
    var orderId : String!

    var coupon : String?
    var couponCode : String?

    var orderID : String!
    var payerID : String!
    var paymentID : String!
    var isDisabledWallet = false
    var isZeroPayment = false
    var isReducedWalletAmount = false
    var reducedWalletAmount : String?

    var isSelectedPayPal = true
    var customerInfo : Customer?
    var isAddedPromo = false
    var disablePaymentMethods = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Checkout"
        setupTableView()
        self.checkPrivateEvent()
        // Do any additional setup after loading the view.
    }
    
    func checkPrivateEvent(){
        if isDisabledWallet{
            self.isSelectedPayPal = true
            getClientAccessToken()
            return
        }
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.checkPrivateEvent()
        }
    }
    
    func getClientAccessToken(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getPaymentAccessToken()
        }
    }
    
    
    func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView =  UIView()
        //        var frame = CGRect.zero
        //        frame.size.height = .leastNormalMagnitude
        //        tableView.tableHeaderView = UIView(frame: frame)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 130
        checkOutBtn.showRoundBorder()
    }
    
    
    func sendNonseToSever(nonseValue: String){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let paymentType = selectedPaymentTypes()
            var couponCode = ""
            if self.isAddedPromo == true{
               let couponCode = self.couponCode ?? ""
            }
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.initiatePayment(amount: totalAmount, nonse: nonseValue, couponCode:  self.isAddedPromo ? self.couponCode ?? "" : nil, payemntModes: paymentType)
        }
    }
    
    
    func selectedPaymentTypes() -> String{
         var paymentTypesAdded : String = ""
        if self.isAddedPromo == true{
            paymentTypesAdded.append("coupon")
            let cVal = self.couponCode ?? ""
        }
        if self.isReducedWalletAmount == true{
            if paymentTypesAdded.isValidString{
                paymentTypesAdded.append("-")
            }
            paymentTypesAdded.append("wallet")
        }else{
            if isSelectedPayPal == false{
                if paymentTypesAdded.isValidString{
                    paymentTypesAdded.append("-")
                }
                paymentTypesAdded.append("wallet")
            }
        }
        if isSelectedPayPal{
            if paymentTypesAdded.isValidString{
                paymentTypesAdded.append("-")
            }
            paymentTypesAdded.append("paypal")
        }
        return paymentTypesAdded
    }
    
    
    func payemtDoneApi(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            var dict = [String : AnyObject]()
            let userId = UserInfo.currentUser()?.userID ?? ""
            dict.updateValue(userId as AnyObject, forKey: "serial")
            
            var paymentTypesAdded : String = ""
            if self.isAddedPromo == true{
                paymentTypesAdded.append("coupon")
                let cVal = self.couponCode ?? ""
                dict.updateValue(cVal as AnyObject, forKey: "coupon")
            }
            
            if self.isReducedWalletAmount == true{
                if paymentTypesAdded.isValidString{
                    paymentTypesAdded.append("-")
                }
                paymentTypesAdded.append("wallet")
            }else{
                if isSelectedPayPal == false{
                    if paymentTypesAdded.isValidString{
                        paymentTypesAdded.append("-")
                    }
                    paymentTypesAdded.append("wallet")
                }
            }
//            dict.updateValue(paymentTypesAdded as AnyObject, forKey: "payment")
            
            if isSelectedPayPal{
                dict.updateValue("paypal" as AnyObject, forKey: "payment")
                let tranId = self.paymentResultRes?.response.transaction.id ?? ""
                dict.updateValue(tranId as AnyObject, forKey: "paymentToken")
                dict.updateValue(tranId as AnyObject, forKey: "orderID")
                if self.paymentResultRes?.response.transaction.paypal == nil{
                    if self.paymentResultRes?.response.transaction.creditCard != nil{
                        let paymentId = self.paymentResultRes?.response.transaction.creditCard.uniqueNumberIdentifier ?? ""
                        dict.updateValue(paymentId as AnyObject, forKey: "payerID")
                        let tokenVal = self.paymentResultRes?.response.transaction.creditCard.token ?? ""
                        dict.updateValue(tokenVal as AnyObject, forKey: "paymentToken")
                    }
                }else{
                    if self.paymentResultRes?.response.transaction.paypal != nil{
                        let paymentId = self.paymentResultRes?.response.transaction.paypal.payerId ?? ""
                        dict.updateValue(paymentId as AnyObject, forKey: "payerID")
                        let tokenVal = self.paymentResultRes?.response.transaction.paypal.token ?? ""
                        dict.updateValue(tokenVal as AnyObject, forKey: "paymentToken")
                    }
                }
                if paymentTypesAdded.isValidString{
                    paymentTypesAdded.append("-")
                }
                paymentTypesAdded.append("paypal")
                dict.updateValue(paymentTypesAdded as AnyObject, forKey: "payment")

            }else{
                dict.updateValue(paymentTypesAdded as AnyObject, forKey: "payment")
            }
//            dict.updateValue(itemInfo?.price as AnyObject? ?? "" as AnyObject, forKey: "price")
            dict.updateValue("sale" as AnyObject, forKey: "intent")
            serviceManager.donePayment(info: dict)
        }
    }
    
//    donePayment
    
    
    @IBAction func placeOrderBtnClicked(sender: UIButton){
        
        if let amount = Float(totalAmount){
            if amount == 0{
               zeroPaymentApi()
            }else{
                if isSelectedPayPal{
                    self.showDropIn(clientTokenOrTokenizationKey: clienbtAccessTokenInfo?.clientToken ?? "")
                }else{
                    self.payemtDoneApi()
                }
            }
        }else{
           zeroPaymentApi()
        }
               
//        self.getClientAccessToken()
    }
    
    @IBAction func cartBtnClicked(sender: UIButton){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func payPalSelectionBtnClicked(sender: UIButton){
        isSelectedPayPal = !isSelectedPayPal
        self.tableView?.reloadData()
//        self.tableView?.reloadSections(IndexSet(integer:2), with: .none)
    }
    
    
    @IBAction func walletSelectionBtnClicked(sender: UIButton){
        isSelectedPayPal = !isSelectedPayPal
        self.tableView?.reloadData()
//        self.tableView?.reloadSections(IndexSet(integer:2), with: .none)
    }
    
   
    func zeroPaymentApi(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            var dict = [String : AnyObject]()
            let userId = UserInfo.currentUser()?.userID ?? ""
            dict.updateValue(userId as AnyObject, forKey: "serial")
            dict.updateValue("sale"as AnyObject, forKey: "intent")
            var paymentTypesAdded : String = ""
            if self.isAddedPromo == true{
                paymentTypesAdded.append("coupon")
                let cVal = self.couponCode ?? ""
                dict.updateValue(cVal as AnyObject, forKey: "coupon")
            }
            
            if self.isReducedWalletAmount == true{
                if paymentTypesAdded.isValidString{
                    paymentTypesAdded.append("-")
                }
                paymentTypesAdded.append("wallet")
            }
            if paymentTypesAdded.isValidString{
                dict.updateValue(paymentTypesAdded as AnyObject, forKey: "payment")
            }else{
                dict.updateValue("wallet" as AnyObject, forKey: "payment")
            }
            self.isZeroPayment = true
            serviceManager.donePayment(info: dict)
        }
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let paymentResult = result {
                let nonseVal : String = paymentResult.paymentMethod?.nonce ?? ""
                self.sendNonseToSever(nonseValue: nonseVal)
                // Use the BTDropInResult properties to update your UI
                // result.paymentOptionType
                // result.paymentMethod
                // result.paymentIcon
                // result.paymentDescription
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }

    
    func payemntSuccessScreen(){
        let successVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
        if isZeroPayment == true{
            successVC.amount = totalAmount
            successVC.orderID = finalPaymentStatus?.orderId ?? "Wallet"
        }else{
            if isSelectedPayPal == true{
                successVC.amount = self.paymentResultRes?.response.transaction.amount
                successVC.orderID = self.paymentResultRes?.response.transaction.id
                successVC.customerInfo =  self.paymentResultRes?.response.transaction.customer
            }else{
                successVC.amount = totalAmount
                successVC.orderID = finalPaymentStatus?.orderId ?? "Wallet"
            }
        }
        self.navigationController?.pushViewController(successVC, animated: true)
//        PaymentSuccessViewController
    }
    
    func showBrainTreePaymentResult(response:BrainTreePlaceOrderRes){
        if response.status == 0 {
            self.showErrorAlert(message: "failed order")
        }
        else if response.paymentStatus == "0"{
            self.showErrorAlert(message: "Payment Filed")
        }
        else if response.cartStatus == 3 {
        
        }
        else{
            if response.status == 1 && response.paymentStatus == "1"{
                self.getOderDetails(orderId: String(response.orderId))
            }
        }
        
    }
    
    func getOderDetails(orderId: String){
        self.orderId = orderId
           if self.isNetWorkAvailable{
               self.addLoadingIndicator()
               let serviceManager = ShopWebServiceManager()
               serviceManager.managerDelegate = self
               serviceManager.orderDetails(orderId: orderId)
           }
       }
       
    func showSuccesSreenForPaypal(){
        let successVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentSuccessViewController") as! PaymentSuccessViewController
        successVC.amount = totalAmount
        successVC.orderID = self.orderId
        self.navigationController?.pushViewController(successVC, animated: true)
    }
    
    
//    getPaymentAccessToken()
}


extension PlaceOrderViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            if let serviceManager = manager as? ShopWebServiceManager{
                if serviceManager.serviceType == .CheckPrivateEvent{
                    self.getClientAccessToken()
                }else{
                    self.showErrorAlert(message: response.error ?? " ")
                }
            }
        }else{
            if let serviceManager = manager as? ShopWebServiceManager{
                if serviceManager.serviceType == .GetPaymentClientAccessToken{
                    if let data = response.data?.responseModel as? PaymentClientAccessToken{
                        clienbtAccessTokenInfo = data
//                        if isSelectedPayPal{
//                            self.showDropIn(clientTokenOrTokenizationKey: clienbtAccessTokenInfo?.clientToken ?? "")
//                        }else{
//                            self.payemtDoneApi()
//                        }
                    }
                }
                else if serviceManager.serviceType == .IntiatePayment{
                    if let data = response.data?.responseModel as? BrainTreePlaceOrderRes{
                        self.showBrainTreePaymentResult(response:data)
//                        self.customerInfo = data.response.transaction.customer
//                        self.paymentResultRes = data
//                        self.payemtDoneApi()
                    }
                }
                
                else if serviceManager.serviceType == .DonePayment{
                    if let data = response.data?.responseModel as? PaymentDone{
//                        self.orderID = data.
                        self.finalPaymentStatus = data
                        self.payemntSuccessScreen()
                    }
                }
                else if serviceManager.serviceType == .CheckPrivateEvent{
                    if let data = response.data?.responseModel as? Bool{
                        if data == true{
                            self.isDisabledWallet = true
                            self.tableView.reloadData()
                        }
                    }
                    self.getClientAccessToken()
                }
                
                else if serviceManager.serviceType == .OrderDetails{
                    self.showSuccesSreenForPaypal()
                }
            }
//            if let data = response.data?.responseModel as? ApiResponse{
//                if let msg = data.msg{
//                    self.showfloatingAlert(message:msg)
//                    self.navigationController?.popViewController(animated: true)
//                }
//            }
        }
    }
    
}


extension PlaceOrderViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if disablePaymentMethods == true{
            return 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2{
            if self.isDisabledWallet{
                return 1
            }else{
                return 2
            }
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 80
        }
        else if indexPath.section == 1{
            return 140
        }
        else if indexPath.section == 2{
            if indexPath.row == 0{
                return 250
            }else{
                return 100
            }
        }
        return 90

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0{
            let quantityCell = tableView.dequeueReusableCell(withIdentifier:"StatusCell",for: indexPath)
            return quantityCell
        }
        else if indexPath.section == 1{
            let cartItemCell = tableView.dequeueReusableCell(withIdentifier:"CartTotalTableViewCell",for: indexPath) as! CartTotalTableViewCell
            cartItemCell.subTotalLbl.text =  "$" + self.subTotalAmount
            cartItemCell.couponLbl.text = ""
            cartItemCell.couponLbl.text?.append(coupon ?? "$0.00")
//            let amountStr = String(format: "%.2f", self.totalAmount)
            cartItemCell.total.text =  "$" + self.totalAmount
            cartItemCell.walletAmntLbl.text =  "$"
            cartItemCell.walletAmntLbl.text?.append(self.reducedWalletAmount ?? "0.00")
            if disablePaymentMethods == false{
                if isReducedWalletAmount == false{
                    if isSelectedPayPal == false{
                        cartItemCell.walletAmntLbl.text =  "$"
                        cartItemCell.walletAmntLbl.text?.append(self.totalAmount ?? "0.00")
                        cartItemCell.total.text =  "$0.00"
                    }
                }
            }
            return cartItemCell
        }
        else if indexPath.section == 2{
            if indexPath.row == 0{
                let cartItemCell = tableView.dequeueReusableCell(withIdentifier:"PayementMethodTableViewCell",for: indexPath) as! PayementMethodTableViewCell
//                if self.isSelectedPayPal{
//                    cartItemCell.selectBtn.isSelected = self.isSelectedPayPal
////                    cartItemCell.selectBtn.isEnabled = false
//                }else{
////                    cartItemCell.selectBtn.isEnabled = true
//                    cartItemCell.selectBtn.isSelected = false
//                }
                cartItemCell.selectBtn.isSelected = self.isSelectedPayPal
                cartItemCell.selectBtn.isUserInteractionEnabled = !self.isSelectedPayPal

                return cartItemCell
            }else{
                let cartItemCell = tableView.dequeueReusableCell(withIdentifier:"WalletSelectionCell",for: indexPath) as! PayementMethodTableViewCell
//                if self.isSelectedPayPal{
//                    cartItemCell.selectBtn.isSelected = true
//                    cartItemCell.selectBtn.isEnabled = false
//                }else{
//                    cartItemCell.selectBtn.isEnabled = false
//                    cartItemCell.selectBtn.isSelected = true
//                }
                cartItemCell.amountLbl.text = "$"
                cartItemCell.amountLbl.text?.append( AppSharedData.sharedInstance.userDetails?.walletAmount ?? "0")
                cartItemCell.selectBtn.isSelected = !self.isSelectedPayPal
                cartItemCell.selectBtn.isUserInteractionEnabled = self.isSelectedPayPal

                return cartItemCell
            }
        }
        else{
            let quantityCell = tableView.dequeueReusableCell(withIdentifier:"StatusCell",for: indexPath)
            return quantityCell
        }
    }
}
