//
//  PaymentSuccessViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 22/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class PaymentSuccessViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    var amount : String!
    var orderID :String!
    var customerInfo : Customer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Cart"
        self.navigationItem.hidesBackButton = true
        let doneBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonClicked))
        self.navigationItem.rightBarButtonItem = doneBarButtonItem
        tableView.tableFooterView = UIView()
        self.clearCart()
        // Do any additional setup after loading the view.
    }
    
    @objc func doneButtonClicked(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    

    func clearCart(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.clearCart()
        }
    }
    

}

extension PaymentSuccessViewController: UITableViewDelegate,UITableViewDataSource {
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 80

        }else{
            return 400
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.row == 0 {
            let quantityCell = tableView.dequeueReusableCell(withIdentifier:"StatusCell",for: indexPath)
            return quantityCell
        }else{
            let infoCell = tableView.dequeueReusableCell(withIdentifier:"PayementSucessInfoTableViewCell",for: indexPath) as! PayementSucessInfoTableViewCell
             infoCell.amountLbl.text = "$"
            infoCell.amountLbl.text?.append(amount)
            infoCell.orderNoLbl.text = "#"
            infoCell.orderNoLbl.text?.append(orderID)
            if customerInfo != nil{
                var name : String =  customerInfo?.firstName ?? ""
                name.append(" ")
                name.append(customerInfo?.lastName ?? "")
                infoCell.nameLbl.text = name
                infoCell.emailLbl.text = customerInfo?.email
            }else{
                var name : String =  UserInfo.currentUser()?.fName ?? ""
                name.append(" ")
                name.append(UserInfo.currentUser()?.lName ?? "")
                infoCell.nameLbl.text = name
                infoCell.emailLbl.text = UserInfo.currentUser()?.userEmail
            }
            let date = Date()
            let format = DateFormatter()
            format.dateFormat = "dd-MM-YYYY"
            let formattedDate = format.string(from: date)
            infoCell.dateLbl.text = formattedDate
            return infoCell
        }
    }
}

extension PaymentSuccessViewController:WebServiceTaskManagerProtocol{
    
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
//            self.showErrorAlert(message: response.error ?? " ")
        }else{
        
        }
        
    }
}
