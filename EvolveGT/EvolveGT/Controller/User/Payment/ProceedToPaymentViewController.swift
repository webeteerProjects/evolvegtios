
//
//  ProceedToPaymentViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 20/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

enum ProceedPaymentCellSection {
    case StatusSection
    case CartSummaryLabel
    case CartItems
    case CartTotal
    case ApplyOfferCode
    case RemoveOfferCode
    case AddAddress
    case EditAddress
    case WalletAmount
}


class ProceedToPaymentViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkOutBtn: UIButton!
    var cartListRes : CartResponse?
    var cartList : [CartItem]?
    var tableSections = [ProceedPaymentCellSection.StatusSection]
    var cartTotalPrice = ""
    var cartPreviewToatalNumberOfCell = 0
    var addedPromoCode = ""
    var addedPromocoupunBalance = ""
    var addedPromoDetails:PromoResponse?
    var isAddressEdit = false
    var totalPrice : Float!
    var amountToPaid : Float!
    var subTotalValue = ""
    var couponAmount = "$0.00"
    var isUserHasBillingAddress = false

    var isWalletEnabled = false

    var isAddedPromoCode = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Checkout"
        amountToPaid = 0.0
        totalPrice = 0.0
        setupTableView()
        createTableSection()
        calculateToatlPrice()
        // Do any additional setup after loading the view.
    }
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AppSharedData.sharedInstance.isRefreshSideMenu == true{
            tableView.reloadData()
        }

        getCartListData()
//        getUserDetails()
    }
    
    
    private func getUserDetails() {
        guard isNetWorkAvailable else { return }
//        addLoadingIndicator()
        let serviceManager = LoginServiceManager()
        serviceManager.serviceType = .UserDetails
        serviceManager.managerDelegate = self
        serviceManager.userDetails()
    }
   
    
    func createTableSection(){
        tableSections =  [ProceedPaymentCellSection]()
        tableSections.append(.StatusSection)
        tableSections.append(.CartItems)
        if isAddedPromoCode{
            tableSections.append(.RemoveOfferCode)
        }else{
            tableSections.append(.ApplyOfferCode)
        }
        if AppSharedData.sharedInstance.userDetails != nil{
            var haveAddress = false
            isUserHasBillingAddress = false
            if let addr = AppSharedData.sharedInstance.userDetails?.billingAddress1{
                if addr.isValidString{
                    haveAddress = true;
                }
            }
            if let addr = AppSharedData.sharedInstance.userDetails?.billingAddress2{
                if addr.isValidString{
                    haveAddress = true;
                }
            }
            if haveAddress{
                isUserHasBillingAddress = true
                tableSections.append(.EditAddress)
            }else{
                tableSections.append(.AddAddress)
            }
        }else{
            tableSections.append(.AddAddress)
        }
        tableSections.append(.WalletAmount)
        self.tableView.reloadData()
    }
    
    func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView =  UIView()
//        var frame = CGRect.zero
//        frame.size.height = .leastNormalMagnitude
//        tableView.tableHeaderView = UIView(frame: frame)

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 130
        checkOutBtn.showRoundBorder()
    }
    
    func getCartListData(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getCardItems()
        }
    }
    
    @IBAction func paymentBtnClicked(sender: UIButton){
        
        if isUserHasBillingAddress == false{
            self.showfloatingAlert(message: "Please add billing address")
            return;
        }
        let paymentVC = self.storyboard?.instantiateViewController(withIdentifier:"PlaceOrderViewController" ) as! PlaceOrderViewController
        paymentVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(paymentVC, animated: true)
        paymentVC.subTotalAmount =  subTotalValue
        paymentVC.coupon = couponAmount
        if let walletBalance = Float(AppSharedData.sharedInstance.userDetails?.walletAmount ?? "0.00"){
            if isWalletEnabled == true{
                paymentVC.isDisabledWallet = !isWalletEnabled
                if walletBalance > 0{
                    if walletBalance <= amountToPaid {
                        amountToPaid = amountToPaid - walletBalance
                        paymentVC.isReducedWalletAmount = true
                        paymentVC.reducedWalletAmount = AppSharedData.sharedInstance.userDetails?.walletAmount ?? "0.00"
                        paymentVC.isDisabledWallet = true
                    }else{
//                        paymentVC.isDisabledWallet = true
                    }
                }else{
                    paymentVC.isDisabledWallet = true
                }
            }else{
                paymentVC.isDisabledWallet = true
            }
        }else{
            paymentVC.isDisabledWallet = !isWalletEnabled
        }
        paymentVC.isAddedPromo = self.isAddedPromoCode
        paymentVC.couponCode = addedPromoCode
        let amountStr = String(format: "%.2f", amountToPaid)
        paymentVC.totalAmount =  amountStr
        if amountToPaid == 0.0{
            paymentVC.disablePaymentMethods = true
        }
        
//        paymentVC.isDisabledWallet = isAddedArchievCard

    }
    
    @IBAction func promoApplyBtnClicked(sender: UIButton){
        if addedPromoCode.isValidString{
            if self.isNetWorkAvailable{
                self.addLoadingIndicator()
                let serviceManager = ShopWebServiceManager()
                serviceManager.managerDelegate = self
                serviceManager.addPromo(promo: addedPromoCode)
            }
        }else{
            self.showfloatingAlert(message: "Please enter valid coupon code")
        }
    }

    @IBAction func removePromoBtnClicked(sender: UIButton){
        addedPromoCode = ""
        calculateToatlPrice()
        isAddedPromoCode = false
        self.addedPromoDetails = nil
        self.createTableSection()
    }
    
    @IBAction func addAddressBtnClicked(sender: UIButton){
        let profileStory = UIStoryboard.init(name: "MyProfile", bundle: nil)
        let detailEditController = profileStory.instantiateViewController(withIdentifier: "ProfileDetailEditController") as! ProfileDetailEditController
        detailEditController.addressType = .billing
        detailEditController.userDetails =  AppSharedData.sharedInstance.userDetails
        navigationController?.pushViewController(detailEditController, animated: true)
    }
    
    @IBAction func editAddressBtnClicked(sender: UIButton){
        let profileStory = UIStoryboard.init(name: "MyProfile", bundle: nil)
        let detailEditController = profileStory.instantiateViewController(withIdentifier: "ProfileDetailEditController") as! ProfileDetailEditController
        detailEditController.addressType = .billing
        detailEditController.userDetails =  AppSharedData.sharedInstance.userDetails
        navigationController?.pushViewController(detailEditController, animated: true)
    }
    
    func applyPromoAmount(){
        calculateToatlPrice()
        if let promoVal = Float(addedPromoDetails?.remain ?? "0.0"){
            if promoVal >= totalPrice {
                let amountStr = NSString(format: "%.2f", totalPrice)
                couponAmount =  "$" + (amountStr as String)
                amountToPaid = 0
            }else{
                let amountStr = NSString(format: "%.2f", promoVal)
                couponAmount =  "$" + (amountStr as String)
                amountToPaid = totalPrice - promoVal
            }
        }
        let amountStr = NSString(format: "%.2f", amountToPaid)

        cartTotalPrice = "$" + (amountStr as String)
    }
    
    func calculateToatlPrice(){
        if self.cartList != nil{
            totalPrice = 0
            for item in self.cartList! {
                if item.source == "archie"{
                    let qnty = Int(item.quantity ?? "1")
                    if let priceVal = Float(item.price ?? "0.00"){
                        let itemPrice = priceVal * Float(qnty ?? 1)
                        totalPrice = totalPrice + itemPrice
                    }
                }else{
                    if item.source == "event"{
                        totalPrice = totalPrice + item.totalAmount
                    }else{
                        if let priceVal = Float(item.price ?? "0.00"){
                            totalPrice = totalPrice + priceVal
                        }
                    }
                  
                }
               
            }
        }
        amountToPaid = totalPrice
        let amountStr = String(format: "%.2f", totalPrice)
        cartTotalPrice = "$" + amountStr
        subTotalValue =  String(format: "%.2f", amountToPaid) //String(amountToPaid)
    }
}


extension ProceedToPaymentViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
        }else{
            
            if let serviceManager = manager as? ShopWebServiceManager{
                if serviceManager.serviceType == .AddPromoCode{
                    if let data = response.data?.responseModel as? PromoResponse{
                        if data.status == 1{
                            self.addedPromoDetails = data
                            self.isAddedPromoCode = true
                            self.applyPromoAmount()
                            self.createTableSection()
                        }else{
                            if data.msg.isValidString{
                                self.showfloatingAlert(message: data.msg)
                            }else{
                                self.showErrorAlert(message: "Invalid coupon code")
                            }
                        }
                    }
                }
                else if serviceManager.serviceType == .CartList{
                    if let data = response.data?.responseModel as? CartResponse{
                        self.cartListRes =  data
                        self.isWalletEnabled = data.walletEnabled
                        cartList =  self.cartListRes?.data
                        let quantityStr = self.cartListRes?.wallet ?? " "
                        self.cartList = self.cartListRes?.data
                        var totalPrice : Float = 0.0
                        if self.cartList != nil{
                            for item in self.cartList! {
                                if let priceVal = Float(item.price ?? "0.00"){
                                    let totalPrice = totalPrice + priceVal
                                }
                            }
                        }
                        if self.isAddedPromoCode == true{
                            self.applyPromoAmount()
//                            self.createTableSection()
                        }else{
                            calculateToatlPrice()
                        }
                        self.createTableSection()
//                        self.tableView.reloadData()
                    }
                }
            }
            
            else if let loginServiceManager = manager as? LoginServiceManager{
                if loginServiceManager.serviceType == .UserDetails{
                    if let data = response.data?.responseModel as? UserDetailsRes{
                        AppSharedData.sharedInstance.userDetails = data.result
                        self.createTableSection()
                    }
                }

            }

            
//            if let data = response.data?.responseModel as? ApiResponse{
//                self.getCartListData()
//            }
        }
    }
    
}

extension ProceedToPaymentViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableSections[section] == .StatusSection{
            return 1
        }
        else  if tableSections[section] == .CartItems{
            let totalRows = cartList?.count ?? 0
            cartPreviewToatalNumberOfCell = totalRows + 2
            if totalRows > 0 {
                return totalRows + 2
            }else{
                return totalRows
            }
        }
        else  if tableSections[section] == .ApplyOfferCode{
            return 1
        }
        else  if tableSections[section] == .RemoveOfferCode{
            return 1
        }
        else  if tableSections[section] == .AddAddress{
            return 1
        }
        else  if tableSections[section] == .EditAddress{
            return 1
        }
        else  if tableSections[section] == .WalletAmount{
            return 1
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableSections[indexPath.section] == .StatusSection{
            return 80
        }
        else  if tableSections[indexPath.section] == .CartSummaryLabel{
            return 45
        }
        else  if tableSections[indexPath.section] == .CartItems{
            if indexPath.row == 0{
                return 45
            }
            else{
                if ((cartPreviewToatalNumberOfCell - 1) == indexPath.row){
                    return 60
                }else{
                    return 90
                }
            }
        }
        else  if tableSections[indexPath.section] == .ApplyOfferCode{
            return 90
        }
        else  if tableSections[indexPath.section] == .RemoveOfferCode{
            return 90
        }
        else  if tableSections[indexPath.section] == .CartTotal{
            return 60
        }
        else  if tableSections[indexPath.section] == .AddAddress{
            return 100
        }
        else  if tableSections[indexPath.section] == .EditAddress{
            return UITableView.automaticDimension
        }
        else  if tableSections[indexPath.section] == .WalletAmount{
            return 80
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableSections[indexPath.section] == .StatusSection{
            let quantityCell = tableView.dequeueReusableCell(withIdentifier:"StatusCell",for: indexPath)
            return quantityCell
        }
        else if tableSections[indexPath.section] == .CartItems{
            if indexPath.row == 0{
                let quantityCell = tableView.dequeueReusableCell(withIdentifier:"CartSummaryLabel",for: indexPath)
                return quantityCell
            }
            else if ((cartPreviewToatalNumberOfCell - 1) == indexPath.row){
                let cartToatlPriceTableViewCell = tableView.dequeueReusableCell(withIdentifier:"CartToatlPriceTableViewCell",for: indexPath) as! CartToatlPriceTableViewCell
                cartToatlPriceTableViewCell.subTotalPriceLbl.text = cartTotalPrice
                cartToatlPriceTableViewCell.totalPriceLbl.text = cartTotalPrice
                return cartToatlPriceTableViewCell
            }else{
                let cartItemCell = tableView.dequeueReusableCell(withIdentifier:"CartPreviewTableViewCell",for: indexPath) as! CartPreviewTableViewCell
                cartItemCell.itemInfo = self.cartList?[indexPath.row - 1]
//                if cartItemCell.itemInfo?.source == "archie"{
//                    self.isWalletEnabled = true
//                }
                cartItemCell.showData()
                return cartItemCell;
            }
        }
        else if tableSections[indexPath.section] == .ApplyOfferCode{
            let quantityCell = tableView.dequeueReusableCell(withIdentifier:"AddPomoCell",for: indexPath) as! AddPromoTableViewCell
            quantityCell.addpromoTxtFld.text = addedPromoCode
            return quantityCell
        }
        else if tableSections[indexPath.section] == .RemoveOfferCode{
            let cartItemCell = tableView.dequeueReusableCell(withIdentifier:"RemovePromoCell",for: indexPath) as! CartPreviewTableViewCell
            cartItemCell.quantityLbl.text = "Coupon Code: " + addedPromoCode
            cartItemCell.toatlPriceLbl.text = "Coupon Balance: "
            cartItemCell.toatlPriceLbl.text?.append(addedPromoDetails?.remain ?? "")
            return cartItemCell;
        }
        else if tableSections[indexPath.section] == .AddAddress{
            let addressCell = tableView.dequeueReusableCell(withIdentifier:"AddBillingAddress",for: indexPath) as! AddressTableViewCell
            addressCell.addressBtn.setTitle("Add", for: .normal)
            addressCell.addressLbl.text = "No billing address found.Please add a new address"
            return addressCell
        }
        else if tableSections[indexPath.section] == .EditAddress{
            let addressCell = tableView.dequeueReusableCell(withIdentifier:"AddBillingAddress",for: indexPath) as! AddressTableViewCell
            addressCell.addressBtn.setTitle("Edit", for: .normal)
            addressCell.addressLbl.text = self.getBillingAddress()
//                AppSharedData.sharedInstance.userDetails?.billingAddress1
            return addressCell
        }
        else if tableSections[indexPath.section] == .WalletAmount{
            let walletCell = tableView.dequeueReusableCell(withIdentifier:"WalletAmountTableViewCell",for: indexPath) as! WalletAmountTableViewCell
            walletCell.amountLbl.text = "$"
            walletCell.amountLbl.text?.append(AppSharedData.sharedInstance.userDetails?.walletAmount ?? "0.00") 
            return walletCell
        }

        else {
            let quantityCell = tableView.dequeueReusableCell(withIdentifier:"StatusCell",for: indexPath)
            return quantityCell
        }
      
    }
    
    func getBillingAddress() -> String{
        var address = ""
        let userDetails = AppSharedData.sharedInstance.userDetails
        if  userDetails?.billingAddress1 == nil{
            return address
        }else{
            if let fname = userDetails?.billingFirstName{
                address.append(fname)
            }
            if let fname = userDetails?.billingLastName{
                address.append(" ")
                address.append(fname)
            }
            if let fname = userDetails?.billingAddress1{
                address.append("\n")
                address.append(fname)
            }
            if let fname = userDetails?.billingAddress2{
                address.append("\n")
                address.append(fname)
            }
            if let fname = userDetails?.billingCity{
                address.append("\n")
                address.append(fname)
            }
            if let fname = userDetails?.billingStateName{
                address.append("\n")
                address.append(fname)
            }
            if let fname = userDetails?.billingPostcode{
                address.append("\n")
                address.append(fname)
            }
            if let fname = userDetails?.billingCountryName{
                address.append("\n")
                address.append(fname)
            }
            if let fname = userDetails?.billingPhone{
                address.append("\n")
                address.append(fname)
            }
            if let fname = userDetails?.billingEmail{
                address.append("\n")
                address.append(fname)
            }
        }
        
        return address
    }
}



extension ProceedToPaymentViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        addedPromoCode = textField.text ?? ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let text: NSString = (textField.text ?? "") as NSString
        addedPromoCode = text.replacingCharacters(in: range, with: string)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
