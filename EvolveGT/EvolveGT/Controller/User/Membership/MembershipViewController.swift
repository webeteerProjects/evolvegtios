//
//  MembershipViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 26/11/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class MembershipViewController: SideMenuAccessibleController {
    @IBOutlet weak var collectionView: UICollectionView!
    var memberships : [Membership]?
    var selectedIndexPath : IndexPath?
    var preSelectedIndexPath : IndexPath?

    var currentPlan:UserMembership?
    var season:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Membership"
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserMembershipDetails()
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
        super.viewWillDisappear(animated)
    }
    
    
    
    
    func getUserMembershipDetails(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getUserMembershipDetails()
        }
    }
    
    
    func getMembershipsListata(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getMembershipList()
        }
    }
    
    func getCartListData(){
        if self.isNetWorkAvailable{
            let serviceManager = ShopWebServiceManager()
            serviceManager.getCardItems()
        }
    }
    
//
    
    @IBAction func purchaseButtonClicked(sender: UIButton){
        if let data = self.memberships?[sender.tag]{
            if self.isNetWorkAvailable{
                self.addLoadingIndicator()
                let serviceManager = ShopWebServiceManager()
                serviceManager.managerDelegate = self
                serviceManager.addMembership(title: data.title, membership: data.slug, price: data.price, image: data.image)
            }
        }
    }
    
    
    @IBAction func viewMoreButtonClicked(sender: UIButton){
        if let data = self.memberships?[sender.tag]{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MembershipDetailsViewController") as! MembershipDetailsViewController
            vc.slug = data.slug
            vc.currentPlan = self.currentPlan
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}


extension MembershipViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return self.memberships?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = ((Constant.ScreenSize.SCREEN_WIDTH - 52) / 2 )
        return CGSize(width: cellWidth, height: cellWidth + 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        var cell : MembershipCollectionViewCell!
        cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "MembershipCollectionViewCell", for: indexPath as IndexPath) as! MembershipCollectionViewCell)
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        cell.memberShipData = self.memberships?[indexPath.row]
        cell.currentPlan = self.currentPlan
        cell.season = self.season
        if let hightLightCell = self.selectedIndexPath{
            if hightLightCell.row == indexPath.row{
                cell.showSelection(status: true)
            }else{
                cell.showSelection(status: false)
            }
        }
        cell.viewMoreBtn.tag = indexPath.row
        cell.purchaseBtn.tag = indexPath.row
        cell.showData()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        preSelectedIndexPath = selectedIndexPath
        selectedIndexPath = indexPath
        if preSelectedIndexPath != nil{
            var cell : MembershipCollectionViewCell =  self.collectionView.cellForItem(at: preSelectedIndexPath!) as! MembershipCollectionViewCell
            cell.showSelection(status: false)
        }
        var cell : MembershipCollectionViewCell =  self.collectionView.cellForItem(at: selectedIndexPath!) as! MembershipCollectionViewCell
        cell.showSelection(status: true)

//        self.collectionView.reloadData()
        //        let detailsView = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailedViewController") as! EventDetailedViewController
        //        detailsView.eventName = self.eventList?[indexPath.row].title
        //        detailsView.eventSlug = self.eventList?[indexPath.row].slug ?? ""
        //        self.navigationController?.pushViewController(detailsView, animated: true)
    }
}


extension MembershipViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
        }else{
            if let data = response.data?.responseModel as? MembershipListRes{
                self.memberships =  data.memberships
                self.season = data.season
                self.collectionView.reloadData()
            }
            if let data = response.data?.responseModel as? UserMembership{
                currentPlan = data
                getMembershipsListata()
            }
            
            if let data = response.data?.responseModel as? AddMembership{
                self.showfloatingAlert(message: data.msg ?? "")
                getCartListData()
                getUserMembershipDetails()
            }
            
        }
    }
}
