//
//  MembershipDetailsViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 26/11/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import WebKit
import Kingfisher


class MembershipDetailsViewController: UIViewController{
    @IBOutlet weak var wkwebview: WKWebView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var purchaseBtn: UIButton!
    @IBOutlet var webViewHeight: NSLayoutConstraint!
    var membershipDetails: MembershipDetails?
    var slug : String?
    var currentPlan:UserMembership?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMembershipsDetails()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    func loadContentWebView(){
        let request = URLRequest(url: URL(string: "**your URL**")!)
        wkwebview?.load(request)
    }
    
    func getMembershipsDetails(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getMembershipListDetails(slug:self.slug ?? "")
        }
    }
    
    @IBAction func purchaseButtonClicked(sender: UIButton){
        if let data = self.membershipDetails{
            if self.isNetWorkAvailable{
                self.addLoadingIndicator()
                let serviceManager = ShopWebServiceManager()
                serviceManager.managerDelegate = self
                serviceManager.addMembership(title: data.title, membership: data.slug, price: data.price, image: data.image)
            }
        }
    }
    
    func showDetails(){
        if let imgUrl = membershipDetails?.image {
            imageView.kf.setImage(with: URL(string : imgUrl),
                                  options: [.transition(ImageTransition.fade(1))])
        }
        titleLbl.text = membershipDetails?.title
        if let htmlData = membershipDetails?.descriptionField {
            wkwebview.loadHTMLString(htmlData, baseURL: nil)
        }
        
        //
        if let status = membershipDetails?.stockStatusStr {
            if status != "instock"{
                purchaseBtn.isEnabled = false
                purchaseBtn.isUserInteractionEnabled = false
                purchaseBtn.backgroundColor = UIColor.gray
                purchaseBtn.setTitleColor(UIColor.white, for: .disabled)
                purchaseBtn.setTitle("SOLD OUT", for: .normal)
                
            }else{
                purchaseBtn.isUserInteractionEnabled = true
                purchaseBtn.isEnabled = true
                purchaseBtn.backgroundColor =  UIColor.init(hexString: "#28AC10")
                purchaseBtn.setTitleColor(UIColor.white, for: .normal)
                purchaseBtn.setTitle("PURCHASE", for: .normal)
            }
        }
        
        if currentPlan?.memberShipIdValue ?? 0  == membershipDetails?.memberShipIdValue ?? 0{
            purchaseBtn.isUserInteractionEnabled = false
            purchaseBtn.backgroundColor =  UIColor.init(hexString: "#28AC10")
            purchaseBtn.setTitle("CURRENT PLAN", for: .normal)
        }
        else  if currentPlan?.memberShipIdValue ?? 0  > membershipDetails?.memberShipIdValue ?? 0{
            purchaseBtn.isUserInteractionEnabled = false
            purchaseBtn.isHidden = true;
        }
        else{
            if membershipDetails?.title == "GUEST"{
                purchaseBtn.isHidden = true;
            }
        }
        
    }
    
}

extension MembershipDetailsViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
        }else{
            if let data = response.data?.responseModel as? MembershipDetails{
                self.membershipDetails =  data
                self.showDetails()
                //                self.collectionView.reloadData()
            }
            if let data = response.data?.responseModel as? AddMembership{
                self.showfloatingAlert(message: data.msg ?? "")
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}


extension MembershipDetailsViewController:WKNavigationDelegate{
    
    //MARK:- WKNavigationDelegate
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    
    
    private func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
    }
    
    private func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        print("finish to load")
        self.wkwebview.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.wkwebview.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    self.webViewHeight.constant = height as! CGFloat
                })
            }
            
        })
    }
}
