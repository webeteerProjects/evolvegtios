
//
//  AddtoCartViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 18/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher

class AddtoCartViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addtoCartBtn: UIButton!
    var name : String?
    var email : String?

    var itemInfo : CardInfo?
    var isGiftCard = false
    var userSelectedQuanity = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        self.title = itemInfo?.title
    }
    
    
    func showBadgeCount(){
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3] as! UITabBarItem
            if AppSharedData.sharedInstance.cartCount > 0{
                tabItem.badgeValue = String(AppSharedData.sharedInstance.cartCount)
            }else{
                tabItem.badgeValue = nil
            }
        }
    }
    
    @IBAction func addToCartBtnClicked(){
        if self.isLoggedInUser{

        
        if isGiftCard{
            guard let nameTxt = name, !nameTxt.isBlank else {
                self.showfloatingAlert(message: "Please enter name of receiver")
                
                //                self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterPassword)
                return
            }
            
            guard let emailTxt = email, !emailTxt.isBlank else {
                            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterEmail)
//                self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterEmail)
                return
            }
            guard let validEmail = email, validEmail.isValidEmail else {
                            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterValidEmail)
//                self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterValidEmail)
                return
            }
          
        }
        
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            var dict = [String : AnyObject]()
            dict.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "serial")
            dict.updateValue(itemInfo?.title as AnyObject? ?? "" as AnyObject, forKey: "title")
            dict.updateValue(itemInfo?.slug as AnyObject? ?? "" as AnyObject, forKey: "slug")
            dict.updateValue(itemInfo?.price as AnyObject? ?? "" as AnyObject, forKey: "price")
            dict.updateValue(userSelectedQuanity as AnyObject? ?? "" as AnyObject, forKey: "quantity")
            if isGiftCard{
                dict.updateValue(itemInfo?.image as AnyObject? ?? "" as AnyObject, forKey: "image")
                dict.updateValue(name as AnyObject? ?? "" as AnyObject, forKey: "name")
                dict.updateValue(email as AnyObject? ?? "" as AnyObject, forKey: "email")
                serviceManager.addGiftCardToCart(info: dict)
            }else {
                dict.updateValue("" as AnyObject, forKey: "image")
                serviceManager.addArchievCardToCart(info: dict)
            }
        }
        }
        
    }

}

extension AddtoCartViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
//            self.showErrorAlert(message: response.error ?? " ")
        }else{
            if let data = response.data?.responseModel as? ApiResponse{
                if let msg = data.msg{
                    self.showfloatingAlert(message:msg)
                    getCartListData()
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func getCartListData(){
        if self.isNetWorkAvailable{
            let serviceManager = ShopWebServiceManager()
            serviceManager.getCardItems()
        }
    }
    
    
}

extension AddtoCartViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 220;
        }
        else if indexPath.row == 1 {
            return 80;
        }
        else if indexPath.row == 2 {
            if isGiftCard {
                return 180;
            }else {
                return 150;
            }
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0 {
            let imgCell = tableView.dequeueReusableCell(withIdentifier:"ImageTableViewCell",for: indexPath) as! ImageTableViewCell
            if let imgUrl = itemInfo?.image{
                imgCell.imgView.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
            }
            return imgCell;
        }
        else if indexPath.row == 1 {
            let infoCell = tableView.dequeueReusableCell(withIdentifier:"ItemInfoTableViewCell",for: indexPath) as! ItemInfoTableViewCell
            if isGiftCard {
                infoCell.titleLbl.text = itemInfo?.title
                infoCell.priceLbl.text = "Price : $"
                infoCell.priceLbl.text?.append(itemInfo?.price ?? "")
            }else{
                infoCell.titleLbl.text = itemInfo?.content
                infoCell.priceLbl.text = "Price : $"
                infoCell.priceLbl.text?.append(itemInfo?.price ?? "0")
            }
            return infoCell;
        }else{
            
            if isGiftCard{
                let quantityCell = tableView.dequeueReusableCell(withIdentifier:"InputInfoCell",for: indexPath) as! InputInfoTableViewCell
                quantityCell.emailTextfld.tag  = 2
                quantityCell.emailTextfld.text = email
                quantityCell.nameTextfld.tag  = 1
                quantityCell.nameTextfld.text = name
                quantityCell.nameTextfld.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                quantityCell.emailTextfld.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)


                return quantityCell
            }else{
                let quantityCell = tableView.dequeueReusableCell(withIdentifier:"QuantityTableViewCell",for: indexPath) as! QuantityTableViewCell
                quantityCell.delegate = self
                if let priceVal = Float(itemInfo?.price ?? "0.00"){
                    quantityCell.price = priceVal
                }
                quantityCell.showValues()
                return quantityCell
            }
        }
    }
}

extension AddtoCartViewController: QuantityCellDelegate, UITextFieldDelegate {
    func didSelectNoQuantity(quanity:Int){
        userSelectedQuanity = quanity
        if quanity == 0 {
            self.addtoCartBtn.isEnabled = false
            addtoCartBtn.backgroundColor = .gray
        } else{
            self.addtoCartBtn.isEnabled = true
            addtoCartBtn.backgroundColor = UIColor.init(hexString: "#08A53A")
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1{
            name = textField.text
        }else{
            email = textField.text
        }
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        if let text = textfield.text {
            if textfield.tag == 1{
                name = text
            }else{
                email = text
            }
        }
    }
}



