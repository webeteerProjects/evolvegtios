//
//  AboutUsViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 03/08/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class AboutUsViewController: SideMenuAccessibleController {
    @IBOutlet weak var gradiantView: UIView!
    @IBOutlet weak var versionLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "About US"
        var versionInfo = "Version "
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versionInfo.append(text)
        }
        versionLabel.text  = versionInfo
        setGradientBackground()
    }
    

    func setGradientBackground() {
        let colorTop =  UIColor.init(hexString: "ffffff").cgColor
        let colorBottom = UIColor.init(hexString: "93caa3").cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
}
