

//
//  ShopViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 10/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class ShopViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var categoryRes : CategoryListRes?
    var categoriesList : [ProductCategory]?
    var viewControllersList :[ProductListingViewController]?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Shop"
        tableView.tableFooterView = UIView()
        setupTableView()
        getCategoryList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    

    func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 560
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "HMenu"), for: UIControl.State.normal)
        button.addTarget(self, action:#selector(self.menuBtnClicked) , for: .touchUpInside)
        button.frame =  CGRect.init(x: 0, y: 0, width: 45, height: 45)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
        
    }
    
    func getCategoryList(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getProCategoryList()
        }
    }
    
    
    @objc func menuBtnClicked(){
        self.sideMenuController?.revealMenu()
    }
    
    @IBAction func cardsBtnClicked(){
        let cardVC = self.storyboard?.instantiateViewController(withIdentifier: "CardsVC") as! CardsViewController
        cardVC.isGiftCard = false
        self.navigationController?.pushViewController(cardVC, animated: true)
//        CardsVC
    }
    
    @IBAction func rentalBtnClicked(){
        if categoriesList == nil{
            return
        }
        var data : ProductCategory?
        for obj in categoriesList!
        {
            if obj.id == "3" {
                data = obj
                break
            }
        }
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "ShopItesmTabViewController") as! ShopItesmTabViewController
        productListVC.categoryName = data?.title ?? ""
        productListVC.categoriesList = data?.children
        self.navigationController?.pushViewController(productListVC, animated: true)

    }
    
    @IBAction func giftBtnClicked(){
        let cardVC = self.storyboard?.instantiateViewController(withIdentifier: "CardsVC") as! CardsViewController
        cardVC.isGiftCard = true
        self.navigationController?.pushViewController(cardVC, animated: true)
    }
    
    @IBAction func gearBtnClicked(){
        if categoriesList == nil{
            return
        }
        var data : ProductCategory?
        for obj in categoriesList!
        {
            if obj.id == "2" {
                data = obj
                break
            }
        }
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "ShopItesmTabViewController") as! ShopItesmTabViewController
        productListVC.categoryName = data?.title ?? ""
        productListVC.categoriesList = data?.children
        self.navigationController?.pushViewController(productListVC, animated: true)

    }

}

extension ShopViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.size.height;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"TabCell",for: indexPath) as! GustDashBoardTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        ShopItesmTabViewController
    }
    
}


extension ShopViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            //            self.showErrorAlert(message: response.error ?? " ")
        }else{
            if let catRes = response.data?.responseModel as? CategoryListRes{
                self.categoryRes = catRes
                self.categoriesList = catRes.data
            }
        }
    }
    
}
