
//
//  UserEventsViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 10/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import RSSelectionMenu


class UserEventsViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var eventsData : EventListRes?
    var eventList : [Result]?
    var isGrid = false
    var eventListWithoutFilter : [Result]?
    var monthList = [String]()
    var eventTypeList = [String]()
    var isDismissingSortView = false
    var isGuestUser = false
    var gridButton : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        if isGuestUser == false{
            addHamburgerMenu()
        }
        addFilterMenu()
        self.navigationItem.title = "Events"

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isDismissingSortView{
            isDismissingSortView = false
        }else{
            getEventsListata()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func addHamburgerMenu(){
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "HMenu"), for: UIControl.State.normal)
        button.addTarget(self, action:#selector(self.menuBtnClicked) , for: .touchUpInside)
        button.frame =  CGRect.init(x: 0, y: 0, width: 45, height: 45)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
    }
    
    func addFilterMenu(){
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "filter"), for: UIControl.State.normal)
        button.addTarget(self, action:#selector(self.filterBtnClicked) , for: .touchUpInside)
        button.frame =  CGRect.init(x: 0, y: 0, width: 45, height: 45)
        let barButton = UIBarButtonItem(customView: button)
        
        gridButton = UIButton(type: UIButton.ButtonType.custom)
        if isGrid == true{
            gridButton.setImage(UIImage(named: "list_view"), for: UIControl.State.normal)
        }else{
            gridButton.setImage(UIImage(named: "grid_view"), for: UIControl.State.normal)
        }
        gridButton.addTarget(self, action:#selector(self.gridChangeBtnClicked) , for: .touchUpInside)
        gridButton.frame =  CGRect.init(x: 0, y: 0, width: 45, height: 45)
        let gridBarButton = UIBarButtonItem(customView: gridButton)
        self.navigationItem.rightBarButtonItems = [gridBarButton,barButton]
    }
    
   

    @objc func menuBtnClicked(){
        self.sideMenuController?.revealMenu()
    }
    
    @objc func gridChangeBtnClicked(){
        isGrid = !isGrid
        if isGrid == true{
            gridButton.setImage(UIImage(named: "list_view"), for: UIControl.State.normal)
        }else{
            gridButton.setImage(UIImage(named: "grid_view"), for: UIControl.State.normal)
        }
        collectionView.reloadData()
    }
    
    @objc func filterBtnClicked(){
        let alerController = UIAlertController(title: "Event Fliter", message: "", preferredStyle: .actionSheet)
        alerController.addAction(UIAlertAction(title: "By Event Month", style: .default, handler: {(action:UIAlertAction) in
            self.showEventMonthSelectionView()
        }));
        alerController.addAction(UIAlertAction(title: "By Event Type", style: .default, handler: {(action:UIAlertAction) in
            self.showEventTypeSelectionView()

        }));
        alerController.addAction(UIAlertAction(title: "Clear", style: .default, handler: {(action:UIAlertAction) in
            self.clearFilter()
        }));
        
        alerController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action:UIAlertAction) in
            
        }));
        if Constant.DeviceType.IS_IPAD {
            alerController.popoverPresentationController?.permittedArrowDirections = []
            alerController.popoverPresentationController?.sourceView = self.view
            alerController.popoverPresentationController?.sourceRect = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH / 2, y: Constant.ScreenSize.SCREEN_HEIGHT, width: 1.0, height: 1.0)
        }
        present(alerController, animated: true, completion: nil)
    }

    func showEventTypeSelectionView(){
        var simpleSelectedArray = [String]()
        if self.eventTypeList.count < 1{
            return
        }
        let simpleDataArray = self.eventTypeList
        let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
            cell.textLabel?.text = item
        }
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
            simpleSelectedArray = selectedItems
            if selectedItems != nil{
                self?.applyEventTypeFilter(key: simpleSelectedArray.first ?? "")
            }
        }
        selectionMenu.maxSelectionLimit = 1
        selectionMenu.cellSelectionStyle = .checkbox
        selectionMenu.title = "Filter by Event Type"
        self.isDismissingSortView = true
        selectionMenu.show(style: .present, from: self)
    }
    
    func showEventMonthSelectionView(){
        var simpleSelectedArray = [String]()
        if self.monthList.count < 1{
            return
        }
        let simpleDataArray = self.monthList
        let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
            cell.textLabel?.text = item
        }
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
            simpleSelectedArray = selectedItems
            if selectedItems != nil{
                self?.applyEventMonthFilter(key: simpleSelectedArray.first ?? "")
            }
        }
        selectionMenu.title = "Filter by Month"
        selectionMenu.maxSelectionLimit = 1
        selectionMenu.cellSelectionStyle = .checkbox
        self.isDismissingSortView = true
        selectionMenu.show(style: .present, from: self)
    }

    func clearFilter(){
        self.eventList = self.eventListWithoutFilter
        self.collectionView.reloadData()
    }
    
    func applyEventTypeFilter(key:String){
        if self.eventListWithoutFilter == nil{
            return
        }
        self.eventList = self.eventListWithoutFilter!.filter {
            $0.eventType.lowercased() == key.lowercased()
        }
        self.collectionView.reloadData()
    }
    
    func applyEventMonthFilter(key:String){
        if self.eventListWithoutFilter == nil{
            return
        }
        self.eventList = self.eventListWithoutFilter!.filter {
            $0.monthYear == key
        }
        self.collectionView.reloadData()
    }
    
    func getEventsListata(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = EventServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getEventList()
        }
    }
    
    func showListData(){
        self.collectionView.reloadData()
    }

    @IBAction func addTocartButtonClicked(sender: UIButton){
        
        if self.isLoggedInUser{
            if let eventData = self.eventList?[sender.tag]{
                if self.isNetWorkAvailable{
                    self.addLoadingIndicator()
                    let serviceManager = ShopWebServiceManager()
                    serviceManager.managerDelegate = self
                    var dict = [String : AnyObject]()
                    dict.updateValue(UserInfo.currentUser()?.userID as AnyObject , forKey: "serial")
                    dict.updateValue(eventData.eventDate as AnyObject , forKey: "event_date")
                    dict.updateValue(eventData.price as AnyObject , forKey: "event_price")
                    dict.updateValue(eventData.slug as AnyObject , forKey: "event")
                    dict.updateValue(UserInfo.currentUser()?.role as AnyObject , forKey: "role")
                    serviceManager.addEventToCart(info: dict)
                }
            }
        }
        
        
        /*
        if isGuestUser{
            let loginVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
            loginVC.isGuestUserLogin = true
            let nav = UINavigationController.init(rootViewController: loginVC)
            self.present(nav, animated: true, completion: nil)

        }else{
        
            if let eventData = self.eventList?[sender.tag]{
                if self.isNetWorkAvailable{
                    self.addLoadingIndicator()
                    let serviceManager = ShopWebServiceManager()
                    serviceManager.managerDelegate = self
                    var dict = [String : AnyObject]()
                    dict.updateValue(UserInfo.currentUser()?.userID as AnyObject , forKey: "serial")
                    dict.updateValue(eventData.eventDate as AnyObject , forKey: "event_date")
                    dict.updateValue(eventData.price as AnyObject , forKey: "event_price")
                    dict.updateValue(eventData.slug as AnyObject , forKey: "event")
                    dict.updateValue(UserInfo.currentUser()?.role as AnyObject , forKey: "role")
                    serviceManager.addEventToCart(info: dict)
                }
            }
            
        }
        
        */
    }
    
    func getCartListData(){
        if self.isNetWorkAvailable{
            let serviceManager = ShopWebServiceManager()
            serviceManager.getCardItems()
        }
    }
}


extension UserEventsViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return self.eventList?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isGrid {
            return CGSize(width: ((Constant.ScreenSize.SCREEN_WIDTH / 2 ) - 20), height: 240)
        }else{
            return CGSize(width: Constant.ScreenSize.SCREEN_WIDTH - 20, height: 150)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        var cell : EventsListCollectionViewCell!
        if isGrid {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCollection", for: indexPath as IndexPath) as! EventsListCollectionViewCell
        }else{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListCollection", for: indexPath as IndexPath) as! EventsListCollectionViewCell
        }
        cell.eventData = self.eventList?[indexPath.row]
        cell.cartBtn.tag = indexPath.row
        cell.showData()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let detailsView = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailedViewController") as! EventDetailedViewController
        detailsView.eventName = self.eventList?[indexPath.row].title
        detailsView.eventSlug = self.eventList?[indexPath.row].slug ?? ""
        self.navigationController?.pushViewController(detailsView, animated: true)
    }
}

extension UserEventsViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
        }else{
            if let serviceMngr = manager as? EventServiceManager{
                if let data = response.data?.responseModel as? EventListRes{
                    self.eventsData =  data
                    self.eventListWithoutFilter = self.eventsData?.results
                    self.eventList = self.eventListWithoutFilter
                    showListData()
                    getAllEventType()
                    getAllEventMonthYearType()
                    
                }
            }
            else if let serviceMngr = manager as? ShopWebServiceManager{
                if serviceMngr.serviceType == .AddEventToCart{
                    if let data = response.data?.responseModel as? ApiResponse{
                        self.getCartListData()
                        self.showfloatingAlert(message: data.msg)
                    }
                }
            }
         
        }
    }
    
   
    
}

extension UserEventsViewController{
    
    func getAllEventType(){
        if eventListWithoutFilter == nil{
            return
        }
        var dict = [String : String]()
        for obj in eventListWithoutFilter! {
            dict.updateValue(obj.eventType, forKey: obj.eventType.uppercased())
        }
        self.eventTypeList = [String] (dict.keys).sorted(by: <)
    }
    
    func getAllEventMonthYearType(){
        if eventListWithoutFilter == nil{
            return
        }
        var dict = [String : String]()
//        let monthSortArray = eventListWithoutFilter?.sorted(by: { $0.monthValue < $1.monthValue })
        var monthSortArray = eventListWithoutFilter?.sorted(by: {
            $0.dateEvt.compare($1.dateEvt) == .orderedDescending
        })
        monthSortArray = monthSortArray?.sorted(by: {
            $0.dateEvt.compare($1.dateEvt) == .orderedDescending
        })
//        for obj in monthSortArray! {
//            print(obj.monthValue)
//            dict.updateValue(obj.monthYear, forKey: obj.monthYear)
//        }
        self.monthList = Array(Set(monthSortArray!.compactMap { $0.monthYear }))

//        self.monthList = [String] (dict.keys)
    }
    
   
}
