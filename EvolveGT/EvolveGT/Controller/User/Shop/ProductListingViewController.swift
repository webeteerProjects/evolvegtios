



//
//  ProductListingViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 09/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher
import XLPagerTabStrip


class ProductListingViewController: UIViewController,IndicatorInfoProvider {
    @IBOutlet weak var collectionView: UICollectionView!
    var productListRes : ProductListRes?
    var productList : [Product]?
    var categoryId : String!
    var category : String!
    var tabTitle : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.getProductList()
        // Do any additional setup after loading the view.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: tabTitle)
    }


    func getProductList(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getProductList(categoryId: categoryId, categoryName: category)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
       // self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
   
}

extension ProductListingViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            //            self.showErrorAlert(message: response.error ?? " ")
        }else{
            if let data = response.data?.responseModel as? ProductListRes{
                self.productListRes = data
                self.productList = data.results
                self.collectionView.reloadData()
            }
        }
    }
    
}

extension ProductListingViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return self.productList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width =  (Constant.ScreenSize.SCREEN_WIDTH - 30) / 2
        return CGSize(width:width, height: width - 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCollectionViewCell", for: indexPath as IndexPath) as! ProductListCollectionViewCell
        cell.titleLbl.text =  productList?[indexPath.row].title
        if let imgUrl = productList?[indexPath.row].image{
            let baseImgUrlStr = "https://evolvegt.com/" + imgUrl
            cell.imageProduct.kf.setImage(with: URL(string : baseImgUrlStr), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let detailsView = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailedViewController") as! ProductDetailedViewController
        detailsView.slugVal =  productList?[indexPath.row].slug
        detailsView.titleVal = productList?[indexPath.row].title
        self.navigationController?.pushViewController(detailsView, animated: true)
    }
}
