//
//  ProductDetailedViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 16/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher
import RSSelectionMenu

enum ProductDetailCellSection {
    case ImageSection
    case ProductInfoSection
    case QuantitySection
    case SelectAttributeSection
    case OutOfStockSection
}

class ProductDetailedViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addtoCartBtn: UIButton!
    var tableSections = [ProductDetailCellSection.ImageSection]
    var name : String?
    var email : String?
    var productInfo:ProductDetails?
    var slugVal : String!
    var isOutOfStock = false
    
    var titleVal : String!

    var itemInfo : CardInfo?
    var isGiftCard = false
    var userSelectedQuanity = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        tableView.tableFooterView = UIView()
        self.title = titleVal
        self.getProductDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false

    }
    
    func getProductDetails(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getProductDetails(slug: slugVal)
        }
    }
    
    
    func createProductOrder(){
        tableSections =  [ProductDetailCellSection]()
        if self.productInfo  == nil{
            return
        }
        tableSections.append(.ImageSection)
        tableSections.append(.ProductInfoSection)
        tableSections.append(.QuantitySection)
        if self.productInfo?.attribute.count ?? -1 > 0{
            if self.productInfo!.attribute != nil{
                for attr in self.productInfo!.attribute{
                    attr.selectedValue = attr.values.first
                }
                tableSections.append(.SelectAttributeSection)
            }
           
        }
        if let variationsArray = self.productInfo?.variations{
            if variationsArray.count > 0{
                if variationsArray.first?.stockStatus == "outofstock"{
                    isOutOfStock = true
                    tableSections.append(.OutOfStockSection)
                    self.addtoCartBtn.isEnabled = false
                    addtoCartBtn.backgroundColor = .gray
                }
            }
        }
        self.tableView.reloadData()
    }
    
    @IBAction func addToCartBtnClicked(){
        self.showfloatingAlert(message: "Out of stock!")
        return
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            var dict = [String : AnyObject]()
            dict.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "serial")
            dict.updateValue(itemInfo?.title as AnyObject? ?? "" as AnyObject, forKey: "title")
            dict.updateValue(itemInfo?.slug as AnyObject? ?? "" as AnyObject, forKey: "slug")
            dict.updateValue(itemInfo?.price as AnyObject? ?? "" as AnyObject, forKey: "price")
            dict.updateValue(userSelectedQuanity as AnyObject? ?? "" as AnyObject, forKey: "quantity")
            if isGiftCard{
                dict.updateValue(itemInfo?.image as AnyObject? ?? "" as AnyObject, forKey: "image")
                dict.updateValue(name as AnyObject? ?? "" as AnyObject, forKey: "name")
                dict.updateValue(email as AnyObject? ?? "" as AnyObject, forKey: "email")
                serviceManager.addGiftCardToCart(info: dict)
            }else {
                dict.updateValue("" as AnyObject, forKey: "image")
                serviceManager.addArchievCardToCart(info: dict)
            }
        }
    }
    
}

extension ProductDetailedViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            //            self.showErrorAlert(message: response.error ?? " ")
        }else{
            
            if let serviceManager = manager as? ShopWebServiceManager{
                if serviceManager.serviceType == .ProductDetails{
                    if let data = response.data?.responseModel as? ProductDetails{
                        self.productInfo = data
                        createProductOrder()
                    }
                }
            }
            
//            if let data = response.data?.responseModel as? ApiResponse{
//                if let msg = data.msg{
//                    self.showfloatingAlert(message:msg)
//                    self.navigationController?.popViewController(animated: true)
//                }
//            }
        }
    }
    
}

extension ProductDetailedViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableSections[section] == .ImageSection{
            return 1
        }
        else  if tableSections[section] == .ProductInfoSection{
            return 1
        }
        else  if tableSections[section] == .QuantitySection{
            return 1
        }
        else  if tableSections[section] == .SelectAttributeSection{
            return self.productInfo?.attribute.count ?? 0
        }
        else  if tableSections[section] == .OutOfStockSection{
            return 1
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableSections[indexPath.section] == .ImageSection{
            return 220
        }
        else  if tableSections[indexPath.section] == .ProductInfoSection{
            return 80
        }
        else  if tableSections[indexPath.section] == .QuantitySection{
            return 162
        }
        else  if tableSections[indexPath.section] == .SelectAttributeSection{
            return 90
        }
        else  if tableSections[indexPath.section] == .OutOfStockSection{
            return 45
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableSections[indexPath.section] == .ImageSection{
            let imgCell = tableView.dequeueReusableCell(withIdentifier:"ImageTableViewCell",for: indexPath) as! ImageTableViewCell
            if let imgUrl = productInfo?.image{
                imgCell.imgView.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
            }
            return imgCell;
        }
        else  if tableSections[indexPath.section] == .ProductInfoSection{
            let infoCell = tableView.dequeueReusableCell(withIdentifier:"ItemInfoTableViewCell",for: indexPath) as! ItemInfoTableViewCell
                infoCell.titleLbl.text = productInfo?.title
                infoCell.priceLbl.text = "Price : $"
                infoCell.priceLbl.text?.append(productInfo?.price ?? "")
//            }else{
//                infoCell.titleLbl.text = productInfo?.content
//                infoCell.priceLbl.text = "Price : "
//                infoCell.priceLbl.text?.append(itemInfo?.title ?? "")
//            }
            return infoCell;
        }
        else  if tableSections[indexPath.section] == .QuantitySection{
            let quantityCell = tableView.dequeueReusableCell(withIdentifier:"QuantityTableViewCell",for: indexPath) as! QuantityTableViewCell
            quantityCell.delegate = self
            if let priceVal = Float(productInfo?.price ?? "0.00"){
                quantityCell.price = priceVal
            }
            quantityCell.showValues()
            return quantityCell
        }
            
        else  if tableSections[indexPath.section] == .SelectAttributeSection{
            let quantityCell = tableView.dequeueReusableCell(withIdentifier:"AttributeTableViewCell",for: indexPath) as! AttributeTableViewCell
            let titleStr =  self.productInfo?.attribute?[indexPath.row].title ?? ""
            quantityCell.titleLbl.text = "Select " + titleStr
            quantityCell.selectedItemLbl.text = self.productInfo?.attribute?[indexPath.row].selectedValue
            return quantityCell
        }
        else  if tableSections[indexPath.section] == .OutOfStockSection{
            let quantityCell = tableView.dequeueReusableCell(withIdentifier:"OutStockCell",for: indexPath)
            return quantityCell
        }else{
            let infoCell = tableView.dequeueReusableCell(withIdentifier:"ItemInfoTableViewCell",for: indexPath) as! ItemInfoTableViewCell
            return infoCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableSections[indexPath.section] == .SelectAttributeSection{
            var simpleSelectedArray = [String]()
            if self.productInfo?.attribute?[indexPath.row].values == nil{
                return
            }
            let simpleDataArray = (self.productInfo?.attribute?[indexPath.row].values)!
            let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
                cell.textLabel?.text = item
            }
            selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
                simpleSelectedArray = selectedItems
                self?.productInfo?.attribute?[indexPath.row].selectedValue = simpleSelectedArray.first ?? ""
                self?.tableView?.reloadSections(IndexSet(integer:indexPath.section), with: .none)
            }
            selectionMenu.maxSelectionLimit = 1
            selectionMenu.cellSelectionStyle = .checkbox
            selectionMenu.show(style: .formSheet, from: self)
        }
    }
}

extension ProductDetailedViewController: QuantityCellDelegate {
    func didSelectNoQuantity(quanity:Int){
        userSelectedQuanity = quanity
        if isOutOfStock{
            self.addtoCartBtn.isEnabled = false
            addtoCartBtn.backgroundColor = .gray
            return
        }
        if quanity == 0 {
            self.addtoCartBtn.isEnabled = false
            addtoCartBtn.backgroundColor = .gray
        } else{
            self.addtoCartBtn.isEnabled = true
            addtoCartBtn.backgroundColor = UIColor.init(hexString: "#08A53A")
        }
    }
    
    
}



