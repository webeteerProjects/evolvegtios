

//
//  LoginViewController.swift
//  Lubax
//
//  Created by Ajeesh T S on 16/05/18.
//  Copyright © 2018 iGrant.com. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//import UserNotifications
//import Firebase
//import FirebaseInstanceID
//import FirebaseMessaging
import SkyFloatingLabelTextField

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextfld: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextfld: SkyFloatingLabelTextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var guestUserBtn: UIButton!

    var isGuestUserLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.title = "Login"
        loginBtn.showRoundCorner(roundCorner: 3.0)
        // Do any additional setup after loading the view.
        
        emailTextfld.text = ""
        passwordTextfld.text = ""
        
        if isGuestUserLogin == true {
            guestUserBtn.isHidden = true
            UINavigationBar.appearance().isTranslucent = false
            let navigationBarAppearace = UINavigationBar.appearance()
            navigationBarAppearace.tintColor =  UIColor.white
            navigationBarAppearace.barTintColor =  UIColor.init(hexString: "#28AC10")
            navigationBarAppearace.shadowImage = UIImage()
            navigationBarAppearace.setBackgroundImage(UIImage(), for: .default)
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            self.title = "Login"
            let cancelItem  = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(LoginViewController.cancelBtnClicked))
            self.navigationItem.leftBarButtonItem = cancelItem
        }
        
    }
    
    @objc func cancelBtnClicked(){
        self.dismiss(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isGuestUserLogin == false{
            self.navigationController?.isNavigationBarHidden = true
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        // Hide the navigation bar on the this view controller
        //        IQKeyboardManager.shared.enableAutoToolbar = false
        //         IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        //        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if isGuestUserLogin == false{
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
    
    
    
    
    @IBAction func loginBtnClicked(){
        
        //        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        //        appDelegate?.showHomeView()
        
        // Generate top floating entry and set some properties
        
        //        IQKeyboardManager.shared.resignFirstResponder()
        if validate(){
            callLoginService()
        }
    }
    
    @IBAction func forgotBtnClicked(){
        let forgotPasswdVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotPasswdVC, animated: true)
        
    }
    
    @IBAction func guestBtnClicked(){
        let guestVC = self.storyboard?.instantiateViewController(withIdentifier: "GuestVC") as! GuestViewController
        self.navigationController?.pushViewController(guestVC, animated: true);
    }
    
    
    
    func validate() -> Bool{
        guard let email = self.emailTextfld.text, !email.isBlank else {
            //            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterEmail)
            self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterEmail)
            return false
        }
        guard let validEmail = self.emailTextfld.text, validEmail.isValidEmail else {
            //            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterValidEmail)
            
            self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterValidEmail)
            return false
        }
        guard let password = self.passwordTextfld.text, !password.isBlank else {
            //            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterPassword)
            
            self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterPassword)
            return false
        }
        
        //        AppSharedData.sharedInstance.email = email
        //        AppSharedData.sharedInstance.password = password
        
        return true
    }
    
    
    func callLoginService(){
        
        let email : String = (self.emailTextfld.text)!
        let password : String = (self.passwordTextfld.text)!
        //        if ((email == "webeteer1@gmail.com") && (password == "Elvisham88!")){
        //            self.addLoadingIndicator();
        //            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
        //                self.showUserHome()
        //            })
        //        }
        
        
        
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = LoginServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.login(uname: (self.emailTextfld.text)!, pwd: (self.passwordTextfld.text)!)
        }
    }
    
    func showHomePage(){
        self.removeLoadingIndicator()
        self.updateDeviceToken()
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.showHomeView()
        
    }
    
    func updateDeviceToken(){
        if let deviceToken = UserDefaults.standard.string(forKey: Constant.DeviceToken.KUserDeviceTokenKey){
            let loginServiceMngr =  LoginServiceManager()
            loginServiceMngr.updateDeviceToken(deviceToken: deviceToken)
        }
    }
    
    /*
     func registerPushNotification(){
     if AppSharedData.sharedInstance.isConfiguredFireBase == false{
     AppSharedData.sharedInstance.isConfiguredFireBase = true
     FirebaseApp.configure()
     }
     if #available(iOS 10.0, *) {
     // For iOS 10 display notification (sent via APNS)
     UNUserNotificationCenter.current().delegate = UIApplication.shared.delegate as! AppDelegate as UNUserNotificationCenterDelegate
     let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
     UNUserNotificationCenter.current().requestAuthorization(
     options: authOptions,
     completionHandler: {_, _ in })
     // For iOS 10 data message (sent via FCM
     Messaging.messaging().delegate = UIApplication.shared.delegate as! AppDelegate as UNUserNotificationCenterDelegate as? MessagingDelegate
     } else {
     let settings: UIUserNotificationSettings =
     UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
     UIApplication.shared.registerUserNotificationSettings(settings)
     }
     UIApplication.shared.registerForRemoteNotifications()
     
     
     }
     
     */
}


extension LoginViewController:WebServiceTaskManagerProtocol,UITextFieldDelegate{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? "")
        }else{
            if isGuestUserLogin == false{
                self.showHomePage()
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0{
            passwordTextfld.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
        
        
    }
    
    func showHomeScreen(){
        appDelegate.showHomeView()
    }
    
}

extension LoginViewController{
    func registerPushNotification(){
        //        self.appDelegate.registerForPNS()
        /*
         if AppSharedData.sharedInstance.isConfiguredFireBase == false{
         AppSharedData.sharedInstance.isConfiguredFireBase = true
         FirebaseApp.configure()
         }
         if #available(iOS 10.0, *) {
         // For iOS 10 display notification (sent via APNS)
         UNUserNotificationCenter.current().delegate = UIApplication.shared.delegate as! AppDelegate as UNUserNotificationCenterDelegate
         let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
         UNUserNotificationCenter.current().requestAuthorization(
         options: authOptions,
         completionHandler: {_, _ in })
         // For iOS 10 data message (sent via FCM
         Messaging.messaging().delegate = UIApplication.shared.delegate as! AppDelegate as UNUserNotificationCenterDelegate as? MessagingDelegate
         } else {
         let settings: UIUserNotificationSettings =
         UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
         UIApplication.shared.registerUserNotificationSettings(settings)
         }
         UIApplication.shared.registerForRemoteNotifications()
         */
    }
}
