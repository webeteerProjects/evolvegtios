//
//  ForgotPasswordViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 25/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import  SkyFloatingLabelTextField
import IQKeyboardManagerSwift

class ForgotPasswordViewController: UIViewController {
    @IBOutlet var resetBtn : UIButton!
    
    @IBOutlet weak var emailTextfld: SkyFloatingLabelTextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI(){
        self.title = "Reset Password"
        submitBtn.showRoundCorner(roundCorner: 3.0)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the navigation bar on the this view controller
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        //        IQKeyboardManager.shared.enableAutoToolbar = false
        //         IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    @IBAction func submitBtnClicked(){
        IQKeyboardManager.shared.resignFirstResponder()
        validateAndCallLoginApi()
        
    }
    
    func validate() -> Bool{
        guard let email = self.emailTextfld.text, !email.isBlank else {
            //            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterEmail)
            self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterEmail)
            return false
        }
        guard let validEmail = self.emailTextfld.text, validEmail.isValidEmail else {
            //            self.showfloatingAlert(message: Constant.Alert.KPromptMsgEnterValidEmail)
            
            self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterValidEmail)
            return false
        }
        return true
    }
}


extension ForgotPasswordViewController{
    
    @IBAction func resetBtnClicked(){
        validateAndCallLoginApi()
    }
    
    
    
    func validateAndCallLoginApi(){
        guard let email = self.emailTextfld.text, !email.isBlank else {
            self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterEmail)
            return
        }
        guard let validEmail = self.emailTextfld.text, validEmail.isValidEmail else {
            self.showWarningAlert(message: Constant.Alert.KPromptMsgEnterValidEmail)
            return
        }
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = LoginServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.forgotPassword(email:validEmail)
        }
        
    }
    
    
    
}
extension ForgotPasswordViewController:WebServiceTaskManagerProtocol,UITextFieldDelegate{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            self.showErrorAlert(message: response.error ?? " ")
            
            //            if response.data?.message != nil{
            //                self.showErrorAlert(message:(response.data?.message)!)
            //            }else{
            //                self.showErrorAlert(message: response.error!)
            //            }
        }else{
            self.showSuccesAlert()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //        emailContainerView.showSelectedThemeBorder()
        return true
    }
    
    func showSuccesAlert() {
        //        self.showfloatingAlert(description: "Email has been sent successfully")
        let alerController = UIAlertController(title: Constant.AppSetupConstant.KAppName, message: "Please check your email to reset your password", preferredStyle: .alert)
        alerController.addAction(UIAlertAction(title: "Ok", style: .default, handler: {(action:UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }));
        
        present(alerController, animated: true, completion: nil)
    }
    
    
    
}

