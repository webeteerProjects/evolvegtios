//
//  UserSettingsViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 24/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
}

class SideMenuAccessibleController: UIViewController {
    
    var notificationTypes: [NotificationType] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewWillDisappear(animated)
    }
}

enum UserSetting: String, CaseIterable {
    case language               = "Language"
    case pushNotifications      = "Push Notifications"
    case more                   = "More"
}

class UserSettingsViewController: SideMenuAccessibleController {
    
    @IBOutlet weak var tableView: UITableView!
    var serviceManager = ProfileServiceManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Settings"
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        getNotificationTypes()
    }
    
    private func getNotificationTypes() {
        guard isNetWorkAvailable else { return }
        self.addLoadingIndicator()
        serviceManager.serviceType = .NotificationTypes
        serviceManager.managerDelegate = self
        serviceManager.getNotificationTypes()
    }
    
    private func updateNotificationSettings(preference: NotificationPreference) {
        guard isNetWorkAvailable else { return }
        self.addLoadingIndicator()
        serviceManager.serviceType = .NotificationUpdate
        serviceManager.managerDelegate = self
        serviceManager.updateNotificationPreference(with: preference)
    }
}

extension UserSettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return UserSetting.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return notificationTypes.count
        case 2:
            return 3
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserSettingsTableViewCell",
                                                 for: indexPath) as? UserSettingsTableViewCell
        cell?.settingValueLabel.text = ""
        cell?.settingStatSwitch.isHidden = true
        cell?.delegate = nil
        switch indexPath.section {
        case 0:
                cell?.settingTitleLabel.text = "Language"
                cell?.settingValueLabel.text = "English"
        case 1:
            cell?.settingTitleLabel.text = notificationTypes[indexPath.row].type
            cell?.settingStatSwitch.setOn(notificationTypes[indexPath.row].status, animated: false)
            cell?.settingStatSwitch.isHidden = false
            cell?.delegate = self
            cell?.notificationType = notificationTypes[indexPath.row]
        case 2:
            if indexPath.row == 0{
                cell?.settingTitleLabel.text = "Terms of Use"
            }
            else if indexPath.row == 1{
                cell?.settingTitleLabel.text = "Privacy Policy "
            }
            else{
                cell?.settingTitleLabel.text = "Refund Policy"
            }
        default:
            cell?.settingTitleLabel.text = ""
            cell?.settingValueLabel.text = ""
            cell?.settingStatSwitch.isHidden = true
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return UserSetting.allCases[section].rawValue
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.backgroundView?.backgroundColor = .white
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2{
            var identifierKey = "TermsVC"
            var navTitle = "Terms of Use"

            if indexPath.row == 0{
                identifierKey = "TermsVC"
                navTitle = "Terms of Use"
            }
            else if indexPath.row == 1{
                identifierKey = "PrivacyVC"
                navTitle = "Privacy Policy"
            }else{
                 identifierKey = "RefundVC"
                    navTitle = "Refund Policy"
            }
            let storyboard = UIStoryboard.init(name: "UserSettings", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: identifierKey) as? InfoViewController
            viewController?.title = navTitle
            self.navigationController?.pushViewController(viewController!, animated: true)
        }
    }
}

extension UserSettingsViewController: WebServiceTaskManagerProtocol {
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)) {
        self.removeLoadingIndicator()
        if serviceManager.serviceType == .NotificationTypes {
            guard response.error == nil, let responseJSONArray = response.data?.response?["result"].array else {
                showErrorAlert(message: response.data?.message ?? "Something went wrong")
                return
            }
            notificationTypes = responseJSONArray.map { NotificationType(fromJson: $0) }
            tableView.reloadData()
        }
    }
}

extension UserSettingsViewController: UserSettingChangeDelegate {
    func changedSetting(for cell: UITableViewCell, switch: UISwitch, isOn: Bool, notificationType: NotificationType) {
        let notificationPref = notificationType.notificationPreference()
        updateNotificationSettings(preference: notificationPref)
    }
}
