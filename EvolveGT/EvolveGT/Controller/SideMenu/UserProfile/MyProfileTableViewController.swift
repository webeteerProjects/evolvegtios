//
//  MyProfileTableViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 25/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import SwiftyJSON

class DefaultTableViewController: UITableViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
        super.viewWillDisappear(animated)
    }
}

enum RelationshipType: String, CaseIterable {
    case friend         = "Friend"
    case localGuardian  = "Local Guardian"
    case parent         = "Parent"
    case spouse         = "Spouse"
    case other          = "Other"
}

enum ProfileTextType: String {
    case firstName = "First Name"
    case lastName = "Last Name"
    case email    = "Email"
    case phone    = "Phone"
    case dob      = "Date of Birth"
    case motorCycle = "Motor Cycle"
    case motorCycleNo = "Motor Cycle No."
    case skillLevel   = "Skill Level"
    case firstNameEM  = "First name"
    case lastNameEm   = "Last name"
    case phoneEM        = "phone"
    case relationship = "Relationship To You"
}

enum QuestionType: String {
    case gender
    case trackRecord
    case license
}

class MyProfileTableViewController: DefaultTableViewController {

    @IBOutlet weak var profilePicView: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    var isPickingImage = false
    private var userDetails: UserDetails?
    
    private var shippingFullName: String {
        if let details = userDetails {
            return details.shippingFirstName + " " + details.shippingLastName
        }
        return ""
    }
    private var billingFullName: String {
        if let details = userDetails {
            return details.billingFirstName + " " + details.billingLastName
        }
        return ""
    }
    private var shippingAddressLine: String {
        if let details = userDetails {
            return details.shippingStateName + "," + details.shippingCity + "-" + details.shippingPostcode
        }
        return ""   
    }
    private var billingAddressLine: String {
        if let details = userDetails {
            return details.billingStateName + "," + details.billingCity  + "-" + details.billingPostcode
        }
        return ""
    }
    private var mailingAddress: String? {
        if let details = userDetails {
            var addressStr = ""
            return shippingFullName + "\n" + details.shippingAddress1 + "\n" + details.shippingAddress2 + "\n" + shippingAddressLine + "\n" + details.shippingCountryName
        }
        return ""
    }
    
    private var billingAddress: String? {
        if let details = userDetails {
            return billingFullName + "\n" + details.billingAddress1 + "\n" + details.billingAddress2 + "\n" + billingAddressLine + "\n" +  details.billingCountryName
        }
        return ""
    }
    
    private var pickerView: UIPickerView?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Edit Profile"
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        
        userDetails = AppSharedData.sharedInstance.userDetails
//        profilePicView.kf.setImage(with: URL(string: userDetails?.profileImageUrl ?? ""))
        
        if let profimgUrl  = userDetails?.profileImageUrl{
            if profimgUrl.isValidString{
                profilePicView.kf.setImage(with: URL(string:profimgUrl))
            }
        }
        pickerView = UIPickerView()
        pickerView?.delegate = self
        pickerView?.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserDetails()
    }
    
    private func getUserDetails() {
        guard isNetWorkAvailable else { return }
        addLoadingIndicator()
        let serviceManager = LoginServiceManager()
        serviceManager.serviceType = .UserDetails
        serviceManager.managerDelegate = self
        serviceManager.userDetails()
    }
    
    private func updateProfilePicture(image: UIImage) {
        guard isNetWorkAvailable else { return }
        guard let imageData = image.jpegData(compressionQuality: 1) else { return }
        self.addLoadingIndicator()
        let serviceManager = LoginServiceManager()
        serviceManager.managerDelegate = self
        serviceManager.updateProfileImage(imageData: imageData as NSData)
    }
    
    
    func imageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImage.Orientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2))
            break
        case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi / 2))
            break
        case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImage.Orientation.upMirrored, UIImage.Orientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImage.Orientation.leftMirrored, UIImage.Orientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImage.Orientation.up, UIImage.Orientation.down, UIImage.Orientation.left, UIImage.Orientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored, UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    @objc func updateDOB(_ sender: UIDatePicker) {
        let cell = tableView.cellForRow(at: IndexPath(row: 6, section: 0)) as? MyProfileTextFieldTableViewCell
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        cell?.textField.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func saveProfileDetails(_ sender: UIButton) {
        guard isNetWorkAvailable else { return }
        addLoadingIndicator()
        let jsonData = try! JSONEncoder().encode(userDetails)
        let loginServiceManager = LoginServiceManager()
        loginServiceManager.managerDelegate = self
        loginServiceManager.serviceType = .ProfileUpdate
        guard let dictionary = try! JSONSerialization.jsonObject(with: jsonData,
                                                                 options: .allowFragments) as? [String: Any] else { return }
        loginServiceManager.updateProfile(userInfoJson: dictionary)
    }
    
    @IBAction func clickedOnCameraIcon(_ sender: Any) {
        isPickingImage = true
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = .camera
            imagePickerController.delegate = self
            DispatchQueue.main.async {
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { _ in
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.delegate = self
            DispatchQueue.main.async {
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if Constant.DeviceType.IS_IPAD {
            alert.popoverPresentationController?.permittedArrowDirections = []
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH / 2, y: Constant.ScreenSize.SCREEN_HEIGHT, width: 1.0, height: 1.0)
        }
        if Constant.DeviceType.IS_IPAD {
            alert.popoverPresentationController?.permittedArrowDirections = []
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH / 2, y: Constant.ScreenSize.SCREEN_HEIGHT, width: 1.0, height: 1.0)
        }
        present(alert, animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 7
        case 1:
            return 8
        case 2:
            return 4
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        switch  indexPath.section {
        case 0:
            if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTextFieldTableViewCell",
                                                     for: indexPath) as! MyProfileTextFieldTableViewCell
                cell.textField.inputView = nil
                cell.textField.isUserInteractionEnabled = true
                var title: ProfileTextType = .firstName
                var value: String? = ""
                if indexPath.row == 0  {
                    title = .firstName
                    value = userDetails?.firstName.capitalized
                } else if indexPath.row == 1 {
                    title = .lastName
                    value = userDetails?.lastName.capitalized
                } else if indexPath.row == 3 {
                    title = .email
                    value = userDetails?.email
                    cell.textField.isUserInteractionEnabled = false
                    cell.makeFontLighter()
                } else if indexPath.row == 4 {
                    title = .phone
                    value = userDetails?.billingPhone
                } else {
                    title = .dob
                    value = userDetails?.evDob
                    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
                    imageView.image = #imageLiteral(resourceName: "calendar")
                    cell.textField.rightViewMode = .always
                    cell.textField.rightView = imageView
                    let datePicker = UIDatePicker()
                    datePicker.datePickerMode = .date
                    datePicker.addTarget(self, action: #selector(updateDOB(_:)), for: .valueChanged)
                    cell.textField.inputView = datePicker
                }
                cell.textType = title
                cell.textField.placeholder = title.rawValue
                cell.textField.title = title.rawValue
                cell.textField.text = value
                cell.delegate = self
                return cell
            } else if indexPath.row == 2 || indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileQuestionCellTableViewCell",
                                                         for: indexPath) as! MyProfileQuestionCellTableViewCell
                if indexPath.row == 2 {
                    cell.firstButton.isUserInteractionEnabled = true
                    cell.secondButton.isUserInteractionEnabled = true
                    cell.questionLabel.text = "Gender"
                    cell.firstOptionLabel.text = "Male"
                    cell.secondOptionLabel.text = "Female"
                    let gender = userDetails?.evGender ?? ""
                    cell.optionValue = gender == "Male" ? .one : .two
                    cell.questionType = .gender
                    cell.delegate = self
                } else if indexPath.row == 5 {
                    cell.questionLabel.text = "Do you have a current race license?"
                    cell.firstOptionLabel.text = "Yes"
                    cell.secondOptionLabel.text = "No"
                    let hasRaceLicense = userDetails?.evRaceLicence
                    cell.optionValue = hasRaceLicense == "1" ? .one : .two
                    cell.questionType = .license
                    cell.firstButton.isUserInteractionEnabled = false
                    cell.secondButton.isUserInteractionEnabled = false
                    cell.makeFontLighter()
                    cell.delegate = self
                }
                return cell
            }
        case 1:
            if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 7 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTextFieldTableViewCell",
                                                         for: indexPath) as! MyProfileTextFieldTableViewCell
                cell.textField.inputView = nil
                 cell.textField.isUserInteractionEnabled = true
                var title: ProfileTextType = .motorCycle
                var value: String?
                if indexPath.row == 0 {
                    title = .motorCycle
                    value = userDetails?.evMotorcycle.capitalized
                } else if indexPath.row == 1 {
                    title = .motorCycleNo
                    value = userDetails?.evMotorcycleNumber
                } else if indexPath.row == 7 {
                    cell.textField.isUserInteractionEnabled = false
                    title = .skillLevel
                    value = userDetails?.skillLevel
                    cell.makeFontLighter()
                }
                cell.textType = title
                cell.textField.placeholder = title.rawValue
                cell.textField.title = title.rawValue
                cell.textField.text = value
                cell.delegate = self
                return cell
            } else if indexPath.row == 2 || indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileHeaderTableVIewCell",
                                                         for: indexPath) as! MyProfileHeaderTableVIewCell
                var title = ""
                if indexPath.row == 2 {
                    title = "Mailing Address"
                } else if indexPath.row == 4 {
                    title = "Billing Address"
                }
                cell.headerTitle.text = title
                return cell
            } else if indexPath.row == 3 || indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTextTableViewCell", for: indexPath) as! MyProfileTextTableViewCell
                var value: String?
                if indexPath.row == 3 {
                    value = mailingAddress
                   
                } else if indexPath.row == 5 {
                    value = billingAddress
                }
                if value == " \n\n\n,-\n"{
                    value = ""
                }
                if let str  = value{
                    if str.isEmpty{
                        if indexPath.row == 3 {
                            value = "No mailing address found. Please add a new address."
                        }
                        else if indexPath.row == 5 {
                            value = "No billing address found. Please add a new address."
                        }
                    }
                }
                cell.descLabel.text = value
                return cell
            } else if indexPath.row == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileQuestionCellTableViewCell",
                                                         for: indexPath) as! MyProfileQuestionCellTableViewCell
                cell.questionLabel.text = "Have you ever been on a track?"
                cell.firstOptionLabel.text = "Yes"
                cell.secondOptionLabel.text = "No"
                cell.delegate = self
                cell.questionType = .trackRecord
                cell.firstButton.isUserInteractionEnabled = false
                cell.secondButton.isUserInteractionEnabled = false
                cell.makeFontLighter()
                return cell
            }
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTextFieldTableViewCell",
                                                     for: indexPath) as! MyProfileTextFieldTableViewCell
            var title: ProfileTextType = .firstNameEM
            var value: String?
            cell.textField.inputView = nil
            cell.textField.isUserInteractionEnabled = true
            if indexPath.row == 0 {
                title = .firstNameEM
                value = userDetails?.evEmergencyFirstName.capitalized
//                cell.textField.isUserInteractionEnabled = true
            } else if indexPath.row == 1 {
                title = .lastNameEm
                value = userDetails?.evEmergencyLastName.capitalized
//                cell.textField.isUserInteractionEnabled = true
            } else if indexPath.row == 2 {
                title = .phoneEM
                value = userDetails?.evEmergencyPhone
            } else {
                title = .relationship
                value = userDetails?.evEmergencyRelationship
                cell.textField.inputView = pickerView
                cell.textField.isUserInteractionEnabled = true
            }
            cell.textType = title
            cell.textField.placeholder = title.rawValue.capitalized
            cell.textField.title = title.rawValue.capitalized
            cell.textField.text = value
            cell.delegate = self
            return cell
        default:
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:
            return "Your Motorcycle"
        case 2:
            return "Emergency Contact"
        default:
            return ""
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
            return 40
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        header.textLabel?.textColor = .black
        header.backgroundView?.backgroundColor = .white
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 2 || indexPath.row == 4 {
                let detailEditController = storyboard?.instantiateViewController(withIdentifier: "ProfileDetailEditController") as! ProfileDetailEditController
                detailEditController.userDetails = self.userDetails
                if indexPath.row == 4 {
                    detailEditController.addressType = .billing
                } else {
                    detailEditController.addressType = .shipping
                }
                navigationController?.pushViewController(detailEditController, animated: true)
            }
        }
    }
}

extension MyProfileTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 5
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch row {
        case 0:
            return RelationshipType.friend.rawValue
        case 1:
            return RelationshipType.localGuardian.rawValue
        case 2:
            return RelationshipType.parent.rawValue
        case 3:
            return RelationshipType.spouse.rawValue
        case 4:
            return RelationshipType.other.rawValue
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        userDetails?.evEmergencyRelationship = RelationshipType.allCases[row].rawValue
        tableView.reloadRows(at: [IndexPath(row: 3, section: 2)], with: .automatic)
    }
}

extension MyProfileTableViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
//        guard let orientationFixed = image.fixedOrientation() else { return }
        profilePicView.image = self.imageOrientation(image)
        picker.dismiss(animated: true, completion: {
            self.updateProfilePicture(image: image)
        })
    }
}

extension MyProfileTableViewController: WebServiceTaskManagerProtocol {
    func didFinishTask(from manager: AnyObject, response: (data: RestResponse?, error: String?)) {
        self.removeLoadingIndicator()
        guard response.error == nil else {
            showErrorAlert(message: response.error ?? "Something went wrong")
            return
        }
        guard let manager = manager as? LoginServiceManager else { return }
        if manager.serviceType == .ProfileUpdate {
            let alert = UIAlertController(title: "Success",
                                          message: "Your profile has been updated successfully",
                                          preferredStyle: .alert)
            alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else if manager.serviceType == .UserDetails, let data = response.data?.responseModel as? UserDetailsRes {
            AppSharedData.sharedInstance.userDetails = data.result
            self.userDetails = data.result
            tableView.reloadData()
        }
    }
}

extension MyProfileTableViewController: MyProfileTextFieldTableViewCellDelegate {
    
    
    func textEditingTextField(_ cell: MyProfileTextFieldTableViewCell, textField: UITextField, textType: ProfileTextType, newText: String){
    
    }
    
    func startedEditingTextField(_ cell: MyProfileTextFieldTableViewCell, textField: UITextField, textType: ProfileTextType) {
    }
    
    func finishedEditingTextField(_ cell: MyProfileTextFieldTableViewCell, textField: UITextField, textType: ProfileTextType) {
        switch textType {
        case .firstName:
            userDetails?.firstName = textField.text
        case .lastName:
            userDetails?.lastName = textField.text
        case .email:
            userDetails?.email = textField.text
        case .phone:
            userDetails?.billingPhone = textField.text
        case .dob:
            userDetails?.evDob = textField.text
        case .motorCycle:
            userDetails?.evMotorcycle = textField.text
        case .motorCycleNo:
            userDetails?.evMotorcycleNumber = textField.text
        case .skillLevel:
            userDetails?.skillLevel = textField.text
        case .firstNameEM:
            userDetails?.evEmergencyFirstName = textField.text
        case .lastNameEm:
            userDetails?.evEmergencyLastName = textField.text
        case .phoneEM:
            userDetails?.evEmergencyPhone = textField.text
        case .relationship:
            print(textField.text ?? "")
//            userDetails?.evEmergencyRelationship = textField.text
        }
    }
    
}

extension MyProfileTableViewController: MyProfileQuestionCellDelegate {
    func choseOption(_ cell: MyProfileQuestionCellTableViewCell, questionType: QuestionType, option: String) {
        switch questionType {
        case .gender:
            userDetails?.evGender = option
        case .trackRecord:
            userDetails?.everBeenTrack = option == "Yes" ? "1" : "0"
        case .license:
            userDetails?.evRaceLicence = option == "Yes" ? "1" : "0"
        }
    }
}

extension MyProfileTableViewController: UINavigationControllerDelegate {  }

