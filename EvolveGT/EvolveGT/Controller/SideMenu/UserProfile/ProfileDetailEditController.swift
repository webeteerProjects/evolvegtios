//
//  ProfileDetailEditController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 25/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import RSSelectionMenu

class ProfileDetailEditController: UITableViewController {
    enum AddressType {
        case shipping
        case billing
    }
    var addressType: AddressType = .billing
    var userDetails: UserDetails?
    var selectedCountry : Country?
    var selectedState : City?
    var stateList : [City]?
    var selectedCountryCode : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = addressType == .billing ? "Billing Address" : "Shipping Address"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

    
    @IBAction func saveAction(_ sender: UIButton) {
        guard isNetWorkAvailable else { return }
        addLoadingIndicator()
        let jsonData = try! JSONEncoder().encode(userDetails)
        let loginServiceManager = LoginServiceManager()
        loginServiceManager.managerDelegate = self
        loginServiceManager.serviceType = .ProfileUpdate
        guard checkValueInMandotoryFields() else {
            showErrorAlert(message: "Please enter all mandatory fields")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                self.removeLoadingIndicator()
            }
            return
        }
        guard performValidations() else {
            showErrorAlert(message: "Please check the input")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                self.removeLoadingIndicator()
            }
            return
        }
        
        if addressType == .billing {
            guard self.userDetails?.billingPostcode.count ?? 0 >= 5 else {
                showErrorAlert(message: "Billing Zip Code must be at least 5 characters")
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                    self.removeLoadingIndicator()
                }
                return
                
            }
            
        } else {
            guard self.userDetails?.shippingPostcode.count ?? 0 >= 5 else {
                showErrorAlert(message: "Shipping Zip Code must be at least 5 characters")
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                    self.removeLoadingIndicator()
                }
                return
                
            }
        }
        
        guard let dictionary = try! JSONSerialization.jsonObject(with: jsonData,
                                                                 options: .allowFragments) as? [String: Any] else { return }
        loginServiceManager.updateProfile(userInfoJson: dictionary)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    func checkValueInMandotoryFields() -> Bool {
        guard let userDetails = self.userDetails else { return false }
        if addressType == .billing {
            guard !userDetails.billingFirstName.isEmpty && !userDetails.billingLastName.isEmpty &&
                  !userDetails.billingCountryName.isEmpty && !userDetails.billingAddress1.isEmpty &&
                  !userDetails.billingCity.isEmpty && !userDetails.billingStateName.isEmpty &&
                  !userDetails.billingPostcode.isEmpty && !userDetails.billingPhone.isEmpty &&
                  !userDetails.billingEmail.isEmpty else { return false }
        } else {
            guard !userDetails.shippingFirstName.isEmpty && !userDetails.shippingLastName.isEmpty &&
                  !userDetails.shippingCountryName.isEmpty && !userDetails.shippingAddress1.isEmpty &&
                  !userDetails.shippingCity.isEmpty && !userDetails.shippingStateName.isEmpty &&
                  !userDetails.shippingPostcode.isEmpty && !userDetails.billingPhone.isEmpty &&
                  !userDetails.billingEmail.isEmpty else { return false }
        }
        return true
    }
    
    func performValidations() -> Bool {
        guard let userDetails = userDetails else { return false }
        guard userDetails.billingEmail.isValidEmail else { return false }
        return true
    }
    
    func callUserDetailsApi() {
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = LoginServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.userDetails()
        }
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTextFieldTableViewCell",
                                                 for: indexPath) as! MyProfileTextFieldTableViewCell
          cell.textField.isUserInteractionEnabled = true
        var title = ""
        var value: String?
        switch indexPath.row {
        case 0:
            title = "First Name*"
            value = addressType == .billing ? userDetails?.billingFirstName : userDetails?.shippingFirstName
            if let val = value{
                if !val.isValidString{
                    value = userDetails?.firstName
                    addressType == .billing ? (userDetails?.billingFirstName = value) : (userDetails?.shippingFirstName = value)
                }
            }else{
                value = userDetails?.firstName
                addressType == .billing ? (userDetails?.billingFirstName = value) : (userDetails?.shippingFirstName = value)
            }
        case 1:
            title = "Last Name*"
            value = addressType == .billing ? userDetails?.billingLastName : userDetails?.shippingLastName
            if let val = value{
                if !val.isValidString{
                    value = userDetails?.lastName
                    addressType == .billing ? (userDetails?.billingLastName = value) : (userDetails?.shippingLastName = value)
                }
            }else{
                value = userDetails?.lastName
                addressType == .billing ? (userDetails?.billingLastName = value) : (userDetails?.shippingLastName = value)

            }
        case 2:
            title = "Company Name"
            value = addressType == .billing ? userDetails?.billingCompany : userDetails?.shippingCompany
        case 3:
            cell.textField.isUserInteractionEnabled = false
            title = "Country*"
            value = addressType == .billing ? userDetails?.billingCountryName : userDetails?.shippingCountryName
            self.selectedCountryCode = addressType == .billing ? userDetails?.billingCountry : userDetails?.shippingCountry
        case 4:
            title = "Address 1*"
            value = addressType == .billing ? userDetails?.billingAddress1 : userDetails?.shippingAddress1
        case 5:
            title = "Address 2"
            value = addressType == .billing ? userDetails?.billingAddress2 : userDetails?.shippingAddress2
        case 6:
            title = "City*"
            value = addressType == .billing ? userDetails?.billingCity : userDetails?.shippingCity
        case 7:
            cell.textField.isUserInteractionEnabled = false
            title = "State*"
            value = addressType == .billing ? userDetails?.billingStateName : userDetails?.shippingStateName
        case 8:
            title = "Zip Code*"
            value = addressType == .billing ? userDetails?.billingPostcode : userDetails?.shippingPostcode
//            cell.characterLimit = 5
            cell.textField.keyboardType = .default
        case 9:
            title = "Phone*"
            value = addressType == .billing ? userDetails?.billingPhone : userDetails?.billingPhone
            cell.textField.keyboardType = .numberPad
        case 10:
            title = "Email*"
            value = addressType == .billing ? userDetails?.billingEmail : userDetails?.billingEmail
            cell.textField.keyboardType = .emailAddress
            if let val = value{
                if !val.isValidString{
                    value = userDetails?.email
                    addressType == .billing ? (userDetails?.billingEmail = value) : (userDetails?.billingEmail = value)
                }
            }else{
                value = userDetails?.email
                addressType == .billing ? (userDetails?.billingEmail = value) : (userDetails?.billingEmail = value)
            }
        default:
            title = ""
        }
        
        cell.textField.placeholder = title
        cell.textField.title = title
        cell.textField.text = value
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row  == 3{
            showCountryList()
        }
        else if indexPath.row  == 7{
            if self.selectedCountryCode == nil{
                self.showfloatingAlert(message: "Please select a country")
            }else{
                if self.stateList == nil{
                    callStateApi()
                }else{
                    showStateList()
                }
            }
        }
    }
    
    
    func showStateList(){
        var countries: [String] = []
        let stateList = AppSharedData.sharedInstance.state
        if stateList == nil{
            return
        }
        countries = Array(Set(stateList!.compactMap { $0.name }))
        countries = countries.sorted(by: <)

        //        for code in NSLocale.isoCountryCodes  {
        //            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
        //            let name = NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
        //            countries.append(name)
        //        }
        var simpleSelectedArray = [String]()
        if countries.count < 1{
            return
        }
        let simpleDataArray = countries
        let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
            cell.textLabel?.text = item
        }
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
            simpleSelectedArray = selectedItems
            if selectedItems != nil{
                let selectedVal = simpleSelectedArray.first ?? ""
                let val = stateList?.filter { $0.name == selectedVal}
                self?.selectedState = val?.first
                self?.addressType == .billing ? (self?.userDetails?.billingStateName = selectedVal) : (self?.userDetails?.shippingStateName = selectedVal)
                self?.addressType == .billing ? (self?.userDetails?.billingState = self?.selectedState?.sortName) : (self?.userDetails?.shippingState = self?.selectedState?.sortName)
                let indexPath = IndexPath(item: 7, section: 0)
                self?.tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
        selectionMenu.maxSelectionLimit = 1
        selectionMenu.cellSelectionStyle = .checkbox
        selectionMenu.title = "State"
        
        //        selectionMenu.showSearchBar { [weak self] (searchText) -> ([String]) in
        // return filtered array based on any condition
        // here let's return array where name starts with specified search text
        //            return countries.filter({ $0.lowercased().hasPrefix(searchText.lowercased()) }) ?? []
        //        }
        //        self.isDismissingSortView = true
        selectionMenu.show(style: .present, from: self)
        
    }
    
    func callStateApi(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = LoginServiceManager()
            serviceManager.managerDelegate = self
            if let cCode = self.selectedCountryCode{
                if cCode.isValidString{
                    serviceManager.getStateList(countryCode:self.selectedCountryCode ?? "")
                }else{
                    let countryList = AppSharedData.sharedInstance.country
                    let value = addressType == .billing ? userDetails?.billingCountryName : userDetails?.shippingCountryName
                    let val = countryList?.filter { $0.country == value}
                    self.selectedCountry = val?.first
                    self.addressType == .billing ? (self.userDetails?.billingCountry = self.selectedCountry?.value) : (self.userDetails?.shippingCountry = self.selectedCountry?.value)
                    self.selectedCountryCode = self.selectedCountry?.value
                    serviceManager.getStateList(countryCode:self.selectedCountryCode ?? "")
                }
            }
        }
    }

    
    func showCountryList(){
        var countries: [String] = []
        let countryList = AppSharedData.sharedInstance.country
        if countryList == nil{
            return
        }
        countries = Array(Set(countryList!.compactMap { $0.country }))

//        for code in NSLocale.isoCountryCodes  {
//            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
//            let name = NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
//            countries.append(name)
//        }
        var simpleSelectedArray = [String]()
        if countries.count < 1{
            return
        }
        let simpleDataArray = countries
        let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
            cell.textLabel?.text = item
        }
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
            simpleSelectedArray = selectedItems
            if selectedItems != nil{
                let selectedVal = simpleSelectedArray.first ?? ""
                let val = countryList?.filter { $0.country == selectedVal}
                self?.selectedCountry = val?.first
                self?.addressType == .billing ? (self?.userDetails?.billingCountryName = selectedVal) : (self?.userDetails?.shippingCountryName = selectedVal)
                self?.addressType == .billing ? (self?.userDetails?.billingCountry = self?.selectedCountry?.value) : (self?.userDetails?.shippingCountry = self?.selectedCountry?.value)
                self?.selectedCountryCode = self?.selectedCountry?.value
                let indexPath = IndexPath(item: 3, section: 0)
                self?.tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
        selectionMenu.maxSelectionLimit = 1
        selectionMenu.cellSelectionStyle = .checkbox
        selectionMenu.title = "Country"
        
//        selectionMenu.showSearchBar { [weak self] (searchText) -> ([String]) in
            // return filtered array based on any condition
            // here let's return array where name starts with specified search text
//            return countries.filter({ $0.lowercased().hasPrefix(searchText.lowercased()) }) ?? []
//        }
//        self.isDismissingSortView = true
        selectionMenu.show(style: .present, from: self)
        
    }
}

extension ProfileDetailEditController: MyProfileTextFieldTableViewCellDelegate {
    func startedEditingTextField(_ cell: MyProfileTextFieldTableViewCell, textField: UITextField, textType: ProfileTextType) {
        //
    }
    
    func finishedEditingTextField(_ cell: MyProfileTextFieldTableViewCell, textField: UITextField, textType: ProfileTextType) {
        
    }
    
    func textEditingTextField(_ cell: MyProfileTextFieldTableViewCell, textField: UITextField, textType: ProfileTextType, newText: String){

        let indexPath = tableView.indexPath(for: cell)
        let row = indexPath?.row ?? 0
        
        switch row {
        case 0:
            addressType == .billing ? (userDetails?.billingFirstName = newText) : (userDetails?.shippingFirstName = newText)
        case 1:
            addressType == .billing ? (userDetails?.billingLastName = newText) : (userDetails?.shippingLastName = newText)
        case 2:
            addressType == .billing ? (userDetails?.billingCompany = newText) : (userDetails?.shippingCompany = newText)
        case 3:
            addressType == .billing ? (userDetails?.billingCountryName = newText) : (userDetails?.shippingCountryName = newText)
        case 4:
            addressType == .billing ? (userDetails?.billingAddress1 = newText) : (userDetails?.shippingAddress1 = newText)
        case 5:
            addressType == .billing ? (userDetails?.billingAddress2 = newText) : (userDetails?.shippingAddress2 = newText)
        case 6:
            addressType == .billing ? (userDetails?.billingCity = newText) : (userDetails?.shippingCity = newText)
        case 7:
            addressType == .billing ? (userDetails?.billingStateName = newText) : (userDetails?.shippingStateName = newText)
        case 8:
            addressType == .billing ? (userDetails?.billingPostcode = newText) : (userDetails?.shippingPostcode = newText)
        case 9:
            addressType == .billing ? (userDetails?.billingPhone = newText) : (userDetails?.billingPhone = newText)
        case 10:
            addressType == .billing ? (userDetails?.billingEmail = newText) : (userDetails?.billingEmail = newText)
        default: return
        }
    }
}

extension ProfileDetailEditController: WebServiceTaskManagerProtocol {
    func didFinishTask(from manager: AnyObject, response: (data: RestResponse?, error: String?)) {
        self.removeLoadingIndicator()
        guard response.error == nil else {
            showErrorAlert(message: response.data?.message ?? "Something went wrong")
            return
        }
        guard let manager = manager as? LoginServiceManager else { return }
        if manager.serviceType == .ProfileUpdate {
            self.callUserDetailsApi()
        }
        else  if manager.serviceType == .UserDetails {
            let alert = UIAlertController(title: "Success",
                                          message: "Updated successfully",
                                          preferredStyle: .alert)
            alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        else  if manager.serviceType == .StateList {
            self.showStateList()
        }
        
        
    }
}
