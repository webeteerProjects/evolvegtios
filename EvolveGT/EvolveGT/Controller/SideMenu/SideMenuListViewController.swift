//
//  SideMenuListViewController.swift
//  EalanDirect
//
//  Created by Ajeesh T S on 22/04/18.
//

import UIKit
import Kingfisher

class SideMenuListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var cellTitle = ["","Home","Upcoming Events","Past Events","Credit History","Membership", "My Profile", "Change Password","Settings", "About Us", "Logout"]
    var cellIcons = ["","homesideMenu","upcoming_events_a","past_events_a","credit_history", "membership","my_profile","change password","settings","aboutUs","logoutGreen"]
    var isHaveSwitchUserModule = false
    var isDisabledMembership = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        createCellOrder()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createCellOrder(){
        isDisabledMembership = false
        if (UserInfo.currentUser()?.role ==  "administrator"){
            isDisabledMembership = true
            addSwitchModulde()
        }
        else if (UserInfo.currentUser()?.role ==  "coach"){
            isDisabledMembership = true
            addSwitchModulde()
        }
        
    }
    
    func addSwitchModulde(){
        isHaveSwitchUserModule = true
        cellTitle = ["","Home","Upcoming Events","Past Events","Credit History", "My Profile", "Change Password","Settings", "About Us", "Switch Module", "Logout"]
        cellIcons = ["","homesideMenu","upcoming_events_a","past_events_a","credit_history","my_profile","change password","settings","aboutUs","SwictUserGreen","logoutGreen"]
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
        
        if AppSharedData.sharedInstance.isRefreshSideMenu{
            AppSharedData.sharedInstance.isRefreshSideMenu = false
            refreshSideMenu()
        }
        //        self.navigationItem.title = ""
        //        self.navigationController?.navigationBar.lt_setBackgroundColor(backgroundColor: .clear)
        //        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        //        self.navigationController?.navigationBar.shadowImage = UIImage()
        //        self.navigationController?.navigationBar.isTranslucent = true
        //        showWhiteTitleNavigationBar()
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //        self.navigationController?.navigationBar .lt_reset()
    }
    
    func refreshSideMenu(){
        createCellOrder()
        self.tableView.reloadData()
    }
    
    @IBAction func editProfileBtnCllicked(){
    }
    
    
    
}

extension  SideMenuListViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return cellTitle.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:Constant.CustomTabelCell.KSideMenuCellID,for: indexPath) as! SidemenuTableViewCell
        cell.titleLbl.text = cellTitle[indexPath.row]
        cell.iconImgView.image =  UIImage(named:cellIcons[indexPath.row])
        cell.separatorLineLbl.isHidden = true
        if UserInfo.currentUser()?.token == nil{
            cell.separatorLineLbl.isHidden = true
        }
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.row == 1 {
            self.sideMenuController?.hideMenu()
        }
            
        else  if indexPath.row == 2 {
            AppSharedData.sharedInstance.selectedTabIndex = 0
            let storyboard = UIStoryboard.init(name: "User", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "EventsTabsViewController") as? EventsTabsViewController
            show(viewController!)
            
            
        }
            
        else  if indexPath.row == 3 {
            AppSharedData.sharedInstance.selectedTabIndex = 1
            let storyboard = UIStoryboard.init(name: "User", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "EventsTabsViewController") as? EventsTabsViewController
            show(viewController!)
        }
            
            
        else  if indexPath.row == 4 {
            //            self.sideMenuController?.hideMenu()
            let storyboard = UIStoryboard.init(name: "User", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "CreditHistoryViewController") as? CreditHistoryViewController
            show(viewController!)
        }
        
        if isDisabledMembership == true{
            if indexPath.row == 5 {
                let storyboard = UIStoryboard.init(name: "MyProfile", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "MyProfileTableViewController") as? MyProfileTableViewController
                show(viewController!)
            }
            else if indexPath.row == 6 {
                let storyboard = UIStoryboard.init(name: "User", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "ChangePWVC") as? ChangePasswordViewController
                show(viewController!)
            }
                
            else if indexPath.row == 7 {
                let storyboard = UIStoryboard.init(name: "UserSettings", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "UserSettingsViewController") as? UserSettingsViewController
                show(viewController!)
            }
            else if indexPath.row == 8 {
                let storyboard = UIStoryboard.init(name: "User", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
                show(viewController!)
            }
                //
                
            else if indexPath.row == 9{
                if isHaveSwitchUserModule{
                    switchToAdminModule()
                }else{
                    self.showLogoutConfirmAlert()
                }
            }
            else if indexPath.row == 10{
                self.showLogoutConfirmAlert()
            }
        }
        else{
            if indexPath.row == 5 {
                let storyboard = UIStoryboard.init(name: "User", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "MembershipViewController") as? MembershipViewController
                show(viewController!)
            }
                
            else if indexPath.row == 6 {
                let storyboard = UIStoryboard.init(name: "MyProfile", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "MyProfileTableViewController") as? MyProfileTableViewController
                show(viewController!)
            }
            else if indexPath.row == 7 {
                let storyboard = UIStoryboard.init(name: "User", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "ChangePWVC") as? ChangePasswordViewController
                show(viewController!)
            }
                
            else if indexPath.row == 8 {
                let storyboard = UIStoryboard.init(name: "UserSettings", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "UserSettingsViewController") as? UserSettingsViewController
                show(viewController!)
            }
            else if indexPath.row == 9 {
                let storyboard = UIStoryboard.init(name: "User", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
                show(viewController!)
            }
                //
                
            else if indexPath.row == 10{
                if isHaveSwitchUserModule{
                    switchToAdminModule()
                }else{
                    self.showLogoutConfirmAlert()
                }
            }
            else if indexPath.row == 11{
                self.showLogoutConfirmAlert()
            }
        }
        
        
        
    }
    
    func switchToAdminModule(){
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.showAdminView()
    }
    
    func show(_ controller: UIViewController) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.pushSideMenuSelection(controller)
    }
    
    func showLogoutConfirmAlert() {
        let alerController = UIAlertController(title: "", message: "Are you sure you want to log out?", preferredStyle: .actionSheet)
        alerController.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: {(action:UIAlertAction) in
            self.logout()
        }));
        alerController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action:UIAlertAction) in
            
        }));
        if Constant.DeviceType.IS_IPAD {
            alerController.popoverPresentationController?.permittedArrowDirections = []
            alerController.popoverPresentationController?.sourceView = self.view
            alerController.popoverPresentationController?.sourceRect = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH / 2, y: Constant.ScreenSize.SCREEN_HEIGHT, width: 1.0, height: 1.0)
        }
        present(alerController, animated: true, completion: nil)
    }
    
    func logout(){
        UserInfo.currentUser()?.clearSession()
        _ = UserInfo.restoreSession()
        AppSharedData.sharedInstance.isLoggedIn = false
        AppSharedData.sharedInstance.userDetails = nil
        AppSharedData.sharedInstance.isCurrentSwitchModuleUser = false
        AppSharedData.sharedInstance.isCurrentSwitchModuleAdmin = false
        let storboard = UIStoryboard.init(name: "Main", bundle: nil)
        let navVC = storboard.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = navVC;
        
        
        //        UIApplication.shared.unregisterForRemoteNotifications()
        //        _ = UserInfo.restoreSession()
        //        NotificationCenter.default.post(name: Constant.NotificationName.KLogoutNotification, object: nil)
        //        AppSharedData.sharedInstance.isRefreshSideMenu = true
        //        refreshSideMenu()
        
        // AppSharedData.sharedInstance.isLoggedIn = false
        //        let loginManager = LoginServiceManager()
        //        loginManager.loginOut()
        //        self.dismissViewControllerAnimated(false, completion: nil)
        //        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let viewController = mainStoryboard.instantiateViewControllerWithIdentifier("LoginNav")
    }
}

class SidemenuTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iconImgView: UIImageView!
    @IBOutlet weak var separatorLineLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
