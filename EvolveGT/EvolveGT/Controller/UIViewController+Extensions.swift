//
//  UIViewController+Extensions.swift
//  
//
//  Created by Ajeesh T S on 27/07/19.
//

import UIKit

extension UIViewController {
    func addTitle(with text: String, needsDefaultBack: Bool = true) {
        guard navigationController != nil else { return }
        let barButtonItem = UIBarButtonItem(title: text, style: .plain, target: nil, action: nil)
        barButtonItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)],
                                             for: .normal)
//        navigationItem.leftItemsSupplementBackButton = needsDefaultBack
//        navigationItem.leftBarButtonItem = barButtonItem
        self.title = text
    }
}
