//
//  GuestViewController.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 03/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class GuestViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var categoryRes : CategoryListRes?
    var categoriesList : [ProductCategory]?
    var viewControllersList :[ProductListingViewController]?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Evolve GT"
       self.setupTableView()
        getCategoryList()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 780
    }
    
    func getCategoryList(){
        if self.isNetWorkAvailable{
            self.addLoadingIndicator()
            let serviceManager = ShopWebServiceManager()
            serviceManager.managerDelegate = self
            serviceManager.getProCategoryList()
        }
    }
    
    
    @IBAction func eventsBtnClicked(){
        let eventVC = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "UserEventsViewController") as! UserEventsViewController
        eventVC.isGuestUser = true
        self.navigationController?.pushViewController(eventVC, animated: true)

    }
    
    @IBAction func cartBtnClicked(){
//        if self.isLoggedInUser{
            let cartVC = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
            cartVC.isGuestUser = true
            self.navigationController?.pushViewController(cartVC, animated: true)
//        }
    }
    
    @IBAction func cardsBtnClicked(){
        let cardVC = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "CardsVC") as! CardsViewController
        cardVC.isGiftCard = false
        self.navigationController?.pushViewController(cardVC, animated: true)
        //        CardsVC
    }
    
    @IBAction func rentalBtnClicked(){
        if categoriesList == nil{
            return
        }
        var data : ProductCategory?
        for obj in categoriesList!
        {
            if obj.id == "3" {
                data = obj
                break
            }
        }
        let productListVC = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "ShopItesmTabViewController") as! ShopItesmTabViewController
        productListVC.categoryName = data?.title ?? ""
        productListVC.categoriesList = data?.children
        self.navigationController?.pushViewController(productListVC, animated: true)
        
    }
    
    @IBAction func giftBtnClicked(){
        let cardVC = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "CardsVC") as! CardsViewController
        cardVC.isGiftCard = true
        self.navigationController?.pushViewController(cardVC, animated: true)
    }
    
    @IBAction func gearBtnClicked(){
        if categoriesList == nil{
            return
        }
        var data : ProductCategory?
        for obj in categoriesList!
        {
            if obj.id == "2" {
                data = obj
                break
            }
        }
    
        let productListVC = UIStoryboard.init(name: "User", bundle: nil) .instantiateViewController(withIdentifier: "ShopItesmTabViewController") as! ShopItesmTabViewController
        productListVC.categoryName = data?.title ?? ""
        productListVC.categoriesList = data?.children
        self.navigationController?.pushViewController(productListVC, animated: true)
        
    }
    

    
}

extension GuestViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 780;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"TabCell",for: indexPath) as! GustDashBoardTableViewCell
        return cell
    }

}



extension GuestViewController:WebServiceTaskManagerProtocol{
    func didFinishTask(from manager:AnyObject, response:(data:RestResponse?,error:String?)){
        self.removeLoadingIndicator()
        if response.error != nil{
            //            self.showErrorAlert(message: response.error ?? " ")
        }else{
            if let catRes = response.data?.responseModel as? CategoryListRes{
                self.categoryRes = catRes
                self.categoriesList = catRes.data
            }
        }
    }
    
}
