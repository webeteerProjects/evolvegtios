//
//  AppDelegate.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 14/05/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import SideMenuSwift
import IQKeyboardManagerSwift
import Braintree
import UserNotifications
import Firebase
import UserNotifications
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let gcmMessageIDKey = "gcm.message_id"

    var window: UIWindow?
    var tabarCntlr : UITabBarController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        BTAppSwitch.setReturnURLScheme("com.evolve.appstore.payments")
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        UINavigationBar.appearance().isTranslucent = false
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor =  UIColor.white
        navigationBarAppearace.barTintColor =  UIColor.init(hexString: "#28AC10")
        navigationBarAppearace.shadowImage = UIImage()
        navigationBarAppearace.setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]

        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        
        //Solicit permission from user to receive notifications
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
        
        //get application instance ID
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        application.registerForRemoteNotifications()
        showHomeView()
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
            let json = JSON(notification)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                self.showNotificationDetailsView(notification: json)
            })
        }
        return true
    }

    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("com.evolve.appstore.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        return false
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func showHomeView(){
//        if UserInfo.currentUser()?.email != nil{
//            let email : String =  (UserInfo.currentUser()?.email)!
//            AppSharedData.sharedInstance.email = email
//        }
//        if UserInfo.currentUser()?.password != nil{
//            let pwd : String =  (UserInfo.currentUser()?.password)!
//            AppSharedData.sharedInstance.password = pwd
//        }
        
        SideMenuController.preferences.basic.menuWidth = 240

        AppSharedData.sharedInstance.startReachabilityNotification()
        UINavigationBar.appearance().isTranslucent = false
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor =  UIColor.white
        navigationBarAppearace.shadowImage = UIImage()
        navigationBarAppearace.setBackgroundImage(UIImage(), for: .default)
        navigationBarAppearace.barTintColor =  UIColor.init(hexString: "#08A53A")
        UITabBar.appearance().barTintColor = UIColor.black
        let storboard = UIStoryboard.init(name: "User", bundle: nil)
        let sideMenuVC = storboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuListViewController
        self.tabarCntlr = storboard.instantiateViewController(withIdentifier: "TabView") as! UITabBarController
        _ = UserInfo.restoreSession()
        if ((UserInfo.currentUser()?.userID) != nil){
            if (UserInfo.currentUser()?.role ==  "administrator"){
                let adminStorboard = UIStoryboard.init(name: "Admin", bundle: nil)
                let nav =  adminStorboard.instantiateViewController(withIdentifier: "AdminNavVC") as! UINavigationController
                UIView.transition(with: self.window!, duration: 0.1
                    , options: .transitionCrossDissolve, animations: {
                        let oldState: Bool = UIView.areAnimationsEnabled
                        UIView.setAnimationsEnabled(false)
                        self.window?.rootViewController = nav
                        UIView.setAnimationsEnabled(oldState)
                }, completion: { (finished: Bool) -> () in
                })
            
            }else{
                UIView.transition(with: self.window!, duration: 0.1
                    , options: .transitionCrossDissolve, animations: {
                        let oldState: Bool = UIView.areAnimationsEnabled
                        UIView.setAnimationsEnabled(false)
                        //                self.window?.rootViewController = tabBarContrl
                        let sideMenuController = SideMenuController(contentViewController: self.tabarCntlr!,
                                                                             menuViewController: sideMenuVC)
                        let navigationController = UINavigationController(rootViewController: sideMenuController)
                        navigationController.view.backgroundColor = #colorLiteral(red: 0.03021821566, green: 0.6054252386, blue: 0.2137703896, alpha: 1)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        UIView.setAnimationsEnabled(oldState)
                }, completion: { (finished: Bool) -> () in
                })
            }
           
        }else{
            
        }
      
//            if AppSharedData.sharedInstance.isConfiguredFireBase == false{
//                AppSharedData.sharedInstance.isConfiguredFireBase = true
//            }
            //            if AppSharedData.sharedInstance.pushcount == 1{
            //                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowBadgeVal"), object:nil)
            //            }
       
    }

    func showAdminView(){
        AppSharedData.sharedInstance.isCurrentSwitchModuleAdmin = true
        AppSharedData.sharedInstance.isCurrentSwitchModuleUser = false
        let adminStorboard = UIStoryboard.init(name: "Admin", bundle: nil)
        let nav =  adminStorboard.instantiateViewController(withIdentifier: "AdminNavVC") as! UINavigationController
        UIView.transition(with: self.window!, duration: 0.1
            , options: .transitionCrossDissolve, animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                self.window?.rootViewController = nav
                UIView.setAnimationsEnabled(oldState)
        }, completion: { (finished: Bool) -> () in
        })
    }
    
    func showUserView(){
        AppSharedData.sharedInstance.isCurrentSwitchModuleAdmin = false
        AppSharedData.sharedInstance.isCurrentSwitchModuleUser = true
        let storboard = UIStoryboard.init(name: "User", bundle: nil)
        let sideMenuVC = storboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuListViewController
        self.tabarCntlr = storboard.instantiateViewController(withIdentifier: "TabView") as! UITabBarController
        UIView.transition(with: self.window!, duration: 0.1
            , options: .transitionCrossDissolve, animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                let sideMenuController = SideMenuController(contentViewController: self.tabarCntlr!,
                                                            menuViewController: sideMenuVC)
                let navigationController = UINavigationController(rootViewController: sideMenuController)
                navigationController.view.backgroundColor = #colorLiteral(red: 0.03021821566, green: 0.6054252386, blue: 0.2137703896, alpha: 1)
                navigationController.isNavigationBarHidden = true
                self.window?.rootViewController = navigationController
                UIView.setAnimationsEnabled(oldState)
        }, completion: { (finished: Bool) -> () in
        })

    }
    
    func pushSideMenuSelection(_ controller: UIViewController) {
        guard let rootNavigationController = window?.rootViewController as? UINavigationController else { return }
        guard let sideMenuController = rootNavigationController.viewControllers.first as? SideMenuController else { return }
        sideMenuController.hideMenu { completed in
            guard completed else { return }
            rootNavigationController.pushViewController(controller, animated: true)
        }
    }
//    window?.rootViewController = SideMenuController(contentViewController: contentViewController,
//    menuViewController: menuViewController)
    
  
    func showBadgeCount(){
//        let tabbarCntlr = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "TabView") as? UITabBarController
        if let tabItems = self.tabarCntlr?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3] as! UITabBarItem
            if AppSharedData.sharedInstance.cartCount > 0{
                tabItem.badgeValue = String(AppSharedData.sharedInstance.cartCount)
            }else{
                tabItem.badgeValue = nil
            }
        }
    }
 
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }

}


extension AppDelegate: UNUserNotificationCenterDelegate{
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        let json = JSON(userInfo)
        self.showNotificationDetailsView(notification: json)

        
        completionHandler()
    }
    
}

extension AppDelegate: MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.set(fcmToken, forKey:Constant.DeviceToken.KUserDeviceTokenKey)
        UserDefaults.standard.synchronize()
        let loginServiceMngr =  LoginServiceManager()
        loginServiceMngr.updateDeviceToken(deviceToken: fcmToken)
//        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func showNotificationDetailsView(notification:JSON){
        if let caseID = notification["event_slug"].string{
            let tmpStoryBaord = UIStoryboard.init(name: "User", bundle: nil)
            let detailsView = tmpStoryBaord.instantiateViewController(withIdentifier: "EventDetailedViewController") as! EventDetailedViewController
            detailsView.eventName = notification["event_title"].string
            detailsView.eventSlug = caseID
            detailsView.hidesBottomBarWhenPushed = false
//            self.pushSideMenuSelection(detailsView)
            guard let rootNavigationController = window?.rootViewController as? UINavigationController else { return }
            guard let sideMenuController = rootNavigationController.viewControllers.first as? SideMenuController else { return }
            if let topController = UIApplication.topViewController(base:  sideMenuController.contentViewController){
                    topController.navigationController?.pushViewController(detailsView, animated: true)
            }
            
//            sideMenuController.contentViewController.tabBarController?.selectedViewController.
//            let rootNavigationController = window?.rootViewController as? UINavigationController
//            rootNavigationController?.pushViewController(detailsView, animated: true)

//            if let topController = UIApplication.topViewControllerInApp() {
//                //                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowBadgeVal"), object:nil)
//                //                UIApplication.shared.applicationIconBadgeNumber = 0
//                topController.navigationController?.pushViewController(detailsView, animated: true)
//            }else{
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowBadgeVal"), object:nil)
           // }
        }else{
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowBadgeVal"), object:nil)
            
        }
    }
}
