
//
//  CardListCollectionViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 18/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher

class CardListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var cardImage: UIImageView!

    var cardData : CardInfo?
    
    func showData(){
        self.contentView.showRoundCorner(roundCorner: 3.0)
        titleLbl.text = cardData?.title
        if priceLbl != nil{
            let priceStr = cardData?.price ?? "0"
            priceLbl.text = "$" + priceStr
        }else{
            titleLbl.text = cardData?.content
        }
        if let imgUrl = cardData?.image{
            self.cardImage.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
        }
//        showShadow()
    }
    
    func showShadow(){
        self.contentView.layer.cornerRadius = 2.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
}
