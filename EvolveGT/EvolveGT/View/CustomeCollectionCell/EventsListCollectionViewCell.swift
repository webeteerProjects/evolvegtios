//
//  EventsListCollectionViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 09/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher

class EventsListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var hostbyLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var eventImage: UIImageView!
    var eventData : Result?

    func showData(){
        self.contentView.showRoundCorner(roundCorner: 5.0)
        self.contentView.clipsToBounds = true
        titleLbl.text = eventData?.title
        hostbyLbl.text = "Hosted By "
        hostbyLbl.text?.append(eventData?.eventType ?? "")
        if eventData?.rolePrice !=  nil{
            let priceStr = eventData?.rolePrice?.currentUserTypePrice ?? "0.00"
            priceLbl.text = "Starting From: $" + priceStr
        }else{
            let priceStr = eventData?.price ?? "0.00"
            priceLbl.text = "Starting From: $" + priceStr
        }

//        let dateStr = eventData?.eventDate ?? ""
         dateLbl.text = "Event Date: "
        if let dateStr = eventData?.eventDate{
//            let joinDate = Date(fromString: dateStr, format: .custom("yyyy-MM-dd HH:mm:ss"))
//            let string = joinDate?.toString(format: .custom("dd MMM YYYY"))
            let joinDate = Date(fromString: dateStr, format: .isoDate)
            let string = joinDate?.toString(format: .custom("dd MMM YYYY"))
            dateLbl.text?.append(string ?? "")

            
        }
        if let imgUrl = eventData?.fullEventLogo{
            self.eventImage.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
        }
        
    }
}
