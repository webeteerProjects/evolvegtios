//
//  MembershipCollectionViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 26/11/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class MembershipCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var hostbyLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var viewMoreBtn: UIButton!
    @IBOutlet weak var purchaseBtn: UIButton!
    @IBOutlet weak var infoContainerView: UIView!
    var memberShipData : Membership?
    var currentPlan:UserMembership?
    var season:String?
    

    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
    }
    
    func showData(){
        viewMoreBtn.layer.borderColor =  UIColor.init(hexString: "#28AC10").cgColor
        viewMoreBtn.layer.borderWidth = 2.0
        let priceStr = memberShipData?.price ?? "0"
        priceLbl.text = "$" + priceStr
        titleLbl.text  = memberShipData?.title
        hostbyLbl.text  = season
        purchaseBtn.isHidden = false;
        purchaseBtn.setTitle("", for: .normal)
        if currentPlan?.memberShipIdValue ?? 0  == memberShipData?.memberShipIdValue ?? 0{
            purchaseBtn.isUserInteractionEnabled = false
            purchaseBtn.backgroundColor =  UIColor.init(hexString: "#28AC10")
            purchaseBtn.setTitle("CURRENT PLAN", for: .normal)
        }else{
            if let status = memberShipData?.stockStatus {
                   if status != "instock"{
                       purchaseBtn.isEnabled = false
                       purchaseBtn.isUserInteractionEnabled = false
                       purchaseBtn.backgroundColor = UIColor.gray
                       purchaseBtn.setTitleColor(UIColor.white, for: .disabled)
                        purchaseBtn.setTitle("SOLD OUT", for: .normal)

                   }else{
                       purchaseBtn.isUserInteractionEnabled = true
                       purchaseBtn.isEnabled = true
                       purchaseBtn.backgroundColor =  UIColor.init(hexString: "#28AC10")
                       purchaseBtn.setTitleColor(UIColor.white, for: .normal)
                       purchaseBtn.setTitle("PURCHASE", for: .normal)
                   }
               }
        }
       

        if currentPlan?.memberShipIdValue ?? 0  > memberShipData?.memberShipIdValue ?? 0{
            purchaseBtn.isUserInteractionEnabled = false
            purchaseBtn.isHidden = true;
        }
        else{
            if memberShipData?.title == "GUEST"{
                purchaseBtn.isHidden = true;
            }
        }
        
    }
    
    func showSelection(status:Bool){
        if status == true{
            infoContainerView.backgroundColor =  UIColor.init(hexString: "#28AC10")
            titleLbl.textColor = UIColor.white
            hostbyLbl.textColor = UIColor.white
            priceLbl.textColor = UIColor.white
        }else{
            infoContainerView.backgroundColor = UIColor.white
            titleLbl.textColor = UIColor.black
            hostbyLbl.textColor = UIColor.black
            priceLbl.textColor = UIColor.black
        }
    }
}
