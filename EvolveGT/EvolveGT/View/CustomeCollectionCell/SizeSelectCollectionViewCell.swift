


//
//  SizeSelectCollectionViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 28/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class SizeSelectCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var sizeLbl: UILabel!
    @IBOutlet weak var radioBtnImage: UIImageView!
    var isSelectedSize = false
    var info : Variation?
    
    func showData(){
        
        if info?.isSelected ?? false {
            radioBtnImage.image = UIImage(named:"button_selected")
        }else{
            radioBtnImage.image = UIImage(named:"button_deselected")
        }
        sizeLbl.text = info?.size
    }
}
