//
//  ProductListCollectionViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 09/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class ProductListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!

}
