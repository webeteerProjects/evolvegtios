//
//  QuantityTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 18/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
protocol QuantityCellDelegate {
    func didSelectNoQuantity(quanity:Int)
}

class QuantityTableViewCell: UITableViewCell {
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var toatlPriceLbl: UILabel!
    @IBOutlet weak var quantityContainerView: UIView!
    var delegate : QuantityCellDelegate?
    var price: Float  = 0.00
    var quantity : Int = 1
    var total: Float  = 0.00


    override func awakeFromNib() {
        super.awakeFromNib()
        quantityContainerView.showRoundBorder()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func showValues(){
        total = price * Float(quantity)
        let priceValStr = String(format: "%.2f", total)
        let totalStr = "Total : $" + priceValStr
        toatlPriceLbl.text = totalStr
        AppSharedData.sharedInstance.toatlPrice = total
    }
    
    
    @IBAction func plusBtnClicked(){
        quantity = quantity + 1
        total = price * Float(quantity)
        let priceValStr = String(format: "%.2f", total)
        let totalStr = "Total : $" + priceValStr
        toatlPriceLbl.text = totalStr
        AppSharedData.sharedInstance.toatlPrice = total
        quantityLbl.text = quantity.description
        self.delegate?.didSelectNoQuantity(quanity: quantity)
    }
    
    @IBAction func minusBtnClicked(){
        if quantity < 1{
            quantity = 0
        }else{
            quantity = quantity - 1
        }
        total = price * Float(quantity)
        let priceValStr = String(format: "%.2f", total)
        let totalStr = "Total : $" + priceValStr
        toatlPriceLbl.text = totalStr
        AppSharedData.sharedInstance.toatlPrice = total
        quantityLbl.text = quantity.description
        if (quantity == 0){
            self.delegate?.didSelectNoQuantity(quanity: quantity)
        }
    }
}
