

//
//  ImageTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 18/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
