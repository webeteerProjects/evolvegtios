

//
//  InputInfoTableViewCell.swift
//  
//
//  Created by Ajeesh T S on 07/07/19.
//

import UIKit
import SkyFloatingLabelTextField

class InputInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var emailTextfld: SkyFloatingLabelTextField!
    @IBOutlet weak var nameTextfld: SkyFloatingLabelTextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
