
//
//  AttributeTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 16/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class AttributeTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var selectedItemLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
