

//
//  ItemInfoTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 18/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class ItemInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var geustPriceLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
