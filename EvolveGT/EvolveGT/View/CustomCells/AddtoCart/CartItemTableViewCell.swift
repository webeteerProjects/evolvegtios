
//
//  CartItemTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 18/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher

class CartItemTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var toatlPriceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var borderContainerView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    var itemInfo : CartItem?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.showRoundCorner(roundCorner: 3.0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showData(){
        titleLbl.text = itemInfo?.title
        let priceStr = itemInfo?.price ?? "0.00"
        let quantityStr = itemInfo?.quantity ?? " "
        if itemInfo?.source == "archie"{
            quantityLbl.text = "Quantity: " + quantityStr
        }
        else if itemInfo?.source == "rentals"{
//            quantityLbl.text = "Quantity: " + quantityStr
            var hostStr = "Event: "
            hostStr.append(itemInfo?.parentTitle ?? "")
            quantityLbl.text = hostStr

            if let giftInfo = itemInfo?.unAttributes.first{
                var detailsStr = giftInfo.name
                var fullInfo  =  "Size: "
                fullInfo.append(detailsStr ?? "")
                if emailLbl != nil{
                    emailLbl.text = fullInfo
                }
            }
        }
        else if itemInfo?.source == "training"{
//            quantityLbl.text = "Quantity: " + quantityStr
            var hostStr = "Event: "
            hostStr.append(itemInfo?.parentTitle ?? "")
            quantityLbl.text = hostStr

        }
        else if itemInfo?.source == "event"{
            quantityLbl.text = "Hosted By: "
            var hostStr = "Hosted By: "
            hostStr.append(itemInfo?.evtype ?? "")
            quantityLbl.text = hostStr
        }
        else if itemInfo?.source == "giftcard"{
            if let giftInfo = itemInfo?.unAttributes.first{
                var detailsStr = giftInfo.name
                detailsStr?.append("\n")
                detailsStr?.append(giftInfo.email)
                var fullInfo  =  "Gift to: "
                fullInfo.append(detailsStr ?? "")
                quantityLbl.text = fullInfo
                if emailLbl != nil{
                    emailLbl.text = giftInfo.email
                }
            }
           
            
        }

        toatlPriceLbl.text = "Price: $" + priceStr
        if itemInfo?.source == "archie"{
            let qnty = Int(itemInfo?.quantity ?? "1")
            if let priceVal = Float(itemInfo?.price ?? "0.00"){
                let itemPrice = priceVal * Float(qnty ?? 1)
//                let totalPriceStr = itemPrice.description
                let totalPriceStr = String(format: "%.2f", itemPrice)
                toatlPriceLbl.text = "Price: $" + totalPriceStr
            }
        }
        if let imgUrl = itemInfo?.image{
            self.imgView.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
        }
    }

}

class CartPreviewTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var toatlPriceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    var itemInfo : CartItem?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func showData(){
        titleLbl.text = itemInfo?.title
        let priceStr = itemInfo?.price ?? "0.00"
        let priceLblText = "Price: $" + priceStr
        let quantityStr = itemInfo?.quantity ?? " "
        quantityLbl.text = "Qty:" + quantityStr + "    |    " + priceLblText
        toatlPriceLbl.text = "Total: $" + priceStr
        if itemInfo?.source == "event"{
            if let latepriceVal = Float(itemInfo?.feeAmount ?? "0.00"){
                if latepriceVal > 0 {
                    let tmpV = itemInfo?.totalAmount ?? 0.00
                    let latefreericeStr = String(format: "%.2f", tmpV)
                    //            let amountStr = String(format: "%.2f", self.totalAmount)

                    toatlPriceLbl.text = "Total: $" + latefreericeStr
                    quantityLbl.text?.append("    |    ")
                    quantityLbl.text?.append("Late Fee: $")
                    quantityLbl.text?.append(itemInfo?.feeAmount ?? "")
                }
            }
        }
        
        if itemInfo?.source == "archie"{
            let qnty = Int(itemInfo?.quantity ?? "1")
            if let priceVal = Float(itemInfo?.price ?? "0.00"){
                let itemPrice = priceVal * Float(qnty ?? 1)
                if itemPrice > 0 {
                    let totalPriceStr = String(format: "%.2f", itemPrice)
                    toatlPriceLbl.text = "Total: $" + totalPriceStr
                }else{
                    toatlPriceLbl.text = "Total: $0.00"
                }
            }
        }
        
    }

}

class CartToatlPriceTableViewCell: UITableViewCell {
    @IBOutlet weak var subTotalPriceLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func showData(){
    }
    
}


class AddPromoTableViewCell: UITableViewCell {
    @IBOutlet weak var addpromoTxtFld: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   
}


class AddressTableViewCell: UITableViewCell {
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var addressBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class WalletAmountTableViewCell: UITableViewCell {
    @IBOutlet weak var amountLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class CartTotalTableViewCell: UITableViewCell {
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var couponLbl: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var walletAmntLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}


class PayementMethodTableViewCell: UITableViewCell {
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var amountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}


class PayementSucessInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var orderNoLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
