//
//  EventParticipantTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 11/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

protocol EventParticipantDelegate: class {
    func clickedOnActionItemForSign(_ cell: EventParticipantTableViewCell, event: AdminEventUser?)
    func clickedOnUpgradeSkill(_ cell: EventParticipantTableViewCell, event: AdminEventUser?)
    func clickedOnActionItemForTraining(_ cell: EventParticipantTableViewCell, trainingInfo: [String])
}

class EventParticipantTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var skillLevelLabel: UILabel!
    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var orderNoLabel: UILabel!
    @IBOutlet weak var trainingButton: UIButton!
    @IBOutlet weak var signButton: UIButton!
    @IBOutlet weak var skillUpGradeButton: UIButton!
    var indePathForRef : IndexPath!

    var eventUser: AdminEventUser? {
        didSet {
            updateUI()
        }
    }
    weak var delegate: EventParticipantDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func prepareForReuse() {
        // CELLS STILL FREEZE EVEN WHEN THE FOLLOWING LINE IS COMMENTED OUT?!?!
        super.prepareForReuse()
        signButton.isHidden = false

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func clickedOnActionItem(_ sender: UIButton) {
        delegate?.clickedOnActionItemForSign(self, event: eventUser)
    }
    
    @IBAction func clckedOnTrainingInfo(_ sender: UIButton) {
        delegate?.clickedOnActionItemForTraining(self, trainingInfo: eventUser?.training ?? [])
    }
    
    
    @IBAction func clickedOnUpgradeSkillActionItem(_ sender: UIButton) {
        delegate?.clickedOnUpgradeSkill(self, event: eventUser)
    
    }
    
    func updateUI() {
        nameLabel.text = ((eventUser?.displayName ?? "") + "(\(eventUser?.role ?? ""))").uppercased()
        skillLevelLabel.text = eventUser?.skillLevel
        userIdLabel.text = "#"
        userIdLabel.text?.append(eventUser?.userId ?? "")
        dobLabel.text = eventUser?.dobFormatted ?? "  "
        emailLabel.text = eventUser?.email
        signButton.isSelected = !(eventUser?.hasSign ?? false)
        trainingButton.isHidden = eventUser?.training?.isEmpty ?? true
        orderNoLabel.text = "#"
        orderNoLabel.text?.append(eventUser?.orderId ?? "")
        if let user = eventUser, let signEnabled = user.isSignEnabled {
            signButton.isHidden = signEnabled == "0"
        }else{
//            print("Else")
        }
    }
}
