
//
//  EventListTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 09/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class EventListTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var cancelBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
