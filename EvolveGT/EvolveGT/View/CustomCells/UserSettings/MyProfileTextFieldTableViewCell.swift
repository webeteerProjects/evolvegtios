//
//  MyProfileTextFieldTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 25/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
protocol MyProfileTextFieldTableViewCellDelegate: class {
    func startedEditingTextField(_ cell: MyProfileTextFieldTableViewCell, textField: UITextField, textType: ProfileTextType)
    func finishedEditingTextField(_ cell: MyProfileTextFieldTableViewCell, textField: UITextField, textType: ProfileTextType)
    func textEditingTextField(_ cell: MyProfileTextFieldTableViewCell, textField: UITextField, textType: ProfileTextType, newText: String)

}

class MyProfileTextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: SkyFloatingLabelTextField!
    var textType: ProfileTextType?
    
    weak var delegate: MyProfileTextFieldTableViewCellDelegate?
    var characterLimit = 0
    var actionView: UIView? = nil {
        didSet {
            textField.inputView = actionView
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        textField.rightView = nil
        textField.textColor = .darkText
        textField.placeholderColor = .darkText
        textField.keyboardType = .default
        self.characterLimit = 0
    }
    
    func makeFontLighter() {
        textField.textColor = .lightGray
        textField.placeholderColor = .lightGray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension MyProfileTextFieldTableViewCell: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.startedEditingTextField(self, textField: textField, textType: textType ?? .firstName)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.finishedEditingTextField(self, textField: textField, textType: textType ?? .firstName)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: NSString = (textField.text ?? "") as NSString
        let newText = text.replacingCharacters(in: range, with: string)
//        delegate?.finishedEditingTextField(self, textField: textField, textType: textType ?? .firstName)
        delegate?.textEditingTextField(self, textField: textField, textType: textType ?? .firstName, newText: newText)
        if characterLimit > 0 {
            let currentCharacterCount = textField.text?.count ?? 0
            if range.length + range.location > currentCharacterCount {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= characterLimit
        }
        return true
    }
    
}
