//
//  MyProfileQuestionCellTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 25/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

protocol MyProfileQuestionCellDelegate: class {
    func choseOption(_ cell: MyProfileQuestionCellTableViewCell, questionType: QuestionType, option: String)
}

class MyProfileQuestionCellTableViewCell: UITableViewCell {

    enum AnswerOption {
        case one
        case two
        case unselected
    }
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var firstOptionLabel: UILabel!
    @IBOutlet weak var secondOptionLabel: UILabel!
    var questionType: QuestionType = .gender
    var optionValue: AnswerOption = .unselected {
        didSet {
            firstButton.isSelected = optionValue == .one
            secondButton.isSelected = optionValue == .two
        }
    }
    
    weak var delegate: MyProfileQuestionCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let unselectedImage = #imageLiteral(resourceName: "circle-outline").withRenderingMode(.alwaysTemplate)
        let selectedImage = #imageLiteral(resourceName: "radio-on-button").withRenderingMode(.alwaysTemplate)
        firstButton.setImage(unselectedImage, for: .normal)
        firstButton.setImage(selectedImage, for: .selected)
        secondButton.setImage(unselectedImage, for: .normal)
        secondButton.setImage(selectedImage, for: .selected)
        firstButton.tintColor = .darkText
        secondButton.tintColor = .darkText
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        firstOptionLabel.textColor = .darkText
        secondOptionLabel.textColor = .darkText
        questionLabel.textColor = .darkText
        firstButton.tintColor = .darkText
        secondButton.tintColor = .darkText
    }

    func makeFontLighter() {
        firstOptionLabel.textColor = .lightGray
        secondOptionLabel.textColor = .lightGray
        questionLabel.textColor = .lightGray
        firstButton.tintColor = .lightGray
        secondButton.tintColor = .lightGray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func clickedFirstOption(_ sender: UIButton) {
        if optionValue != .one {
            optionValue = .one
            delegate?.choseOption(self, questionType: questionType, option: firstOptionLabel.text ?? "")
        }
    }
    
    @IBAction func clickedSecondOption(_ sender: UIButton) {
        if optionValue != .two {
            optionValue = .two
            delegate?.choseOption(self, questionType: questionType, option: secondOptionLabel.text ?? "")
        }
    }
}
