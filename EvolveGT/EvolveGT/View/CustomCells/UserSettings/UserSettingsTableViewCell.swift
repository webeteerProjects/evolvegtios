//
//  UserSettingsTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 24/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
protocol UserSettingChangeDelegate: class {
    func changedSetting(for cell: UITableViewCell, switch: UISwitch, isOn: Bool, notificationType: NotificationType)
}

class UserSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var settingTitleLabel: UILabel!
    @IBOutlet weak var settingStatSwitch: UISwitch!
    @IBOutlet weak var settingValueLabel: UILabel!
    weak var delegate: UserSettingChangeDelegate?
    var notificationType: NotificationType?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func didChangeSwitchState(_ sender: UISwitch) {
        notificationType?.status = sender.isOn
        delegate?.changedSetting(for: self, switch: sender, isOn: sender.isOn, notificationType: notificationType!)
    }
    
}
