//
//  CreditHistoryTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 17/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class CreditHistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var borderContainerView: UIView!
    @IBOutlet weak var expandBtn: UIButton!
    var creditInfo : CreditHistory?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showData(){
        let amountStr = creditInfo?.amount ?? ""
        self.nameLbl.text  =  "$" + amountStr
        orderDateLbl.text = creditInfo?.descriptionField
        dateLbl.text = "Posted on: "
        if let dateStr = creditInfo?.postDate{
            let joinDate = Date(fromString: dateStr, format: .custom("yyyy-MM-dd HH:mm:ss"))
            let string = joinDate?.toString(format: .custom("dd MMM YYYY"))
            self.dateLbl.text?.append(string ?? dateStr)
        }
    }

}
