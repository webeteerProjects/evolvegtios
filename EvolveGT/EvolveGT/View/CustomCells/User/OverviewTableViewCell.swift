//
//  OverviewTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 27/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class OverviewTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
