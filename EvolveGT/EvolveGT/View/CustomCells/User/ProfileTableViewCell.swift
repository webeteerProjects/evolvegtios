
//
//  ProfileTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 17/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import AFDateHelper
import Kingfisher

class ProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var proflieImage: UIImageView!
    @IBOutlet weak var eventCountContainerView: UIView!
    @IBOutlet weak var skillContainerView: UIView!
    @IBOutlet weak var pastEventCountContainerView: UIView!
    @IBOutlet weak var allEventCountContainerView: UIView!
    @IBOutlet weak var profileImgContainerView: UIView!

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var memberDateLbl: UILabel!
    @IBOutlet weak var memberStausLbl: UILabel!
    @IBOutlet weak var walletLbl: UILabel!
    @IBOutlet weak var memberExpLbl: UILabel!
    @IBOutlet weak var upcomingEvntCountLbl: UILabel!
    @IBOutlet weak var skillLbl: UILabel!
    @IBOutlet weak var pastEventsLbl: UILabel!
    @IBOutlet weak var toatlEventLbl: UILabel!
    
    var userInfo : UserDetails?
    var eventSummary : EventSummary?

    override func awakeFromNib() {
        super.awakeFromNib()
        proflieImage.showRoundCorner()
        proflieImage.layer.borderColor = UIColor.white.cgColor
        proflieImage.layer.borderWidth = 2.0
        allEventCountContainerView.showRoundBorder()
        eventCountContainerView.showRoundBorder()
        skillContainerView.showRoundBorder()
        pastEventCountContainerView.showRoundBorder()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showData(){
        
        if let imgUrl = userInfo?.profileImageUrl{
            if imgUrl.isValidString{
                self.proflieImage.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
            }
        }
        self.nameLbl.text = userInfo?.firstName ?? ""
        self.nameLbl.text?.append(" ")
        self.nameLbl.text?.append(userInfo?.lastName ?? "")
        self.memberDateLbl.text = "Member Since: "
        if let dateStr = userInfo?.registered{
            let joinDate = Date(fromString: dateStr, format: .custom("yyyy-MM-dd HH:mm:ss"))
//            let string = joinDate?.toString(format: .isoDate)
            let string = joinDate?.toString(format: .custom("dd MMM YYYY"))
            self.memberDateLbl.text?.append(string ?? "")
        }
        self.nameLbl.text =  self.nameLbl.text?.capitalized
//        self.memberDateLbl.text = userInfo?.registered
        self.walletLbl.text =  "$"
        self.walletLbl.text?.append(userInfo?.walletAmount ?? "")
        if let statusVal =  userInfo?.status{
            if statusVal == "0" {
                self.memberStausLbl.text = "ACTIVE("
                self.memberStausLbl.text?.append(userInfo?.evRole ?? "")
                self.memberStausLbl.text?.append(")")
            }else{
                self.memberStausLbl.text = "INACTIVE("
                self.memberStausLbl.text?.append(userInfo?.evRole ?? "")
                self.memberStausLbl.text?.append(")")
            }
        }
//        self.memberExpLbl.text = userInfo?.membershipExpDate
        if let dateStr = userInfo?.membershipExpDate{
//            let joinDate = Date(fromString: dateStr, format: .custom("yyyy-mm-dd hh:mm:ss"))
//
             let joinDate = Date(fromString: dateStr, format: .custom("yyyy-MM-dd HH:mm:ss"))
//            let string = joinDate?.toString(format: .isoDate)
            let string = joinDate?.toString(format: .custom("dd MMM yyyy"))

            self.memberExpLbl.text = string
            if let dateVal = joinDate{
                if dateVal.compare(.isInThePast){
                    self.memberExpLbl.textColor =  .red
                }
            }
           
        }
        self.skillLbl.text = userInfo?.skillLevel
        self.pastEventsLbl.text = "0"
        self.toatlEventLbl.text = "0"
        self.upcomingEvntCountLbl.text = "0"
        
        if self.eventSummary != nil{
            let pEvnts = self.eventSummary?.pastEventCount ?? 0
            let upEvnts = self.eventSummary?.upcomingEventCount ?? 0
            let totalEvent = pEvnts + upEvnts
            self.pastEventsLbl.text = String(describing: pEvnts)
            self.toatlEventLbl.text =  String(describing:totalEvent)
            self.upcomingEvntCountLbl.text =  String(describing:upEvnts)

        }
    }
    
    

}
