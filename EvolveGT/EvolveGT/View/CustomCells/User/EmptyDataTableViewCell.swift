
//
//  EmptyDataTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 17/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class EmptyDataTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var borderContainerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
