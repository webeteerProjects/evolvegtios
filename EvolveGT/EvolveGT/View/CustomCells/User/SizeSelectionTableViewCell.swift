

//
//  SizeSelectionTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 28/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

protocol TrainigSelectionCellDelegate {
    func refreshSelectedTrianingItem(indexPath:IndexPath)
}

protocol SizeSelectionCellDelegate {
    func refreshSelectedItem(indexPath:IndexPath)
    func didSelectedSize(sizeInfo: Variation)
}

class SizeSelectionTableViewCell: UITableViewCell {
    var delegate : SizeSelectionCellDelegate?
    @IBOutlet weak var sizeListCollectionView: UICollectionView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var sizeTextLbl: UILabel!
    @IBOutlet weak var checkBoxImage: UIImageView!
    @IBOutlet var bottomLayout : NSLayoutConstraint!
    @IBOutlet var szieLabelHeightLayout : NSLayoutConstraint!

    var isSelectedMe = false
    var rentalInfo : RentalData?
    var selectedIndexPath = 0
    var indexPathTableView : IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showData(){
        titleLbl.text = rentalInfo?.title
//        priceLbl.text = rentalInfo?.price
        priceLbl.text = "$"
        priceLbl.text?.append(self.rentalInfo?.price ?? "")

        if rentalInfo?.isSelected ?? false {
            checkBoxImage.image = UIImage(named:"button_selected")
            sizeListCollectionView.isHidden = false
            sizeTextLbl.isHidden = false
        }else{
            sizeListCollectionView.isHidden = true
            sizeTextLbl.isHidden = true
            checkBoxImage.image = UIImage(named:"button_deselected")
        }
    }
    
    @IBAction func expandButtonClicked(){
        rentalInfo?.isSelected = !rentalInfo!.isSelected
        self.delegate?.refreshSelectedItem(indexPath: self.indexPathTableView)
    }

}

extension SizeSelectionTableViewCell: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return self.rentalInfo?.variations?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let count = self.rentalInfo?.variations?.count ?? 0
        var width = 0.0
        if count > 0 {
            var columnC = count / 2
            if count % 2 != 0 {
                columnC = columnC + 1
            }
            width =  Double((Constant.ScreenSize.SCREEN_WIDTH - 60) / CGFloat(columnC))
        }
        return CGSize(width: width, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SizeSelectCollectionViewCell", for: indexPath as IndexPath) as! SizeSelectCollectionViewCell
            if self.rentalInfo?.variations.count ?? 0 > indexPath.row{
                if let data =  self.rentalInfo?.variations?[indexPath.row]{
                    cell.info = data
                    if selectedIndexPath != indexPath.row{
                        data.isSelected = false
                    }
                    cell.showData()
                }
            }
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if self.rentalInfo?.variations.count ?? 0 > indexPath.row{
            self.rentalInfo?.variations?[indexPath.row].isSelected =  !(self.rentalInfo?.variations?[indexPath.row].isSelected)!
            selectedIndexPath = indexPath.row
//            self.sizeListCollectionView.reloadItems(at: [indexPath])
//            self.delegate?.didSelectedSize(sizeInfo:(self.rentalInfo?.variations?[indexPath.row])!)
            self.sizeListCollectionView.reloadData()

        }
    }
}


class TrianingSelectionTableViewCell: UITableViewCell {
    var delegate : TrainigSelectionCellDelegate?
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var sizeTextLbl: UILabel!
    @IBOutlet weak var checkBoxImage: UIImageView!
    var isSelectedMe = false
    var trainingData : TrainingData?
    var selectedIndexPath = 0
    var indexPathTableView : IndexPath!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func showData(){
        priceLbl.text = "$"
        priceLbl.text?.append(self.trainingData?.price ?? "")
        titleLbl.text = trainingData?.title
        if trainingData?.isSelected ?? false {
            checkBoxImage.image = UIImage(named:"button_selected")
        }else{
            checkBoxImage.image = UIImage(named:"button_deselected")
        }
    }
    
    @IBAction func selectionButtonClicked(){
        trainingData?.isSelected = !trainingData!.isSelected
        self.delegate?.refreshSelectedTrianingItem(indexPath: self.indexPathTableView)
    }
}
