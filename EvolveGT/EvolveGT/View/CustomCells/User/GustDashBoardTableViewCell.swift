
//
//  GustDashBoardTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 03/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class GustDashBoardTableViewCell: UITableViewCell {
    @IBOutlet weak var tab1: UIView!
    @IBOutlet weak var tab2: UIView!
    @IBOutlet weak var tab3: UIView!
    @IBOutlet weak var tab4: UIView!
    @IBOutlet weak var tab5: UIView!
    @IBOutlet weak var tab6: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        tab1.showRoundBorder()
        tab2.showRoundBorder()
        tab3.showRoundBorder()
        tab4.showRoundBorder()
        if tab5 != nil{
            tab5.showRoundBorder()
        }
        if tab6 != nil{
            tab6.showRoundBorder()
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
