
//
//  EventInfoTableViewCell.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 17/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import Kingfisher

class EventInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var borderContainerView: UIView!
    @IBOutlet weak var expandBtn: UIButton!
    @IBOutlet weak var eventImage: UIImageView!
    var eventInfo : EventInfo?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showData(){
        let evntNameStr = eventInfo?.productName ?? ""
        self.nameLbl.text  =  "Event: " + evntNameStr
        self.dateLbl.text  =  self.eventInfo?.eventDate
//        let dateStr = eventInfo?.eventDate ?? ""
//        dateLbl.text = "Event Date: " + dateStr
//        let orderDateStr = eventInfo?.orderDate ?? ""
//        orderDateLbl.text = "Order Date: " + orderDateStr
        if let imgUrl = eventInfo?.eventImage{
            self.eventImage.kf.setImage(with: URL(string : imgUrl), placeholder: nil, options: [.transition(ImageTransition.fade(1))])
        }
        
        dateLbl.text = "Event Date: "
        if let dateStr = eventInfo?.eventDate{
//            let joinDate = Date(fromString: dateStr, format: .isoDate)
//            let string = joinDate?.toString(format: .isoDate)
            self.dateLbl.text?.append(dateStr)
        }
        
        orderDateLbl.text = "Order Date: "
        if let dateStr = eventInfo?.orderDate{
//            let joinDate = Date(fromString: dateStr, format: .custom("yyyy-MM-dd HH:mm:ss"))
//            let string = joinDate?.toString(format: .isoDate)
            self.orderDateLbl.text?.append(dateStr)
        }
    }

}
