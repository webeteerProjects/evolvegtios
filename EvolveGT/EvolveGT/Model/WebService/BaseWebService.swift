
//
//  BaseWebService.swift
//  FnB
//
//  Created by Ajeesh T S on 05/01/17.
//  Copyright © 2017 SimStream. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON



//Live
var baseUrl = "https://evolvegt.com/evolve-api/public/"
//var baseUrl = "https://evolvegt.webeteerprojects.com/evolve-api/public/"

//var baseUrl  = "https://evolvenewdev.webeteerprojects.com/evolve-api/public/"

//Live
var domain = "https://evolvegt.com/"
//var domain = "https://evolvegt.webeteerprojects.com/"

var userId = "5abbb5a71a776200019f77ee"
var pageCountNewsWebService = 10
var ClassifiedCountNewsWebService = 25

var baseImageURl = "https://evolvegt.com/evolve-api/public/"
//var baseImageURl = "https://evolvegt.webeteerprojects.com/evolve-api/public/"

class RestResponse : NSObject {
    var response : JSON?
    var responseModel : AnyObject?
    var responseCode : Int?
    var responseHttpCode : Int?
    var responseDetail : AnyObject?
    var error : Error?
    var paginationIndex : NSMutableString?
    var requestData : AnyObject?
    var message : String?
    var requestType : AnyObject?
}

class MultipartData: NSObject {
    var data: NSData
    var name: String
    var fileName: String
    var mimeType: String
    init(data: NSData, name: String, fileName: String, mimeType: String) {
        self.data = data
        self.name = name
        self.fileName = fileName
        self.mimeType = mimeType
        
    }
}

@objc protocol BaseServiceDelegates {
    //the service call was successful and the data is passed to the manager
    @objc optional func didSuccessfullyReceiveData(response:RestResponse?)
    
    //the service call was failed and the error message to be shown is
    //fetched and returned to the manager.
    @objc optional func didFailedToReceiveData(response:RestResponse?)
    
    @objc optional func imageUploadPrograss(prograssValue : Float)
    
}

enum WebServiceType {
    case None
    case FoodSpotReviewImageUploading
}


class BaseWebService: NSObject {
    var currentServiceType = WebServiceType.None
    var delegate : BaseServiceDelegates?
    var url: String?
    var parameters = [String: AnyObject]()
    var uploadData: [MultipartData]?
    var isMultipartArray = false
    var header:[String : String]?
    var requestInfo : [String:String]?
    var errorMsg : String?

    func failureWithError(error: Error?) {
        let restResponse = RestResponse()
        restResponse.error = error
        restResponse.message = errorMsg
        delegate?.didFailedToReceiveData!(response: restResponse)
    }
    
    func successWithResponse(response:JSON?){
        let restResponse = RestResponse()
        restResponse.response = response
        restResponse.requestData = self.requestInfo as AnyObject?
        delegate?.didSuccessfullyReceiveData!(response: restResponse)
    }

    func GET(){
        if UserInfo.currentUser()?.token != nil{
            let token : String = (UserInfo.currentUser()?.token)!
            header = [String : String]()
            header?.updateValue("Bearer \(token)", forKey: "Authorization")
            //            header?.updateValue("application/json", forKey: "Accept")
            header?.updateValue("application/json", forKey: "Content-Type")
        }else{
            header = nil
        }
        print(url)
        Alamofire.request(url!, parameters: parameters, headers:header)
            .validate()
            .responseJSON {response in
                switch response.result{
                case .success:
                    if let val = response.result.value {
                        let json = JSON(val)
                        self.successWithResponse(response: json)
                    }
                case .failure(let error):
                    if let data = response.data, let utf8Text = String.init(data: data, encoding: String.Encoding.utf8) {
                        print("Data: \(utf8Text)")
                        let json = JSON(response.data as Any)
                        if let errorDictionary = json["error"].dictionary{
                            if let firstItem = errorDictionary.first {
                                if let msg = firstItem.value.array{
                                    self.errorMsg = msg.first?.string
                                }
                            }
                        }
                        else {
                            if let msg = json["message"].string{
                                self.errorMsg = msg
                            }
                        }
                    }
                    self.failureWithError(error: error)
                }
            }
    }
    
    func POST() {
        if let data = uploadData {
            self.upload(data: data)
        }
        else {
            POST_normal()
        }
    }
    
    func POST_normal(){
        if UserInfo.currentUser()?.token != nil{
            let token : String = (UserInfo.currentUser()?.token)!
            let hearDict = ["Authorization":"Bearer \(token)"]
            header = hearDict
            header = [String : String]()
            header?.updateValue("Bearer \(token)", forKey: "Authorization")
//            header?.updateValue("application/json", forKey: "Accept")
//            header?.updateValue("application/json", forKey: "Content-Type")
        }else{
            
        }
        print(url)

        Alamofire.request(url!, method: .post, parameters: parameters,  encoding: JSONEncoding.default, headers: header)
            .validate()
            .responseJSON {response in
                switch response.result{
                case .success:
                    if let val = response.result.value {
                        let json = JSON(val)
                        self.successWithResponse(response: json)
                    }
                case .failure(let error):
                    if let data = response.data, let utf8Text = String.init(data: data, encoding: String.Encoding.utf8) {
//                        print("Data: \(utf8Text)")
                        let json = JSON(response.data as Any)
                        if let errorDictionary = json["error"].dictionary{
                            if let firstItem = errorDictionary.first {
                                if let msg = firstItem.value.array{
                                    self.errorMsg = msg.first?.string
                                }
                            }
                        }
                        else {
                            if let msg = json["message"].string{
                                self.errorMsg = msg
                            }
                        }
                    }
                    self.failureWithError(error: error)
                }
            }
    }
    
    
    func PUT(){
        if UserInfo.currentUser()?.token != nil{
            
        }else{
        }
        Alamofire.request(url!, method: .put, parameters: parameters,  encoding: JSONEncoding.default)
            .validate()
            .responseJSON {response in
                switch response.result{
                case .success:
                    if let val = response.result.value {
                        let json = JSON(val)
                        self.successWithResponse(response: json)
                    }
                case .failure(let error):
                    self.failureWithError(error: error)
                    if let data = response.data, let utf8Text = String.init(data: data, encoding: String.Encoding.utf8) {
                        print("Data: \(utf8Text)")
                        let json = JSON(response.data as Any)
                        if let errorDictionary = json["error"].dictionary{
                            if let firstItem = errorDictionary.first {
                                if let msg = firstItem.value.array{
                                    self.errorMsg = msg.first?.string
                                }
                            }
                        }
                        else {
                            if let msg = json["message"].string{
                                self.errorMsg = msg
                            }
                        }
                    }
                    

                }
        }
    }
    
    func PATCH(){
        if UserInfo.currentUser()?.token != nil{
            let token : String = (UserInfo.currentUser()?.token)!
            header = [String : String]()
            header?.updateValue("Bearer \(token)", forKey: "Authorization")
            //            header?.updateValue("application/json", forKey: "Accept")
            header?.updateValue("application/json", forKey: "Content-Type")
        }else{
        }
        Alamofire.request(url!, method: .patch, parameters: parameters,  encoding: JSONEncoding.default,headers: header)
            .validate()
            .responseJSON {response in
                switch response.result{
                case .success:
                    if let val = response.result.value {
                        let json = JSON(val)
                        self.successWithResponse(response: json)
                    }
                case .failure(let error):
                    if let data = response.data, let utf8Text = String.init(data: data, encoding: String.Encoding.utf8) {
                        print("Data: \(utf8Text)")
                        let json = JSON(response.data as Any)
                        if let errorDictionary = json["error"].dictionary{
                            if let firstItem = errorDictionary.first {
                                if let msg = firstItem.value.array{
                                    self.errorMsg = msg.first?.string
                                }
                            }
                        }
                        else {
                            if let msg = json["message"].string{
                                self.errorMsg = msg
                            }
                        }
                    }
                    
                    self.failureWithError(error: error)
                    
                }
        }
    }
    
    func DELETE(){
        if UserInfo.currentUser()?.token != nil{
            
        }else{
            header = nil
        }
        Alamofire.request(url!, method: .delete, headers:header)
            .validate()
            .responseJSON {response in
                switch response.result{
                case .success:
                    if let val = response.result.value {
                        let json = JSON(val)
                        self.successWithResponse(response: json)
                    }
                case .failure(let error):
                    self.failureWithError(error: error)
                }
        }
    }
    
    func upload(data: [MultipartData]) {
        if UserInfo.currentUser()?.token != nil{
            let token : String = (UserInfo.currentUser()?.token)!
            let hearDict = ["Authorization":"Bearer \(token)"]
//            header = hearDict
            header = [String : String]()
            header?.updateValue("Bearer \(token)", forKey: "Authorization")
//            header?.updateValue("application/json", forKey: "Accept")
            header?.updateValue("multipart/form-data", forKey: "Content-type")

        }else{
            header = nil
        }
        let urlRequest = try! URLRequest(url: url!, method: .post, headers: header)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in self.parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if self.isMultipartArray == true {
                for imageData in data {
                    multipartFormData.append(imageData.data as Data, withName: "gallery[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                }
            }else{
                for data in self.uploadData! {
                    multipartFormData.append(data.data as Data, withName: data.name, fileName: data.fileName, mimeType: data.mimeType)
                }
                
            }
//            for imageData in data {
//                multipartFormData.append(imageData.data as Data, withName: "image", fileName: "image.png", mimeType: "image/png")
//            }
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: header) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    debugPrint(response)
                    let json = JSON(response.result.value as Any)
                    self.successWithResponse(response: json)

                    if let err = response.error{
//                        onError?(err)
                        return
                    }
//                    onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
//                onError?(error)
            }
        }
    }
        
        /*
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
           for imageData in data {
                multipartFormData.append(imageData.data as Data, withName: "gallery[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
            for (key, value) in self.parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: "dasd",
           
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                }
            case .failure(let error):
                print(error)
            }
            
        })
        */
        
        /*
        Alamofire.upload(
            multipartFormData: { multipartFormData in
//         for (key, value) in self.parameters {
//            multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
//         }
                for (key,value) in self.parameters {
                    if let valueString = value as? String {
                        multipartFormData.append(valueString.data(using: String.Encoding.utf8)!, withName: key)

                    }
                }
                /*
                if self.isMultipartArray == true {
                    for imageData in data {
                        multipartFormData.append(imageData.data as Data, withName: "gallery[]", fileName: "img.jpeg", mimeType: "image/jpeg")
                    }
                }else{
                    for data in self.uploadData! {
                        multipartFormData.append(data.data as Data, withName: data.name, fileName: data.fileName, mimeType: data.mimeType)
                    }

                }
                */
 
        },
            with: urlRequest,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        let json = JSON(response.result.value as Any)
                        self.successWithResponse(response: json)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    self.failureWithError(error: encodingError)
                }
                
        }
        )
 */
//    }
    
    
   
    
    
}
