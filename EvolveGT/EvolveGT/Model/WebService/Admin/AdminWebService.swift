//
//  AdminWebService.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 18/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class AdminWebService: BaseWebService {

    func saveSignature() {
        self.url = "https://evolvegt.com/ev-angular-api/public/sign/saveSign"
        POST()
    }
    
    func getSignature() {
        self.url = "https://evolvegt.com/ev-angular-api/public/sign/getSignature"
        POST()
    }
}
