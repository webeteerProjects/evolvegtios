//
//  AdminServiceManager.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 18/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

enum AdminApiType {
    case saveSignature
    case getSignature
}

class AdminServiceManager: BaseWebServiceManager {

    var serviceType: AdminApiType = .saveSignature
    func saveSignatureForEvent(sign: UIImage, for signatureID: String) {
        serviceType = .saveSignature
        DispatchQueue.global().async{
            let loginService = AdminWebService()
            loginService.delegate = self
            let imageData = sign.pngData()
            let imageStr = imageData?.base64EncodedString() ?? ""
            loginService.parameters.updateValue(signatureID as AnyObject, forKey: "signature_id")
            loginService.parameters.updateValue(imageStr as AnyObject, forKey: "signature")
            loginService.saveSignature()
        }
    }
    
    func getSignatureForAdmin(with signatureID: String) {
        serviceType = .getSignature
        DispatchQueue.global().async {
            let adminService = AdminWebService()
            adminService.delegate = self
            print(signatureID)
            adminService.parameters.updateValue(signatureID as AnyObject, forKey: "signature_id")
            adminService.getSignature()
        }
    }
}

extension AdminServiceManager {
    func handleSignatureSaveResponse(response: RestResponse) {
        let responseData = response.response!
        DispatchQueue.global().async {
            let eventsData = UserSignature(fromJson: responseData)
            response.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleGetSignatureResponse(response: RestResponse) {
        let responseData = response.response!
        DispatchQueue.global().async {
            let eventsData = UserSignature(fromJson: responseData)
            response.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
}
extension AdminServiceManager: BaseServiceDelegates {
    
    func didSuccessfullyReceiveData(response:RestResponse?) {
        let responseData = response!.response!
        guard let status = responseData["status"].int, status == 1 else {
            var errorMsg = ""
            if let msg = responseData["msg"].string {
                errorMsg = msg
                response?.message = msg
            }
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: errorMsg))
                return
            }
            return
        }
        switch(serviceType) {
        case .saveSignature: handleSignatureSaveResponse(response: response!)
        case .getSignature: handleGetSignatureResponse(response: response!)
        }
    }
    
    func didFailedToReceiveData(response:RestResponse?){
        self.managerDelegate?.didFinishTask(from: self,
                                            response: (data: response,
                                                       error: Constant.Alert.KPromptServerConectError))
    }
}
