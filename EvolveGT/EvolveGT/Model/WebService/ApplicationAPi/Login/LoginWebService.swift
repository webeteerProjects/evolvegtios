//
//  LoginWebService.swift
//  EalanDirect
//
//  Created by Ajeesh T S on 12/09/18.
//  Copyright © 2018 iGrant.com. All rights reserved.
//

import UIKit

class LoginWebService: BaseWebService {
    
    func signupService(){
        self.url = baseUrl + "register"
        POST()
    }
    
    func loginService(){
        self.url = baseUrl + "user/auth?"
        POST()
    }
    
    func updateDeviceToken(){
        self.url = domain + "ev-angular-api/public/notifications/updateDeviceToken"
        POST()
    }
    
    func updateProfile(){
        self.url = baseUrl + "user/updateProfile"
        POST()
    }
    
    func updateProfileImage(){
        self.url = baseUrl + "user/profilepic"
        POST()
    }
    
    func userDetails(){
        self.url = baseUrl + "user/AlluserData"
        POST()
    }
    
    func summaryList(){
        self.url = baseUrl + "user/dashboardIOS"
        POST()
    }
    
    func forgotPasswordService(){
        self.url = baseUrl + "user/forgotPassword"
        POST()
    }
    
    func resetPasswordService(){
        self.url = baseUrl + "user/changePassword"
        POST()
    }
    
    func countryList(){
        self.url = baseUrl + "common/countries"
        GET()
    }
    
    func stateList(){
        self.url = baseUrl + "common/states"
        POST()
    }
}

