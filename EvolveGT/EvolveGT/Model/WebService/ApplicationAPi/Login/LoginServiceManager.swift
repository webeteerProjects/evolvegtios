
//
//  LoginServiceManager.swift
//  EalanDirect
//
//  Created by Ajeesh T S on 12/09/18.
//  Copyright © 2018 iGrant.com. All rights reserved.
//

import UIKit

class SignUpData: NSObject {
    var gender: String!
    var fname: String!
    var lname : String!
    var state : String!
    var phone : String!
    var title : String!
    var email : String!
    var pasword : String!
    
}

enum LoginApiType {
    case Signup
    case Login
    case UpdateDeviceToken
    case ProfileUpdate
    case UpdateProfileImage
    case UserDetails
    case ForgotPassword
    case ResetPassword
    case Summary
    case CountryList
    case StateList
}

class LoginServiceManager: BaseWebServiceManager {
    var serviceType = LoginApiType.Signup
    var deviceTokenStr = ""
    
    func signup(userInfo:SignUpData){
        serviceType = .Signup
        DispatchQueue.global().async {
            let loginService = LoginWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(userInfo.fname as AnyObject, forKey: "first_name")
            loginService.parameters.updateValue(userInfo.lname as AnyObject, forKey: "last_name")
            loginService.parameters.updateValue("+91" as AnyObject, forKey: "phone_prefix")
            loginService.parameters.updateValue(userInfo.phone as AnyObject, forKey: "phone")
            
            loginService.parameters.updateValue(userInfo.email as AnyObject, forKey: "email")
            loginService.parameters.updateValue(userInfo.gender as AnyObject, forKey: "gender")
            loginService.parameters.updateValue("" as AnyObject, forKey: "place")
            loginService.parameters.updateValue(userInfo.pasword as AnyObject, forKey: "password")
            loginService.signupService()
        }
    }
    
    func updateProfile(userInfoJson: [String: Any]) {
        serviceType = .ProfileUpdate
        DispatchQueue.global().async {
            let loginService = LoginWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(userInfoJson as AnyObject, forKey: "data")
            loginService.parameters.updateValue(UserInfo.currentUser()!.userID as AnyObject, forKey: "user_id")
            print(loginService.parameters)
            loginService.updateProfile()
        }
    }
    
    func login(uname: String, pwd: String){
        self.serviceType = .Login
        DispatchQueue.global().async{
            let loginService = LoginWebService()
            loginService.delegate = self
            loginService.parameters =  ["email_login": uname as AnyObject, "password_login": pwd as AnyObject]
            loginService.loginService()
        }
    }
    
    func userDetails(){
        self.serviceType = .UserDetails
        DispatchQueue.global().async{
            let loginService = LoginWebService()
            loginService.delegate = self
            let userId = UserInfo.currentUser()?.userID ?? ""
            loginService.parameters =  ["user_id": userId as AnyObject]
            loginService.userDetails()
        }
    }
    
    
    func eventSummery(){
        self.serviceType = .Summary
        DispatchQueue.global().async{
            let loginService = LoginWebService()
            loginService.delegate = self
            let userId = UserInfo.currentUser()?.userID ?? ""
            loginService.parameters =  ["user_id": userId as AnyObject]
            loginService.summaryList()
        }
    }
    
    
    func getCountryList(){
        self.serviceType = .CountryList
        DispatchQueue.global().async{
            let loginService = LoginWebService()
            loginService.delegate = self
            loginService.countryList()
        }
    }
    
    func getStateList(countryCode:String){
        self.serviceType = .StateList
        DispatchQueue.global().async{
            let loginService = LoginWebService()
            loginService.delegate = self
            loginService.parameters =  ["country_code": countryCode as AnyObject]
            loginService.stateList()
        }
    }
    
    
    func facebookLogin(token: String){
        self.serviceType = .UserDetails
        DispatchQueue.global().async{
            let loginService = LoginWebService()
            loginService.delegate = self
            loginService.parameters =  ["login_via": "facebook" as AnyObject, "token": token as AnyObject]
            loginService.userDetails()
        }
    }
    
    
    func updateDeviceToken(deviceToken:String){
        deviceTokenStr = deviceToken
        self.serviceType = .UpdateDeviceToken
        DispatchQueue.global().async{
            let loginService = LoginWebService()
            loginService.delegate = self
            let userId = UserInfo.currentUser()?.userID ?? ""
            loginService.parameters.updateValue("ios" as AnyObject, forKey: "device_type")
            loginService.parameters.updateValue(deviceToken as AnyObject, forKey: "token")
            loginService.parameters.updateValue(userId as AnyObject, forKey: "user_id")
            loginService.updateDeviceToken()
        }
        
    }
    
    func updateProfileImage(imageData: NSData) {
        self.serviceType = .UpdateProfileImage
        DispatchQueue.global().async {
            let loginService = LoginWebService()
            loginService.delegate = self
            loginService.parameters.updateValue((UserInfo.currentUser()?.userID)! as AnyObject, forKey: "user_id")
            let multipartData = MultipartData(data: imageData,
                                              name: "myFile",
                                              fileName: "image.jpeg",
                                              mimeType: "image/jpeg")
            loginService.uploadData = [multipartData]
            loginService.updateProfileImage()
        }
    }
        
    func forgotPassword(email: String){
        self.serviceType = .ForgotPassword
        DispatchQueue.global().async{
            let loginService = LoginWebService()
            loginService.delegate = self
            loginService.parameters =  ["user_email": email as AnyObject]
            loginService.forgotPasswordService()
        }
    }
    
    func resetPassword(oldPassword: String,newPassword: String){
        self.serviceType = .ResetPassword
        DispatchQueue.global().async{
            var dict = [String : AnyObject]()
            dict.updateValue(newPassword  as AnyObject, forKey: "password")
            dict.updateValue(newPassword  as AnyObject, forKey: "confirm-password")
            dict.updateValue(oldPassword  as AnyObject, forKey: "currentpassword")
            let loginService = LoginWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "user_id")
            loginService.parameters.updateValue(dict as AnyObject, forKey: "data")
//            loginService.parameters =  ["user_email": oldPassword as AnyObject]
            loginService.resetPasswordService()
        }
    }
    
    
    //    forgotPasswordService
}


extension LoginServiceManager : BaseServiceDelegates {
    func didSuccessfullyReceiveData(response:RestResponse?){
        let responseData = response!.response!
        if let status = responseData["status"].int{
            if status != 1{
                var errorMSg = ""
                if let msg = responseData["message"].string{
                    errorMSg = msg
                }
                if let msg = responseData["msg"].string{
                    errorMSg = msg
                }
                DispatchQueue.main.async {
                    self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: errorMSg))
                    return
                }
            }
            else{
                switch(serviceType) {
                case .Signup:handleSignupResponse(response: response)
                case .Login: handleSignupResponse(response: response)
                case .UpdateDeviceToken :self.handleDeviceTokenUpdateResponse(response: response)
                case .ProfileUpdate : handleProfileUpdateResponse(response: response)
                case .UpdateProfileImage:handleProfileImageResponse(response: response)
                case .UserDetails : handleUSerDetailsResponse(response: response)
                case .ForgotPassword : handleForgotPasswordResponse(response: response)
                case .Summary :handleSummaryDetailsResponse(response: response)
                case .ResetPassword : handleForgotPasswordResponse(response: response)
                case .CountryList : handleCountryListResponse(response: response)
                case .StateList : handleStateListResponse(response: response)

                }
            }
        }
    }
    
    func didFailedToReceiveData(response:RestResponse?){
        self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: Constant.Alert.KPromptServerConectError))
    }
    
}

extension LoginServiceManager {
    func handleSignupResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            UserInfo.createSessionWith(json: responseData)
            //   UserInfo.currentUser()?.imageUrl = nil
            UserInfo.currentUser()?.save()
            //            AppSharedData.sharedInstance.isRefreshSideMenu = true
            //            let userData = SignupResult(fromJson: responseData)
            //            response?.responseModel = userData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleUSerDetailsResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let useData = UserDetailsRes(fromJson: responseData)
            AppSharedData.sharedInstance.isRefreshSideMenu = true
            AppSharedData.sharedInstance.userDetails = useData.result
            response?.responseModel = useData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleSummaryDetailsResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let useData = EventSummary(fromJson: responseData)
            response?.responseModel = useData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleProfileUpdateResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            if let json = responseData["data"].dictionary{
                //                if let data = json["name"].string {
                //                    self.userName = data
                //                }
                //                if let data = json["email"].string {
                //                    self.userEmail = data
                //                }
                //                if let data = json["image"].string {
                //                    self.imageUrl = data
                //                }
                //                if let data = json["phone"].string {
                //                    self.phone = data
                //                }
                if let data = json["name"]?.string {
                    UserInfo.currentUser()?.userName = data
                }
                if let data = json["first_name"]?.string {
                    UserInfo.currentUser()?.fName = data
                }
                if let data = json["last_name"]?.string {
                    UserInfo.currentUser()?.lName = data
                }
                
                UserInfo.currentUser()?.save()
                AppSharedData.sharedInstance.isRefreshSideMenu = true
                
            }
            
            
            if 1 == responseData["status"] {
                response?.responseModel = true as AnyObject?
            }else{
                response?.responseModel = false as AnyObject?
            }
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleDeviceTokenUpdateResponse(response:RestResponse?){
        
//        UserDefaults.standard.set(self.deviceTokenStr, forKey:Constant.DeviceToken.KUserDeviceTokenKey)
//        UserDefaults.standard.synchronize()
        //        let responseData = response!.response!
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleProfileImageResponse(response: RestResponse?) {
        let responseData = response!.response!
        if let img = responseData["data"].string {
            AppSharedData.sharedInstance.isRefreshSideMenu = true
        }
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleForgotPasswordResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let res = ApiResponse(fromJson: responseData)
            response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleCountryListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let res = CountryListResp(fromJson: responseData)
            AppSharedData.sharedInstance.country = res.country
            response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleStateListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let res = CityListResp(fromJson: responseData)
            AppSharedData.sharedInstance.state = res.city

            response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
}

