//
//  EventServiceManager.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 09/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

enum EventApiType {
    case List
    case Details
    case PastEvents
    case UpComingEvent
    case CreditHistory
    case EventDetails
    case adminEventUsers
    case UpgradeSkill
    case AllEvents
    case CancelEvent
}

class EventServiceManager: BaseWebServiceManager {
    var serviceType = EventApiType.List
    
    func getEventList(){
        self.serviceType = .List
        DispatchQueue.global().async{
            let loginService = EventWebService()
            loginService.delegate = self
            if (UserInfo.currentUser()?.role ==  "administrator"){
                if AppSharedData.sharedInstance.isCurrentSwitchModuleUser == true{
                    loginService.eventList()
                }else{
                    loginService.adminEventList()
                }
            }
            else{
                if AppSharedData.sharedInstance.isCurrentSwitchModuleAdmin == true{
                    loginService.adminEventList()
                }else{
                    loginService.eventList()
                }
            }
        }
    }
    
    func getEventDetails(eventSlug: String){
        self.serviceType = .EventDetails
        DispatchQueue.global().async{
            let loginService = EventWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(eventSlug as AnyObject, forKey: "slug")
            loginService.parameters.updateValue("publish" as AnyObject, forKey: "post_status")
            loginService.eventDetails()
        }
    }
    
    func cancelEvent(eventId:String){
        self.serviceType = .CancelEvent
        DispatchQueue.global().async{
            let loginService = EventWebService()
            loginService.delegate = self
            let userId = UserInfo.currentUser()?.userID ?? ""
            let skill = UserInfo.currentUser()?.skillLevel ?? ""
            loginService.parameters.updateValue(eventId as AnyObject, forKey: "order_item_id")
            loginService.parameters.updateValue(userId as AnyObject, forKey: "user_id")
            loginService.parameters.updateValue(skill as AnyObject, forKey: "skill_level")
            loginService.cancelEvent()
        }
    }
    
    func upgradeSkill(userId: String,skill:String){
        self.serviceType = .UpgradeSkill
        DispatchQueue.global().async{
            let loginService = EventWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(userId as AnyObject, forKey: "user_id")
            loginService.parameters.updateValue(skill as AnyObject, forKey: "skilllevel")
            loginService.upgradeSkill()
        }
    }
    
    func getAllEventList(){
        self.serviceType = .AllEvents
        DispatchQueue.global().async{
            let loginService = EventWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "user_id")
            loginService.allEventList()
        }
    }
    
    
    func getPastEventList(){
        self.serviceType = .PastEvents
        DispatchQueue.global().async{
            let loginService = EventWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "user_id")
            loginService.pastEventList()
        }
    }
    
    
    func getUpComingEventList(){
        self.serviceType = .UpComingEvent
        DispatchQueue.global().async{
            let loginService = EventWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "user_id")
            loginService.upcomingEventList()
        }
    }
    
    
    func getCreditHistoryList(){
        self.serviceType = .CreditHistory
        DispatchQueue.global().async{
            let loginService = EventWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "user_id")
            loginService.creditHistoryList()
        }
    }
    
    func getUsersListForEventToAdmin(for eventID: Result) {
        self.serviceType = .adminEventUsers
        DispatchQueue.global().async {
            let service = EventWebService()
            service.delegate = self
            service.parameters.updateValue(eventID.eventId as AnyObject? ?? "" as AnyObject, forKey: "event_id")
            service.adminEventUsers()
        }
    }
}

extension EventServiceManager : BaseServiceDelegates {
    func didSuccessfullyReceiveData(response:RestResponse?){
        let responseData = response!.response!
        if let status = responseData["status"].int{
            if status != 1{
                var errorMSg = ""
                if let msg = responseData["msg"].string{
                    errorMSg = msg
                    response?.message = msg
                }
                DispatchQueue.main.async {
                    self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: errorMSg))
                    return
                }
            }else{
                switch(serviceType) {
                    case .List:handleEventListResponse(response: response)
                    case .Details: handleEventListResponse(response: response)
                    case .PastEvents : handlePastEventListResponse(response: response)
                    case .CreditHistory : handleCreditHistoryListResponse(response: response)
                    case .UpComingEvent : handlePastEventListResponse(response: response)
                    case .EventDetails : handleEventDetailsResponse(response: response)
                    case .adminEventUsers: handleUserListForEventResponse(response: response)
                    case .UpgradeSkill:handleUpgradeSkillResponse(response: response)
                    case .AllEvents:  handleAllEventListResponse(response: response)
                    case .CancelEvent :  handleCancelEventResponse(response: response)
                }
            }
        }
        
        
    }
    
    func didFailedToReceiveData(response:RestResponse?){
        self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: Constant.Alert.KPromptServerConectError))
    }
    
    
    
}

extension EventServiceManager {
    
    
    func handleCancelEventResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = CommonResponse(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
//            response?.requestType = self.serviceType as AnyObject
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleUpgradeSkillResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = CommonResponse(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            response?.requestType = self.serviceType as AnyObject
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleEventListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = EventListRes(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleEventDetailsResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = EventDetails(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handlePastEventListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = PastEventListeRes(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleAllEventListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = AllEventListeRes(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleCreditHistoryListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = CreditHistoryRes(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleUserListForEventResponse(response: RestResponse?) {
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = AdminEventUserList(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
}

