//
//  EventWebService.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 09/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit



class EventWebService: BaseWebService {
    
    func eventList(){
        self.url = baseUrl + "event/list"
        POST()
    }
    
    func adminEventList(){
        self.url = domain + "ev-angular-api/public/sign/signEvents"
        POST()
    }
    
    
    func eventDetails(){
        self.url = baseUrl + "event/detail"
        POST()
    }
    
    func allEventList(){
        self.url = baseUrl + "user/allEventsApp"
        POST()
    }
    
    func pastEventList(){
        self.url = baseUrl + "user/pastEventsApp"
        POST()
    }
    
    func upcomingEventList(){
        self.url = baseUrl + "user/upcomingEventsApp"
        POST()
    }
    
    func cancelEvent(){
        self.url =  baseUrl + "user/cancelOrderItem"
        POST()
    }
    
    func upgradeSkill(){
        self.url = baseUrl + "user/updateSkillLevel"
        self.url = "https://evolvegt.com/evolve-api/public/user/updateSkillLevel"
        POST()
    }
    
    func creditHistoryList(){
        self.url = baseUrl + "user/creditHistory"
        POST()
    }
    
    func adminEventUsers() {
        self.url = "https://evolvegt.com/ev-angular-api/public/sign/toSignEvents"
        POST()
    }
}

