
//
//  ShopWebServiceManager.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 17/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

//let payPalMode = "sandbox"
let payPalMode = "production"

enum ShopApiType {
    case GiftCardList
    case ArchiveCardList
    case AddArchiveCard
    case AddGiftCardList
    case CartList
    case DeleteCartItem
    case CategoryList
    case ProductList
    case ProductDetails
    case AddEventToCart
    case GetPaymentClientAccessToken
    case IntiatePayment
    case AddPromoCode
    case DonePayment
    case ClearCart
    case CheckPrivateEvent
    case OrderDetails
    case MembershipList
    case MembershipDetails
    case UserMembershipDetails
    case AddMembership
    
}

class ShopWebServiceManager: BaseWebServiceManager {
    var serviceType = ShopApiType.GiftCardList
    
    func addMembership(title:String,membership:String,price:String,image:String){
        self.serviceType = .AddMembership
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
               loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "serial")
            loginService.parameters.updateValue(title as AnyObject, forKey: "title")
            loginService.parameters.updateValue(membership as AnyObject, forKey: "membership")
            loginService.parameters.updateValue(price as AnyObject, forKey: "price")
            loginService.parameters.updateValue(image as AnyObject, forKey: "image")
            loginService.addMembershipToCart()
        }
    }
    
    
    func getUserMembershipDetails(){
             self.serviceType = .UserMembershipDetails
             DispatchQueue.global().async{
                 let loginService = ShopWebService()
                 loginService.delegate = self
                    loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "user_id")
                 loginService.userMembershipDetails()
             }
       }
    
    func getMembershipList(){
          self.serviceType = .MembershipList
          DispatchQueue.global().async{
              let loginService = ShopWebService()
              loginService.delegate = self
              loginService.membershipList()
          }
    }
    
    func getMembershipListDetails(slug:String){
           self.serviceType = .MembershipDetails
           DispatchQueue.global().async{
               let loginService = ShopWebService()
               loginService.delegate = self
               loginService.parameters.updateValue(slug as AnyObject, forKey: "slug")
               loginService.membershipDetails()
           }
       }
    
    func orderDetails(orderId:String){
        self.serviceType = .OrderDetails
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "serial")
            loginService.parameters.updateValue(orderId as AnyObject, forKey: "track_id")
            loginService.orderDetails()
        }
    }
    
    func clearCart(){
        self.serviceType = .ClearCart
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "serial")
            loginService.clearCartItems()
        }
    }
    
    func checkPrivateEvent(){
        self.serviceType = .CheckPrivateEvent
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "serial")
            loginService.checkPrivateEvent()
        }
    }
    
    func donePayment(info:[String : AnyObject]){
        self.serviceType = .DonePayment
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters = info
            loginService.finalTransctionProcess()
        }
    }
    
    func addPromo(promo: String){
        self.serviceType = .AddPromoCode
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "uid")
            loginService.parameters.updateValue(promo as AnyObject, forKey: "coupon")
            loginService.addPromoCode()
        }
    }
    
    func getPaymentAccessToken(){
        self.serviceType = .GetPaymentClientAccessToken
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            //Change to configure api..
            loginService.parameters.updateValue(payPalMode as AnyObject, forKey: "mode")
            let userID = UserInfo.currentUser()?.userID ?? ""
            loginService.parameters.updateValue(userID as AnyObject, forKey: "userId")
            let userEmail = UserInfo.currentUser()?.userEmail ?? ""
            loginService.parameters.updateValue(userEmail as AnyObject, forKey: "email")
            loginService.getPaymentAccesToken()
        }
    }
    
    
    func initiatePayment(amount: String, nonse:String , couponCode: String?,  payemntModes: String){
        self.serviceType = .IntiatePayment
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(payPalMode as AnyObject, forKey: "mode")
            loginService.parameters.updateValue(amount as AnyObject, forKey: "amount")
            loginService.parameters.updateValue(nonse as AnyObject, forKey: "nonce")
            loginService.parameters.updateValue(payemntModes as AnyObject, forKey: "payment")
            if couponCode != nil{
                loginService.parameters.updateValue(couponCode as AnyObject? ?? "" as AnyObject, forKey: "coupon")
            }
            let userID = UserInfo.currentUser()?.userID ?? ""
            loginService.parameters.updateValue(userID as AnyObject, forKey: "serial")
            loginService.makeTranscation()
        }
    }
    
    
    func getArchieCards(){
        self.serviceType = .ArchiveCardList
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.getArchieCards()
        }
    }
    
    func getGiftCards(){
        self.serviceType = .GiftCardList
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.getGiftCards()
        }
    }
    
    func getProCategoryList(){
        self.serviceType = .CategoryList
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.getCategoryList()
        }
    }
    
    func getProductList(categoryId: String, categoryName:String){
        self.serviceType = .ProductList
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters.updateValue("publish" as AnyObject, forKey: "post_status")
            loginService.parameters.updateValue(categoryName as AnyObject, forKey: "category")
            loginService.parameters.updateValue(0 as AnyObject, forKey: "status")
            loginService.parameters.updateValue("product" as AnyObject, forKey: "method")
            loginService.parameters.updateValue(categoryId as AnyObject, forKey: "category_id_list")
            loginService.getProductList()
        }
    }
    
    
    func getProductDetails(slug:String){
        self.serviceType = .ProductDetails
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters.updateValue(slug as AnyObject, forKey: "slug")
            loginService.getProductDetails()
        }
    }
    
    func addGiftCardToCart(info:[String : AnyObject]){
        self.serviceType = .AddGiftCardList
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters = info
            loginService.addGiftCardToCart()
        }
    }
    
    func addArchievCardToCart(info:[String : AnyObject]){
        self.serviceType = .AddArchiveCard
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters = info
            loginService.addArchievCardToCart()
        }
    }
    
    func addEventToCart(info:[String : AnyObject]){
        self.serviceType = .AddEventToCart
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters = info
            loginService.addEventToCart()
        }
    }
    
    func getCardItems(){
        self.serviceType = .CartList
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
             loginService.parameters.updateValue(UserInfo.currentUser()?.userID as AnyObject? ?? "" as AnyObject, forKey: "serial")
            loginService.getCartList()
        }
    }
    
    func deleteCart(info:[String : AnyObject]){
        self.serviceType = .DeleteCartItem
        DispatchQueue.global().async{
            let loginService = ShopWebService()
            loginService.delegate = self
            loginService.parameters = info
            loginService.deleteItemFromCartList()
        }
    }

    
}

extension ShopWebServiceManager : BaseServiceDelegates {
    func didSuccessfullyReceiveData(response:RestResponse?){
        let responseData = response!.response!
        if let status = responseData["status"].int{
            if status != 1{
                var errorMSg = ""
                if let msg = responseData["message"].string{
                    errorMSg = msg
                }
                if let msg = responseData["msg"].string{
                    errorMSg = msg
                }else{
                    if !errorMSg.isValidString{
                        errorMSg = Constant.Alert.KPromptSomeThingWentWrong
                    }
                }
                DispatchQueue.main.async {
                    self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: errorMSg))
                    return
                }
            }else{
                switch(serviceType) {
                    case .GiftCardList:handleCardListResponse(response: response)
                    case .ArchiveCardList: handleCardListResponse(response: response)
                    case .AddArchiveCard:cardTocartResponse(response: response)
                    case .AddGiftCardList: cardTocartResponse(response: response)
                    case .CartList: handleCartListResponse(response: response)
                    case .DeleteCartItem: cardTocartResponse(response: response)
                    case .ProductList: handleProductListResponse(response: response)
                    case .ProductDetails: handleProductDetailsResponse(response: response)
                    case .CategoryList: handleCategoryListResponse(response: response)
                    case .AddEventToCart:cardTocartResponse(response: response)
                    case .GetPaymentClientAccessToken:handlePaymentResponse(response: response)
                    case .IntiatePayment :handlePaymentTransactionResponse(response: response)
                    case .AddPromoCode :handleAddPromoResponse(response: response)
                    case .DonePayment :handleFinalPaymentTransactionResponse(response: response)
                    case .ClearCart :handleClearCartResponse(response: response)
                    case .CheckPrivateEvent: handleCheckPrivateEventResponse(response: response)
                    case .OrderDetails: handleOrderDetails(response: response)
                    case .MembershipList: handleMembershipListResponse(response: response)
                    case .MembershipDetails: handleMembershipDetailsResponse(response: response)
                    case .UserMembershipDetails: handleUserMembershipDetailsResponse(response: response)
                    case .AddMembership: handleAddMembershipDetailsResponse(response: response)


                }
            }
        }
        
        
    }
    
    func didFailedToReceiveData(response:RestResponse?){
        self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: Constant.Alert.KPromptServerConectError))
    }
    
    
}


extension ShopWebServiceManager {
    
    
    func handleAddMembershipDetailsResponse(response:RestResponse?){
       let responseData = response!.response!
       DispatchQueue.global().async {
          let res = AddMembership(fromJson: responseData)
          response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleUserMembershipDetailsResponse(response:RestResponse?){
           let responseData = response!.response!
           DispatchQueue.global().async {
              let res = UserMembership(fromJson: responseData)
              response?.responseModel = res as AnyObject?
                DispatchQueue.main.async {
                    self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
                }
            }
        }
    
    func handleMembershipListResponse(response:RestResponse?){
          let responseData = response!.response!
          DispatchQueue.global().async {
            let res = MembershipListRes(fromJson: responseData)
            response?.responseModel = res as AnyObject?
              DispatchQueue.main.async {
                  self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
              }
          }
      }
    
    func handleMembershipDetailsResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
           let res = MembershipDetails(fromJson: responseData)
           response?.responseModel = res as AnyObject?
             DispatchQueue.main.async {
                 self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
             }
         }
     }
    
    
    
    func handleOrderDetails(response:RestResponse?){
          let responseData = response!.response!
          DispatchQueue.global().async {
            let res = OrderDetails(fromJson: responseData)
            response?.responseModel = res as AnyObject?
              DispatchQueue.main.async {
                  self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
              }
          }
      }
    
    
    func handleCheckPrivateEventResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            if let statusVal = responseData["hasPrivateEvent"].bool{
                response?.responseModel = statusVal as AnyObject?
            }
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    
    func handleClearCartResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
//            let res = PaymentDone(fromJson: responseData)
//            response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleFinalPaymentTransactionResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let res = PaymentDone(fromJson: responseData)
            response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    
    func handlePaymentTransactionResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let res = BrainTreePlaceOrderRes(fromJson: responseData)
            response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    
    
    func handleAddPromoResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let res = PromoResponse(fromJson: responseData)
            response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handlePaymentResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let res = PaymentClientAccessToken(fromJson: responseData)
            response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func cardTocartResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let res = ApiResponse(fromJson: responseData)
            response?.responseModel = res as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleProductDetailsResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = ProductDetails(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    
    func handleProductListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = ProductListRes(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleCategoryListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = CategoryListRes(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleCardListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = CardListRes(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            DispatchQueue.main.async {
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }
    
    func handleCartListResponse(response:RestResponse?){
        let responseData = response!.response!
        DispatchQueue.global().async {
            let eventsData = CartResponse(fromJson: responseData)
            response?.responseModel = eventsData as AnyObject?
            AppSharedData.sharedInstance.cartCount = eventsData.data.count
            DispatchQueue.main.async {
//                let tabbarCntlr = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "TabView") as? UITabBarController
//                if let tabItems = tabbarCntlr?.tabBar.items as NSArray!
//                {
//                    // In this case we want to modify the badge number of the third tab:
//                    let tabItem = tabItems[3] as! UITabBarItem
//                    if AppSharedData.sharedInstance.cartCount > 0{
//                        tabItem.badgeValue = String(AppSharedData.sharedInstance.cartCount)
//                    }else{
//                        tabItem.badgeValue = ""
//                    }
//                }
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showBadgeCount()
                self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
            }
        }
    }


}

