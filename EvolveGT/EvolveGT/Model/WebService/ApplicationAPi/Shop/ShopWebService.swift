
//
//  ShopeWebService.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 17/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit

class ShopWebService: BaseWebService {

    
    func checkPrivateEvent(){
        self.url = baseUrl + "product/isAnyPrivateEvents"
        POST()
    }
        
    func clearCartItems(){
        self.url = baseUrl + "product/resetCart"
        POST()
    }
    
    func finalTransctionProcess(){
        self.url = baseUrl + "product/placeOrder"
        POST()
    }
    
    func makeTranscation(){
        self.url = baseUrl + "braintree/transaction"
        POST()
    }
    
    func addPromoCode(){
        self.url = baseUrl + "product/validateCoupon"
        POST()
    }
    
    func getPaymentAccesToken(){
        self.url = baseUrl + "braintree/token"
        POST()
    }
    
    
    func paymentTransaction(){
        self.url = baseUrl + "braintree/token"
        POST()
    }
    
    
    
    func getCategoryList(){
        self.url = baseUrl + "mobile/getCategory"
        POST()
    }
    
    func getProductList(){
        self.url = baseUrl + "category_product/list"
        POST()
    }
    
    func getProductDetails(){
        self.url = baseUrl + "mobile/getSingleProduct"
        POST()
    }
    
    func getArchieCards(){
        self.url = baseUrl + "cards/getAllArchieCardsApp"
        GET()
    }
    
    func getGiftCards(){
        self.url = baseUrl + "cards/getAllGiftCardsApp"
        GET()
    }
    
    func addArchievCardToCart(){
        self.url = baseUrl + "product/archieCart"
        POST()
    }
    
    func addGiftCardToCart(){
        self.url = baseUrl + "product/giftcardCart"
        POST()
    }
    
    func addEventToCart(){
        self.url = baseUrl + "product/eventCart"
        POST()
    }
    
    func getCartList(){
        self.url = baseUrl + "mobile/beforeOrder"
        POST()
    }
    
    func deleteItemFromCartList(){
        self.url = baseUrl + "header/removeCartItem"
        POST()
    }
    
    func orderDetails(){
        self.url = baseUrl + "product/reviewOrder"
        POST()
    }
    
    func membershipList(){
       self.url = baseUrl + "mobile/memberships/fetch"
       GET()
    }
       
    func membershipDetails(){
       self.url = baseUrl + "mobile/memberships/single"
       POST()
    }
    
    func userMembershipDetails(){
         self.url = baseUrl + "mobile/user/membership"
         POST()
      }
    
    func addMembershipToCart(){
       self.url = baseUrl + "product/membershipCart"
       POST()
    }

    
//    product/membershipCart
    
}
