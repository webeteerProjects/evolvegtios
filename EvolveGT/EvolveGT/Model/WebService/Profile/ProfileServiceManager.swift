//
//  ProfileServiceManager.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 29/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import Foundation

enum ProfileUpdateAPIType {
    case ProfilePic
    case ProfileData
    case NotificationTypes
    case NotificationUpdate
}

class ProfileServiceManager: BaseWebServiceManager {
    var serviceType: ProfileUpdateAPIType = .ProfilePic
    
    func updateProfilePic(with imageData: NSData) {
        self.serviceType = .ProfilePic
        DispatchQueue.global().async {
            let profileService = ProfileWebService()
            profileService.uploadData = [MultipartData(data: imageData,
                                                      name: "image",
                                                      fileName: "image",
                                                      mimeType: "png")]
            profileService.delegate = self
            let userId = UserInfo.currentUser()?.userID ?? ""
            profileService.parameters =  ["user_id": userId as AnyObject]
        }
    }
    
    func getNotificationTypes() {
        serviceType = .NotificationTypes
        let profileService = ProfileWebService()
        profileService.delegate = self
        let userId = UserInfo.currentUser()?.userID ?? ""
        profileService.parameters = ["user_id": userId as AnyObject]
        profileService.notificationTypeService()
    }
    
    func updateNotificationPreference(with preference: NotificationPreference) {
        serviceType = .NotificationUpdate
        let profileService = ProfileWebService()
        profileService.delegate = self
        profileService.parameters = try! preference.asDictionary() as [String : AnyObject]
        profileService.notificationPreferenceChange()
    }
}

extension ProfileServiceManager: BaseServiceDelegates {
    func didSuccessfullyReceiveData(response:RestResponse?) {
        let responseData = response!.response!
        if let status = responseData["status"].int {
            if status != 1 {
                var errorMSg = ""
                if let msg = responseData["message"].string{
                    errorMSg = msg
                }
                DispatchQueue.main.async {
                    self.managerDelegate?.didFinishTask(from: self, response: (data: response, error: errorMSg))
                    return
                }
            }
        }
        switch serviceType {
        case .ProfilePic: return
        case .ProfileData: return
        case .NotificationTypes: handleNotificationTypeResponse(response: response)
        case .NotificationUpdate: handleNotificationUpdateResponse(response: response)
        }
    }
    
    func didFailedToReceiveData(response:RestResponse?){
        self.managerDelegate?.didFinishTask(from: self,
                                            response: (data: response,
                                                       error: Constant.Alert.KPromptServerConectError))
    }
}

extension ProfileServiceManager {
    func handleNotificationTypeResponse(response: RestResponse?) {
        managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
    }
    
    func handleNotificationUpdateResponse(response: RestResponse?) {
        managerDelegate?.didFinishTask(from: self, response: (data: response, error: nil))
    }
}

extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
