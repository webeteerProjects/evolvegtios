//
//  ProfileWebService.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 29/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import Foundation

class ProfileWebService: BaseWebService {
    
    func profilePicUpdateService() {
        self.url = baseUrl + "user/profilepic"
        POST()
    }
    
    func notificationTypeService() {
        self.url = "https://evolvegt.com/ev-angular-api/public/notifications/types"
        POST()
    }
    
    func notificationPreferenceChange() {
        self.url = "https://evolvegt.com/ev-angular-api/public/notifications/updateNotificationPreference"
        POST()
    }
}
