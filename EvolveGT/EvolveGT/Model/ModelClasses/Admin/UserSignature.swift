//
//  UserSignature.swift
//  EvolveGT
//
//  Created by Vishnu T Vijay on 20/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import UIKit
import SwiftyJSON

let ImageTag = "data:image/png;base64,"

class UserSignature {
    var msg : String?
    var status : Int?
    var signed: Int?
    var signature: String?
    
    init(fromJson json: JSON!) {
        if json.isEmpty{
            return
        }
        msg = json["msg"].stringValue
        status = json["status"].intValue
        signed = json["signed"].intValue
        signature = json["signature"].stringValue
    }
    
    var signatureAsImage: UIImage? {
        get {
            
            if let str = signature{
                var newString = str
                newString = newString.replacingOccurrences(of: ImageTag, with: "")
                newString = newString.replacingOccurrences(of: "\n", with: "")
                guard let data = Data(base64Encoded: newString) else { return nil }
                return UIImage(data: data)
//                if newString.contains(ImageTag){
//                    newString = newString.replacingOccurrences(of: ImageTag, with: "")
//                    newString = newString.replacingOccurrences(of: "\n", with: "")
//                    guard let data = Data(base64Encoded: newString) else { return nil }
//                    return UIImage(data: data)
//                }else{
//                    guard let data = Data(base64Encoded: str) else { return nil }
//                    return UIImage(data: data)
//
//                }
            }
            return nil


//            let imageData = Data.init(base64Encoded: signature!, options: .init(rawValue: 0))

//            let imageData = Data(base64Encoded: signature!, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)
//            if imageData == nil{
//                return nil
//            }
//            return UIImage(data: imageData!)
//            guard let encodedString = signature?.replacingOccurrences(of: "\n", with: "") else { return nil }
//            guard let base64String = encodedString.split(separator: ",").last else { return nil  }
//            let string = String(base64String)
//            guard let imageData = NSData(base64Encoded: string,
//                                         options: NSData.Base64DecodingOptions(rawValue: 0)) else { return nil }
//            let image = UIImage(data: imageData as Data)
//            return image
        }
    }
}
