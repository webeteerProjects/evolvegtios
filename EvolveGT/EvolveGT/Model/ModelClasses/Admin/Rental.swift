//
//	Rental.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Rental{

	var attribute : String!
	var name : String!
	var value : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		attribute = json["attribute"].stringValue
		name = json["name"].stringValue
		value = json["value"].stringValue
	}

}