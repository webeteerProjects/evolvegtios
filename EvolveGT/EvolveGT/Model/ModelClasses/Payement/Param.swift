//
//	Param.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Param{

	var transaction : Transaction!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let transactionJson = json["transaction"]
		if !transactionJson.isEmpty{
			transaction = Transaction(fromJson: transactionJson)
		}
	}

}