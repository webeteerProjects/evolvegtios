//
//	BrainTreePlaceOrderRes.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class BrainTreePlaceOrderRes{

	var cartStatus : Int!
	var msg : String!
	var orderId : Int!
	var paymentStatus : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		cartStatus = json["cart_status"].intValue
		msg = json["msg"].stringValue
		orderId = json["order_id"].intValue
		paymentStatus = json["payment_status"].stringValue
		status = json["status"].intValue
	}

}
