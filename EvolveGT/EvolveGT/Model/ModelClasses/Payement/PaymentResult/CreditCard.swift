//
//	CreditCard.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CreditCard{

	var accountType : String!
	var bin : String!
	var cardType : String!
	var cardholderName : String!
	var commercial : String!
	var countryOfIssuance : String!
	var customerLocation : String!
	var debit : String!
	var durbinRegulated : String!
	var expirationMonth : String!
	var expirationYear : String!
	var globalId : String!
	var healthcare : String!
	var imageUrl : String!
	var issuingBank : String!
	var last4 : String!
	var payroll : String!
	var prepaid : String!
	var productId : String!
	var token : String!
	var uniqueNumberIdentifier : String!
	var venmoSdk : Bool!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		accountType = json["accountType"].stringValue
		bin = json["bin"].stringValue
		cardType = json["cardType"].stringValue
		cardholderName = json["cardholderName"].stringValue
		commercial = json["commercial"].stringValue
		countryOfIssuance = json["countryOfIssuance"].stringValue
		customerLocation = json["customerLocation"].stringValue
		debit = json["debit"].stringValue
		durbinRegulated = json["durbinRegulated"].stringValue
		expirationMonth = json["expirationMonth"].stringValue
		expirationYear = json["expirationYear"].stringValue
		globalId = json["globalId"].stringValue
		healthcare = json["healthcare"].stringValue
		imageUrl = json["imageUrl"].stringValue
		issuingBank = json["issuingBank"].stringValue
		last4 = json["last4"].stringValue
		payroll = json["payroll"].stringValue
		prepaid = json["prepaid"].stringValue
		productId = json["productId"].stringValue
		token = json["token"].stringValue
		uniqueNumberIdentifier = json["uniqueNumberIdentifier"].stringValue
		venmoSdk = json["venmoSdk"].boolValue
	}

}
