//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class OrderData{

	var attr : String!
	var attributes : String!
	var feeAmount : String!
	var price : String!
	var productName : String!
	var quantity : String!
	var slug : String!
	var subtotal : String!
	var total : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		attr = json["attr"].stringValue
		attributes = json["attributes"].stringValue
		feeAmount = json["fee_amount"].stringValue
		price = json["price"].stringValue
		productName = json["product_name"].stringValue
		quantity = json["quantity"].stringValue
		slug = json["slug"].stringValue
		subtotal = json["subtotal"].stringValue
		total = json["total"].stringValue
	}

}
