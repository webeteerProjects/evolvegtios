//
//	Paypal.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Paypal{

	var authorizationId : String!
	var captureId : String!
	var customField : String!
	var debugId : String!
	var descriptionField : String!
	var globalId : String!
	var imageUrl : String!
	var payeeEmail : String!
	var payeeId : String!
	var payerEmail : String!
	var payerFirstName : String!
	var payerId : String!
	var payerLastName : String!
	var payerStatus : String!
	var paymentId : String!
	var refundFromTransactionFeeAmount : String!
	var refundFromTransactionFeeCurrencyIsoCode : String!
	var refundId : String!
	var sellerProtectionStatus : String!
	var token : String!
	var transactionFeeAmount : String!
	var transactionFeeCurrencyIsoCode : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		authorizationId = json["authorizationId"].stringValue
		captureId = json["captureId"].stringValue
		customField = json["customField"].stringValue
		debugId = json["debugId"].stringValue
		descriptionField = json["description"].stringValue
		globalId = json["globalId"].stringValue
		imageUrl = json["imageUrl"].stringValue
		payeeEmail = json["payeeEmail"].stringValue
		payeeId = json["payeeId"].stringValue
		payerEmail = json["payerEmail"].stringValue
		payerFirstName = json["payerFirstName"].stringValue
		payerId = json["payerId"].stringValue
		payerLastName = json["payerLastName"].stringValue
		payerStatus = json["payerStatus"].stringValue
		paymentId = json["paymentId"].stringValue
		refundFromTransactionFeeAmount = json["refundFromTransactionFeeAmount"].stringValue
		refundFromTransactionFeeCurrencyIsoCode = json["refundFromTransactionFeeCurrencyIsoCode"].stringValue
		refundId = json["refundId"].stringValue
		sellerProtectionStatus = json["sellerProtectionStatus"].stringValue
		token = json["token"].stringValue
		transactionFeeAmount = json["transactionFeeAmount"].stringValue
		transactionFeeCurrencyIsoCode = json["transactionFeeCurrencyIsoCode"].stringValue
	}

}
