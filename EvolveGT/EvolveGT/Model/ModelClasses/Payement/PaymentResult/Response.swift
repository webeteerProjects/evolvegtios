//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Response{

	var success : Bool!
	var transaction : Transaction!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		success = json["success"].boolValue
		let transactionJson = json["transaction"]
		if !transactionJson.isEmpty{
			transaction = Transaction(fromJson: transactionJson)
		}
	}

}