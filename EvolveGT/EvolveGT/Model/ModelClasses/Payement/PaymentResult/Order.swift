//
//	Order.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Order{

	var address1 : String!
	var address2 : String!
	var city : String!
	var company : String!
	var country : String!
	var email : String!
	var firstName : String!
	var lastName : String!
	var orderDate : String!
	var paymentMethod : String!
	var phone : String!
	var postcode : String!
	var state : String!
	var total : String?


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		address1 = json["address_1"].stringValue
		address2 = json["address_2"].stringValue
		city = json["city"].stringValue
		company = json["company"].stringValue
		country = json["country"].stringValue
		email = json["email"].stringValue
		firstName = json["first_name"].stringValue
		lastName = json["last_name"].stringValue
		orderDate = json["order_date"].stringValue
		paymentMethod = json["payment_method"].stringValue
		phone = json["phone"].stringValue
		postcode = json["postcode"].stringValue
		state = json["state"].stringValue
		total = json["total"].stringValue
	}

}
