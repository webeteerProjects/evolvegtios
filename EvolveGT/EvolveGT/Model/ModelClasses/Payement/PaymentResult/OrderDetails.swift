//
//	OrderDetails.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class OrderDetails{

	var data : [OrderData]!
	var msg : String!
	var netTotal : Int!
	var order : [Order]!
	var paymentLog : [PaymentLog]!
	var status : Int!
	var subTotal : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		data = [OrderData]()
		let dataArray = json["data"].arrayValue
		for dataJson in dataArray{
			let value = OrderData(fromJson: dataJson)
			data.append(value)
		}
		msg = json["msg"].stringValue
		netTotal = json["net_total"].intValue
		order = [Order]()
		let orderArray = json["order"].arrayValue
		for orderJson in orderArray{
			let value = Order(fromJson: orderJson)
			order.append(value)
		}
		paymentLog = [PaymentLog]()
		let paymentLogArray = json["payment_log"].arrayValue
		for paymentLogJson in paymentLogArray{
			let value = PaymentLog(fromJson: paymentLogJson)
			paymentLog.append(value)
		}
		status = json["status"].intValue
		subTotal = json["sub_total"].intValue
	}

}
