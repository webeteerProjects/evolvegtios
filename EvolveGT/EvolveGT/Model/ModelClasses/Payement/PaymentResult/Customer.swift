//
//	Customer.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Customer{

	var company : String!
	var email : String!
	var fax : String!
	var firstName : String!
	var globalId : String!
	var id : String!
	var lastName : String!
	var phone : String!
	var website : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		company = json["company"].stringValue
		email = json["email"].stringValue
		fax = json["fax"].stringValue
		firstName = json["firstName"].stringValue
		globalId = json["globalId"].stringValue
		id = json["id"].stringValue
		lastName = json["lastName"].stringValue
		phone = json["phone"].stringValue
		website = json["website"].stringValue
	}

}
