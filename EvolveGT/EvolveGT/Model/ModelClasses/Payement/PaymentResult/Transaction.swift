//
//	Transaction.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Transaction{

//    var addOns : [String]!
//    var additionalProcessorResponse : AnyObject!
	var amount : String!
//    var authorizationAdjustments : [String]!
//    var authorizationExpiresAt : AuthorizationExpiresAt!
//    var authorizedTransactionGlobalId : String!
//    var authorizedTransactionId : AnyObject!
//    var avsErrorResponseCode : AnyObject!
//    var avsPostalCodeResponseCode : String!
//    var avsStreetAddressResponseCode : String!
//    var billing : Billing!
//    var billingDetails : BillingDetail!
//    var channel : AnyObject!
    var createdAt : AuthorizationExpiresAt!
    var creditCard : CreditCard!
//    var creditCardDetails : BillingDetail!
//    var currencyIsoCode : String!
//    var customFields : AnyObject!
    var customer : Customer!
//    var customerDetails : BillingDetail!
//    var cvvResponseCode : String!
//    var descriptor : BillingDetail!
//    var disbursementDetails : BillingDetail!
//    var discountAmount : AnyObject!
//    var discounts : [AnyObject]!
//    var disputes : [AnyObject]!
//    var escrowStatus : AnyObject!
//    var gatewayRejectionReason : AnyObject!
	var globalId : String!
	var id : String!
//    var masterMerchantAccountId : AnyObject!
//    var merchantAccountId : String!
//    var networkResponseCode : AnyObject!
//    var networkResponseText : AnyObject!
//    var networkTransactionId : AnyObject!
	var orderId : String!
//    var partialSettlementTransactionGlobalIds : [AnyObject]!
//    var partialSettlementTransactionIds : [AnyObject]!
//    var paymentInstrumentType : String!
	var paypal : Paypal!
//    var paypalDetails : BillingDetail!
//    var planId : AnyObject!
//    var processorAuthorizationCode : AnyObject!
//    var processorResponseCode : String!
//    var processorResponseText : String!
//    var processorResponseType : String!
//    var processorSettlementResponseCode : String!
//    var processorSettlementResponseText : String!
//    var purchaseOrderNumber : AnyObject!
//    var recurring : Bool!
//    var refundGlobalIds : [AnyObject]!
//    var refundId : AnyObject!
//    var refundIds : [AnyObject]!
//    var refundedTransactionGlobalId : AnyObject!
//    var refundedTransactionId : AnyObject!
//    var serviceFeeAmount : AnyObject!
//    var settlementBatchId : String!
//    var shipping : Billing!
//    var shippingAmount : AnyObject!
//    var shippingDetails : BillingDetail!
//    var shipsFromPostalCode : AnyObject!
//    var status : String!
//    var statusHistory : [BillingDetail]!
//    var subMerchantAccountId : AnyObject!
//    var subscription : Subscription!
//    var subscriptionDetails : BillingDetail!
//    var subscriptionId : AnyObject!
//    var taxAmount : AnyObject!
//    var taxExempt : Bool!
//    var threeDSecureInfo : AnyObject!
//    var type : String!
//    var updatedAt : AuthorizationExpiresAt!
//    var voiceReferralNumber : AnyObject!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        
        let paypalJson = json["paypal"]
        if !paypalJson.isEmpty{
            paypal = Paypal(fromJson: paypalJson)
        }
        amount = json["amount"].stringValue
        globalId = json["globalId"].stringValue
        id = json["id"].stringValue
        orderId = json["orderId"].stringValue
        let customerJson = json["customer"]
        if !customerJson.isEmpty{
            customer = Customer(fromJson: customerJson)
        }
        let createdAtJson = json["createdAt"]
        if !createdAtJson.isEmpty{
            createdAt = AuthorizationExpiresAt(fromJson: createdAtJson)
        }
        let creditCardJson = json["creditCard"]
        if !creditCardJson.isEmpty{
            creditCard = CreditCard(fromJson: creditCardJson)
        }

        /*
		addOns = [AnyObject]()
		let addOnsArray = json["addOns"].arrayValue
		for addOnsJson in addOnsArray{
			addOns.append(addOnsJson.stringValue)
		}
		additionalProcessorResponse = json["additionalProcessorResponse"].stringValue
		amount = json["amount"].stringValue
		authorizationAdjustments = [AnyObject]()
		let authorizationAdjustmentsArray = json["authorizationAdjustments"].arrayValue
		for authorizationAdjustmentsJson in authorizationAdjustmentsArray{
			authorizationAdjustments.append(authorizationAdjustmentsJson.stringValue)
		}
		let authorizationExpiresAtJson = json["authorizationExpiresAt"]
		if !authorizationExpiresAtJson.isEmpty{
			authorizationExpiresAt = AuthorizationExpiresAt(fromJson: authorizationExpiresAtJson)
		}
		authorizedTransactionGlobalId = json["authorizedTransactionGlobalId"].stringValue
		authorizedTransactionId = json["authorizedTransactionId"].stringValue
		avsErrorResponseCode = json["avsErrorResponseCode"].stringValue
		avsPostalCodeResponseCode = json["avsPostalCodeResponseCode"].stringValue
		avsStreetAddressResponseCode = json["avsStreetAddressResponseCode"].stringValue
		let billingJson = json["billing"]
		if !billingJson.isEmpty{
			billing = Billing(fromJson: billingJson)
		}
		let billingDetailsJson = json["billingDetails"]
		if !billingDetailsJson.isEmpty{
			billingDetails = BillingDetail(fromJson: billingDetailsJson)
		}
		channel = json["channel"].stringValue
        let createdAtJson = json["createdAt"]
        if !createdAtJson.isEmpty{
            createdAt = AuthorizationExpiresAt(fromJson: createdAtJson)
        }
       
		let creditCardDetailsJson = json["creditCardDetails"]
		if !creditCardDetailsJson.isEmpty{
			creditCardDetails = BillingDetail(fromJson: creditCardDetailsJson)
		}
		currencyIsoCode = json["currencyIsoCode"].stringValue
		customFields = json["customFields"].stringValue
		let customerJson = json["customer"]
		if !customerJson.isEmpty{
			customer = Customer(fromJson: customerJson)
		}
		let customerDetailsJson = json["customerDetails"]
		if !customerDetailsJson.isEmpty{
			customerDetails = BillingDetail(fromJson: customerDetailsJson)
		}
		cvvResponseCode = json["cvvResponseCode"].stringValue
		let descriptorJson = json["descriptor"]
		if !descriptorJson.isEmpty{
			descriptor = BillingDetail(fromJson: descriptorJson)
		}
		let disbursementDetailsJson = json["disbursementDetails"]
		if !disbursementDetailsJson.isEmpty{
			disbursementDetails = BillingDetail(fromJson: disbursementDetailsJson)
		}
		discountAmount = json["discountAmount"].stringValue
		discounts = [AnyObject]()
		let discountsArray = json["discounts"].arrayValue
		for discountsJson in discountsArray{
			discounts.append(discountsJson.stringValue)
		}
		disputes = [AnyObject]()
		let disputesArray = json["disputes"].arrayValue
		for disputesJson in disputesArray{
			disputes.append(disputesJson.stringValue)
		}
		escrowStatus = json["escrowStatus"].stringValue
		gatewayRejectionReason = json["gatewayRejectionReason"].stringValue
		globalId = json["globalId"].stringValue
		id = json["id"].stringValue
		masterMerchantAccountId = json["masterMerchantAccountId"].stringValue
		merchantAccountId = json["merchantAccountId"].stringValue
		networkResponseCode = json["networkResponseCode"].stringValue
		networkResponseText = json["networkResponseText"].stringValue
		networkTransactionId = json["networkTransactionId"].stringValue
		orderId = json["orderId"].stringValue
		partialSettlementTransactionGlobalIds = [AnyObject]()
		let partialSettlementTransactionGlobalIdsArray = json["partialSettlementTransactionGlobalIds"].arrayValue
		for partialSettlementTransactionGlobalIdsJson in partialSettlementTransactionGlobalIdsArray{
			partialSettlementTransactionGlobalIds.append(partialSettlementTransactionGlobalIdsJson.stringValue)
		}
		partialSettlementTransactionIds = [AnyObject]()
		let partialSettlementTransactionIdsArray = json["partialSettlementTransactionIds"].arrayValue
		for partialSettlementTransactionIdsJson in partialSettlementTransactionIdsArray{
			partialSettlementTransactionIds.append(partialSettlementTransactionIdsJson.stringValue)
		}
		paymentInstrumentType = json["paymentInstrumentType"].stringValue
		let paypalJson = json["paypal"]
		if !paypalJson.isEmpty{
			paypal = Paypal(fromJson: paypalJson)
		}
		let paypalDetailsJson = json["paypalDetails"]
		if !paypalDetailsJson.isEmpty{
			paypalDetails = BillingDetail(fromJson: paypalDetailsJson)
		}
		planId = json["planId"].stringValue
		processorAuthorizationCode = json["processorAuthorizationCode"].stringValue
		processorResponseCode = json["processorResponseCode"].stringValue
		processorResponseText = json["processorResponseText"].stringValue
		processorResponseType = json["processorResponseType"].stringValue
		processorSettlementResponseCode = json["processorSettlementResponseCode"].stringValue
		processorSettlementResponseText = json["processorSettlementResponseText"].stringValue
		purchaseOrderNumber = json["purchaseOrderNumber"].stringValue
		recurring = json["recurring"].boolValue
		refundGlobalIds = [AnyObject]()
		let refundGlobalIdsArray = json["refundGlobalIds"].arrayValue
		for refundGlobalIdsJson in refundGlobalIdsArray{
			refundGlobalIds.append(refundGlobalIdsJson.stringValue)
		}
		refundId = json["refundId"].stringValue
		refundIds = [AnyObject]()
		let refundIdsArray = json["refundIds"].arrayValue
		for refundIdsJson in refundIdsArray{
			refundIds.append(refundIdsJson.stringValue)
		}
		refundedTransactionGlobalId = json["refundedTransactionGlobalId"].stringValue
		refundedTransactionId = json["refundedTransactionId"].stringValue
		serviceFeeAmount = json["serviceFeeAmount"].stringValue
		settlementBatchId = json["settlementBatchId"].stringValue
		let shippingJson = json["shipping"]
		if !shippingJson.isEmpty{
			shipping = Billing(fromJson: shippingJson)
		}
		shippingAmount = json["shippingAmount"].stringValue
		let shippingDetailsJson = json["shippingDetails"]
		if !shippingDetailsJson.isEmpty{
			shippingDetails = BillingDetail(fromJson: shippingDetailsJson)
		}
		shipsFromPostalCode = json["shipsFromPostalCode"].stringValue
		status = json["status"].stringValue
		statusHistory = [BillingDetail]()
		let statusHistoryArray = json["statusHistory"].arrayValue
		for statusHistoryJson in statusHistoryArray{
			let value = BillingDetail(fromJson: statusHistoryJson)
			statusHistory.append(value)
		}
		subMerchantAccountId = json["subMerchantAccountId"].stringValue
		let subscriptionJson = json["subscription"]
		if !subscriptionJson.isEmpty{
			subscription = Subscription(fromJson: subscriptionJson)
		}
		let subscriptionDetailsJson = json["subscriptionDetails"]
		if !subscriptionDetailsJson.isEmpty{
			subscriptionDetails = BillingDetail(fromJson: subscriptionDetailsJson)
		}
		subscriptionId = json["subscriptionId"].stringValue
		taxAmount = json["taxAmount"].stringValue
		taxExempt = json["taxExempt"].boolValue
		threeDSecureInfo = json["threeDSecureInfo"].stringValue
		type = json["type"].stringValue
		let updatedAtJson = json["updatedAt"]
		if !updatedAtJson.isEmpty{
			updatedAt = AuthorizationExpiresAt(fromJson: updatedAtJson)
		}
		voiceReferralNumber = json["voiceReferralNumber"].stringValue
 
 */
	}

}
