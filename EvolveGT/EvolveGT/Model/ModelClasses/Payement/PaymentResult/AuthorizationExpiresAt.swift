//
//	AuthorizationExpiresAt.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class AuthorizationExpiresAt{

	var date : String!
	var timezone : String!
	var timezoneType : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		date = json["date"].stringValue
		timezone = json["timezone"].stringValue
		timezoneType = json["timezone_type"].intValue
	}

}
