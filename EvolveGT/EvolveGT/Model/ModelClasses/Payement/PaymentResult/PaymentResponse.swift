//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PaymentResponse{

	var response : Response!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let responseJson = json["response"]
		if !responseJson.isEmpty{
			response = Response(fromJson: responseJson)
		}
		status = json["status"].intValue
	}

}
