//
//	PromoResponse.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PromoResponse{

	var msg : String!
	var remain : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		msg = json["msg"].stringValue
		remain = json["remain"].stringValue
		status = json["status"].intValue
	}

}
