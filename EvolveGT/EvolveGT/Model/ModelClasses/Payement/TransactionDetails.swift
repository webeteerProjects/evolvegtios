//
//	Transaction.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class TransactionDetails{

	var amount : String!
	var id : String!
	var orderId : String!
	var paypal : PaypalResult!
	var type : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		amount = json["amount"].stringValue
		id = json["id"].stringValue
		orderId = json["orderId"].stringValue
		let paypalJson = json["PaypalResult"]
		if !paypalJson.isEmpty{
			paypal = PaypalResult(fromJson: paypalJson)
		}
		type = json["type"].stringValue
	}

}
