//
//	PaymentClientAccessToken.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PaymentClientAccessToken{

	var clientToken : String!
	var customerId : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		clientToken = json["client_token"].stringValue
		customerId = json["customer_id"].stringValue
		status = json["status"].intValue
	}

}