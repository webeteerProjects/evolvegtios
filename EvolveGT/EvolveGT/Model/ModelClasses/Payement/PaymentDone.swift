//
//	PaymentDone.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PaymentDone{

	var msg : String!
	var payMsg : String!
	var payStatus : Int!
	var roleStatus : Int!
	var status : Int!
    var orderId : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        
        orderId = json["ack_id"].stringValue
		msg = json["msg"].stringValue
		payMsg = json["pay_msg"].stringValue
		payStatus = json["pay_status"].intValue
		roleStatus = json["role_status"].intValue
		status = json["status"].intValue
	}

}
