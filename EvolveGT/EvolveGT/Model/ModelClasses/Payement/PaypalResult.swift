//
//	Paypal.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PaypalResult{

	var payerId : String!
	var paymentId : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		payerId = json["payerId"].stringValue
		paymentId = json["paymentId"].stringValue
	}

}
