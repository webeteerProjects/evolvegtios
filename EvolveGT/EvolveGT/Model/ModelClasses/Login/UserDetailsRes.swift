//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class UserDetailsRes{

	var msg : String!
	var result : UserDetails!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		msg = json["msg"].stringValue
		let resultJson = json["result"]
		if !resultJson.isEmpty{
			result = UserDetails(fromJson: resultJson)
		}
		status = json["status"].intValue
	}

}
