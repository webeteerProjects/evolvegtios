//
//	Result.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

struct UserDetails{

	var activationKey : String!
	var adminNotes : String!
	var billingAddress1 : String!
	var billingAddress2 : String!
	var billingCity : String!
	var billingCompany : String!
	var billingCountry : String!
    var billingCountryName : String!
	var billingEmail : String!
	var billingFirstName : String!
	var billingLastName : String!
	var billingPhone : String!
	var billingPostcode : String!
	var billingState : String!
    var billingStateName : String!

	var displayName : String!
	var email : String!
	var evDob : String!
	var evDobDay : String!
	var evDobMonth : String!
	var evDobYear : String!
	var evEmergencyFirstName : String!
	var evEmergencyLastName : String!
	var evEmergencyPhone : String!
	var evEmergencyRelationship : String!
	var evGender : String!
	var evMedicalConditions : String!
	var evMedications : String!
	var evMotocycleYear : String!
	var evMotorcycle : String!
	var evMotorcycleNumber : String!
	var evMotorcycleShiftPattern : String!
	var evRaceLicence : String!
	var evRole : String!
	var evStaff : String!
	var eventCancel : Bool!
	var everBeenTrack : String!
	var firstName : String!
	var hashCode : String!
	var lastName : String!
	var membershipExpDate : String!
	var memo : String!
	var n2Rider : String!
	var newsletter : String!
	var nicename : String!
	var nickname : String!
	var oldId : String!
	var password : String!
	var profileImage : String!
	var registered : String!
	var shippingAddress1 : String!
	var shippingAddress2 : String!
	var shippingCity : String!
	var shippingCompany : String!
	var shippingCountry : String!
    var shippingCountryName : String!

	var shippingFirstName : String!
	var shippingLastName : String!
	var shippingMethod : String!
	var shippingPostcode : String!
	var shippingState : String!
    var shippingStateName : String!

	var skillLevel : String!
	var status : String!
	var url : String!
	var userId : String!
	var username : String!
	var walletAmount : String!
    var profileImageUrl : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		activationKey = json["activation_key"].stringValue
		adminNotes = json["admin_notes"].stringValue
		billingAddress1 = json["billing_address_1"].stringValue
		billingAddress2 = json["billing_address_2"].stringValue
		billingCity = json["billing_city"].stringValue
		billingCompany = json["billing_company"].stringValue
		billingCountry = json["billing_country"].stringValue
        billingCountryName = json["billing_country_name"].stringValue
		billingEmail = json["billing_email"].stringValue
		billingFirstName = json["billing_first_name"].stringValue
		billingLastName = json["billing_last_name"].stringValue
		billingPhone = json["billing_phone"].stringValue
		billingPostcode = json["billing_postcode"].stringValue
		billingState = json["billing_state"].stringValue
        billingStateName = json["billing_state_name"].stringValue
		displayName = json["display_name"].stringValue
		email = json["email"].stringValue
		evDob = json["ev_dob"].stringValue
		evDobDay = json["ev_dob_day"].stringValue
		evDobMonth = json["ev_dob_month"].stringValue
		evDobYear = json["ev_dob_year"].stringValue
		evEmergencyFirstName = json["ev_emergency_first_name"].stringValue
		evEmergencyLastName = json["ev_emergency_last_name"].stringValue
		evEmergencyPhone = json["ev_emergency_phone"].stringValue
		evEmergencyRelationship = json["ev_emergency_relationship"].stringValue
		evGender = json["ev_gender"].stringValue
        if evGender.isValidString{
            evGender = evGender.capitalized
        }
		evMedicalConditions = json["ev_medical_conditions"].stringValue
		evMedications = json["ev_medications"].stringValue
		evMotocycleYear = json["ev_motocycle_year"].stringValue
		evMotorcycle = json["ev_motorcycle"].stringValue
		evMotorcycleNumber = json["ev_motorcycle_number"].stringValue
		evMotorcycleShiftPattern = json["ev_motorcycle_shift_pattern"].stringValue
		evRaceLicence = json["ev_race_licence"].stringValue
		evRole = json["ev_role"].stringValue
        if evRole.isValidString{
            UserInfo.currentUser()?.role = evRole
            UserInfo.currentUser()?.save()
        }
        
		evStaff = json["ev_staff"].stringValue
		eventCancel = json["event_cancel"].boolValue
		everBeenTrack = json["ever_been_track"].stringValue
		firstName = json["first_name"].stringValue
		hashCode = json["hash_code"].stringValue
		lastName = json["last_name"].stringValue
		membershipExpDate = json["membership_exp_date"].stringValue
		memo = json["memo"].stringValue
		n2Rider = json["n2_rider"].stringValue
		newsletter = json["newsletter"].stringValue
		nicename = json["nicename"].stringValue
		nickname = json["nickname"].stringValue
		oldId = json["old_id"].stringValue
		password = json["password"].stringValue
		profileImage = json["profile_image"].stringValue
		registered = json["registered"].stringValue
		shippingAddress1 = json["shipping_address_1"].stringValue
		shippingAddress2 = json["shipping_address_2"].stringValue
		shippingCity = json["shipping_city"].stringValue
		shippingCompany = json["shipping_company"].stringValue
		shippingCountry = json["shipping_country"].stringValue
        shippingCountryName = json["shipping_country_name"].stringValue
		shippingFirstName = json["shipping_first_name"].stringValue
		shippingLastName = json["shipping_last_name"].stringValue
		shippingMethod = json["shipping_method"].stringValue
		shippingPostcode = json["shipping_postcode"].stringValue
		shippingState = json["shipping_state"].stringValue
        shippingStateName = json["shipping_state_name"].stringValue
		skillLevel = json["skill_level"].stringValue
		status = json["status"].stringValue
		url = json["url"].stringValue
		userId = json["user_id"].stringValue
		username = json["username"].stringValue
		walletAmount = json["wallet_amount"].stringValue
        profileImageUrl = json["full_profile_image"].stringValue
	}

}

extension UserDetails: Encodable {
    enum CodingKeys: String, CodingKey {
        case activationKey = "activation_key"
        case adminNotes = "admin_notes"
        case billingAddress1 = "billing_address_1"
        case billingAddress2 = "billing_address_2"
        case billingCity = "billing_city"
        case billingCompany = "billing_company"
        case billingCountry = "billing_country"
        case billingCountryName = "billing_country_name"
        case billingEmail = "billing_email"
        case billingFirstName = "billing_first_name"
        case billingLastName = "billing_last_name"
        case billingPhone = "billing_phone"
        case billingPostcode = "billing_postcode"
        case billingState = "billing_state"
        case billingStateName = "billing_state_name"
        case displayName = "display_name"
        case email = "email"
        case evDob = "ev_dob"
        case evDobDay = "ev_dob_day"
        case evDobMonth = "ev_dob_month"
        case evDobYear = "ev_dob_year"
        case evEmergencyFirstName = "ev_emergency_first_name"
        case evEmergencyLastName = "ev_emergency_last_name"
        case evEmergencyPhone = "ev_emergency_phone"
        case evEmergencyRelationship = "ev_emergency_relationship"
        case evGender = "ev_gender"
        case evMedicalConditions = "ev_medical_conditions"
        case evMedications = "ev_medications"
        case evMotocycleYear = "ev_motocycle_year"
        case evMotorcycle = "ev_motorcycle"
        case evMotorcycleNumber = "ev_motorcycle_number"
        case evMotorcycleShiftPattern = "ev_motorcycle_shift_pattern"
        case evRaceLicence = "ev_race_licence"
        case evRole = "ev_role"
        case evStaff = "ev_staff"
        case eventCancel = "event_cancel"
        case everBeenTrack = "ever_been_track"
        case firstName = "first_name"
        case hashCode = "hash_code"
        case lastName = "last_name"
        case membershipExpDate = "membership_exp_date"
        case memo = "memo"
        case n2Rider = "n2_rider"
        case newsletter = "newsletter"
        case nicename = "nicename"
        case nickname = "nickname"
        case oldId = "old_id"
        case password = "password"
        case profileImage = "profile_image"
        case registered = "registered"
        case shippingAddress1 = "shipping_address_1"
        case shippingAddress2 = "shipping_address_2"
        case shippingCity = "shipping_city"
        case shippingCompany = "shipping_company"
        case shippingCountry = "shipping_country"
        case shippingCountryName = "shipping_country_name"
        case shippingFirstName = "shipping_first_name"
        case shippingLastName = "shipping_last_name"
        case shippingMethod = "shipping_method"
        case shippingPostcode = "shipping_postcode"
        case shippingState = "shipping_state"
        case shippingStateName = "shipping_state_name"
        case skillLevel = "skill_level"
        case status = "status"
        case url = "url"
        case userId = "user_id"
        case username = "username"
        case walletAmount = "wallet_amount"
        case profileImageUrl = "full_profile_image"
    }
}
