//
//  UserInfo.swift
//  FnB
//
//  Created by Ajeesh T S on 05/01/17.
//  Copyright © 2017 SimStream. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserInfo: NSObject,NSCoding {
    
    var errorCode : String?
    var errorMsg : String?
    var userID: String?
    var userPwd: String?
    var userEmail: String?
    var userName: String?
    var skillLevel: String?
    var displayName: String?
    var fName: String?
    var lName: String?
    var role: String?
    


    
    var status : Int?
    var isEnabledBiometricLogin : Int?
    
    var isUserAuthenticated = false
    
    var token : String?
        
    private static var __currentUser: UserInfo? = nil
    
    class func createSessionWith(json: JSON) {
        __currentUser = UserInfo(json: json)
    }
    
    class func createDummySession() {
        __currentUser = UserInfo()
        
    }
    
    class func currentUser() -> UserInfo? {
        return __currentUser
    }
    
    override init() {
        self.userID = ""
    }
    
    
    init(json: JSON) {
        if let data = json["token"].string {
            self.token = data
        }
        if let data = json["status"].int {
            self.status = data
        }

        let currentUserJson = json["currentUser"]
        if !currentUserJson.isEmpty{
            if let data = currentUserJson["first_name"].string {
                self.fName = data
            }
            if let data = currentUserJson["last_name"].string {
                self.lName = data
            }
            if let data = currentUserJson["display_name"].string {
                self.displayName = data
            }
            if let data = currentUserJson["id"].string {
                self.userID = data
            }
            if let data = currentUserJson["email"].string {
                self.userEmail = data
            }
            if let data = currentUserJson["role"].string {
                self.role = data
            }
            if let data = currentUserJson["skill_level"].string {
                self.skillLevel = data
            }
        }
        
       
    }
    
    //
    func save() {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: self)
        saveData(data: encodedData as NSData?)
    }
    
    func clearSession() {
        saveData(data: nil)
    }
    
    class func restoreSession() -> Bool {
        if let data = UserDefaults.standard.object(forKey: Constant.AppSetupConstant.KSavingUSerInfoUserDefaultKey) as? NSData {
            __currentUser = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? UserInfo
            AppSharedData.sharedInstance.isLoggedIn = true
        }else{
            __currentUser = nil
            AppSharedData.sharedInstance.isLoggedIn = false
        }
        return (__currentUser != nil)
    }
    
    private func saveData(data: NSData?) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey: Constant.AppSetupConstant.KSavingUSerInfoUserDefaultKey)
        userDefaults.synchronize()
    }
    
    func encode(with aCoder: NSCoder){
        if let value = errorCode {
            aCoder.encode(value, forKey: "errorCode")
        }
        if let value = errorMsg {
            aCoder.encode(value, forKey: "errorMsg")
        }
        if let value = userID {
            aCoder.encode(value, forKey: "userID")
        }
        //        if let value = isUserAuthenticated {
        aCoder.encode(isUserAuthenticated, forKey: "isUserAuthenticated")
        //        }
        if let value = token {
            aCoder.encode(value, forKey: "token")
        }
        if let value = userPwd {
            aCoder.encode(value, forKey: "userPwd")
        }
        if let value = userEmail {
            aCoder.encode(value, forKey: "userEmail")
        }
        if let value = userName {
            aCoder.encode(value, forKey: "userName")
        }
        if let value = displayName {
            aCoder.encode(value, forKey: "displayName")
        }
        if let value = skillLevel {
            aCoder.encode(value, forKey: "skillLevel")
        }
        if let value = status {
            aCoder.encode(value, forKey: "status")
        }
        if let value = isEnabledBiometricLogin {
            aCoder.encode(value, forKey: "isEnabledBiometricLogin")
        }
        if let value = fName {
            aCoder.encode(value, forKey: "fName")
        }
        if let value = lName {
            aCoder.encode(value, forKey: "lName")
        }
        if let value = role {
            aCoder.encode(value, forKey: "role")
        }
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        errorCode = aDecoder.decodeObject(forKey: "errorCode") as? String
        errorMsg = aDecoder.decodeObject(forKey: "errorMsg") as? String
        userID = aDecoder.decodeObject(forKey: "userID") as? String
        userPwd = aDecoder.decodeObject(forKey: "userPwd") as? String
        userEmail = aDecoder.decodeObject(forKey: "userEmail") as? String
        token = aDecoder.decodeObject(forKey: "token") as? String
        skillLevel = aDecoder.decodeObject(forKey: "skillLevel") as? String
        userName = aDecoder.decodeObject(forKey: "userName") as? String
        displayName = aDecoder.decodeObject(forKey: "displayName") as? String
        status = aDecoder.decodeInteger(forKey: "status")
        isEnabledBiometricLogin = aDecoder.decodeInteger(forKey: "isEnabledBiometricLogin")
        role = aDecoder.decodeObject(forKey: "role") as? String
        fName = aDecoder.decodeObject(forKey: "fName") as? String
        lName = aDecoder.decodeObject(forKey: "lName") as? String
    }
    
}


