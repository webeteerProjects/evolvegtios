//
//	Userdata.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Userdata{

	var email : String!
	var image : String!
	var name : String!
	var token : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		email = json["email"].stringValue
		image = json["image"].stringValue
		name = json["name"].stringValue
		token = json["token"].stringValue
	}

}
