//
//  File.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 30/06/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import Foundation
import SwiftyJSON

class NotificationType {
    var id: String = ""
    var type: String = ""
    var status: Bool = false
    
    init(fromJson json: JSON!) {
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        type = json["type"].stringValue
        status = json["status"].boolValue
    }

    func notificationPreference() -> NotificationPreference {
        let pref = NotificationPreference()
        pref.userId = (UserInfo.currentUser()?.userID)!
        let updateType = NotificationUpdateType()
        updateType.status = status ? "1" : "0"
        updateType.notificationType = id
        pref.preference = [updateType]
        return pref
    }
}

class NotificationPreference: Encodable {
    var userId: String = ""
    var preference: [NotificationUpdateType] = []
    
    enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case preference = "preferences"
    }
}

class NotificationUpdateType: Encodable {
    var notificationType: String = ""
    var status: String = ""
    
    enum CodingKeys: String, CodingKey {
        case notificationType = "notification_type"
        case status = "status"
    }
}
