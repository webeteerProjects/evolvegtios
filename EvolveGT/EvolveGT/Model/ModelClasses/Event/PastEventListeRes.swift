//
//	PastEventListeRes.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PastEventListeRes{

	var events : [EventInfo]!
	var msg : String!
	var pastEventCount : Int!
	var pastEventCurrentYear : [EventInfo]!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		events = [EventInfo]()
		let eventsArray = json["events"].arrayValue
		for eventsJson in eventsArray{
			let value = EventInfo(fromJson: eventsJson)
			events.append(value)
		}
		msg = json["msg"].stringValue
		pastEventCount = json["pastEventCount"].intValue
		pastEventCurrentYear = [EventInfo]()
		let pastEventCurrentYearArray = json["pastEventCurrentYear"].arrayValue
		for pastEventCurrentYearJson in pastEventCurrentYearArray{
			let value = EventInfo(fromJson: pastEventCurrentYearJson)
			pastEventCurrentYear.append(value)
		}
		status = json["status"].intValue
	}

}


class AllEventListeRes{
    
    var events : [EventInfo]!
    var msg : String!
    var allEventCount : Int!
    var status : Int!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        events = [EventInfo]()
        let eventsArray = json["events"].arrayValue
        for eventsJson in eventsArray{
            let value = EventInfo(fromJson: eventsJson)
            events.append(value)
        }
        msg = json["msg"].stringValue
        allEventCount = json["allEventCount"].intValue
        status = json["status"].intValue
    }
    
}
