//
//	Result.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON
import AFDateHelper

class Result{

	var couponBasedPrice : String!
	var couponCode : String!
	var evType : String!
	var eventDate : String!
	var eventId : String!
	var eventImage : String!
	var eventLogo : String!
	var eventType : String!
	var fullEventLogo : String!
	var isCouponEnabled : String!
	var maxQuantityOfE1People : String!
	var maxQuantityOfE2People : String!
	var maxQuantityOfE3People : String!
	var maxQuantityOfE4People : String!
	var maxQuantityOfGt1People : String!
	var price : String!
	var rolePrice : RolePrice!
	var roleBasedPrice : String!
	var slug : String!
	var title : String!
    var monthYear: String!
    var trainingType: [String]?
    var eventDateDMY: String?
    var monthValue:Int = 10000
    var eventDateValue: Date!
    var dateEvt: Date = Date()

    
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		couponBasedPrice = json["coupon_based_price"].stringValue
		couponCode = json["coupon_code"].stringValue
		evType = json["ev_type"].stringValue
		eventDate = json["event_date"].stringValue
        if eventDate.isValidString{
            let eventdateVal = Date(fromString: eventDate, format: .isoDate)
            if eventdateVal != nil{
                self.dateEvt = eventdateVal!
            }
            self.eventDateValue = eventdateVal
            let string = eventdateVal?.toString(format: .custom("MMM YYYY"))
            monthYear = string
            
            let anotherFormatString = eventdateVal?.toString(format: .custom("dd MMM YYYY"))
            eventDateDMY = anotherFormatString
            if let monthStr = eventdateVal?.toString(format: .custom("MM")){
                let monthVal: String = monthStr
                monthValue = Int(monthVal) ?? 10000
//                print(monthStr)
//                print(monthVal)
//                print(monthValue)
            }else{
                monthValue = 10000
            }
        }
        eventId = json["event_id"].stringValue
		eventImage = json["event_image"].stringValue
		eventLogo = json["event_logo"].stringValue
		eventType = json["event_type"].stringValue
		fullEventLogo = json["full_event_logo"].stringValue
		isCouponEnabled = json["is_coupon_enabled"].stringValue
		maxQuantityOfE1People = json["max_quantity_of_e1_people"].stringValue
		maxQuantityOfE2People = json["max_quantity_of_e2_people"].stringValue
		maxQuantityOfE3People = json["max_quantity_of_e3_people"].stringValue
		maxQuantityOfE4People = json["max_quantity_of_e4_people"].stringValue
		maxQuantityOfGt1People = json["max_quantity_of_gt1_people"].stringValue
		price = json["price"].stringValue
		let rolePriceJson = json["rolePrice"]
		if !rolePriceJson.isEmpty{
			rolePrice = RolePrice(fromJson: rolePriceJson)
		}
		roleBasedPrice = json["role_based_price"].stringValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
        trainingType = json["training_type"].arrayObject as? [String]
	}

}
