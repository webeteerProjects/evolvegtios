//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class EventListRes{

	var msg : String!
	var results : [Result]!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		msg = json["msg"].stringValue
		results = [Result]()
        var keyValue = "results"
        if (UserInfo.currentUser()?.role ==  "administrator"){
            keyValue = "result"
            if AppSharedData.sharedInstance.isCurrentSwitchModuleUser == true{
                keyValue = "results"
            }
        }else{
            if AppSharedData.sharedInstance.isCurrentSwitchModuleAdmin == true{
                keyValue = "result"
            }else{
                
            }
        }
		let resultsArray = json[keyValue].arrayValue
		for resultsJson in resultsArray{
			let value = Result(fromJson: resultsJson)
			results.append(value)
		}
		status = json["status"].intValue
	}

}
