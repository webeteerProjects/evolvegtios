//
//	CreditHistory.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CreditHistory{

	var amount : String!
	var descriptionField : String!
	var isCredit : String!
	var mode : String!
	var orderId : String!
	var paymentLogId : String!
	var postDate : String!
	var postModified : String!
	var userId : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		amount = json["amount"].stringValue
		descriptionField = json["description"].stringValue
		isCredit = json["is_credit"].stringValue
		mode = json["mode"].stringValue
		orderId = json["order_id"].stringValue
		paymentLogId = json["payment_log_id"].stringValue
		postDate = json["post_date"].stringValue
		postModified = json["post_modified"].stringValue
		userId = json["user_id"].stringValue
	}

}