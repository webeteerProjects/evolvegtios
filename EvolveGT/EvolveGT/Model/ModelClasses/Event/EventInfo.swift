//
//	PastEvent.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON
import AFDateHelper

class EventInfo{

	var eventDate : String!
	var eventImage : String!
	var eventMonth : String!
	var orderDate : String!
	var orderItemId : String!
	var orderStatus : String!
	var productName : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		eventDate = json["event_date"].stringValue
        if eventDate.isValidString{
            let joinDate = Date(fromString: eventDate, format: .custom("yyyy-MM-dd"))
            let string = joinDate?.toString(format: .custom("dd MMM yyyy"))
            eventDate = string
        }
		eventImage = json["event_image"].stringValue
		eventMonth = json["event_month"].stringValue
		orderDate = json["order_date"].stringValue
        if orderDate.isValidString{
            let joinDate = Date(fromString: orderDate, format: .custom("yyyy-MM-dd HH:mm:ss"))
            let string = joinDate?.toString(format: .custom("dd MMM YYYY"))
            orderDate = string
        }
		orderItemId = json["order_item_id"].stringValue
		orderStatus = json["order_status"].stringValue
		productName = json["product_name"].stringValue
	}

}
