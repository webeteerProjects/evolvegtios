//
//	EventSummary.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class EventSummary{

	var creditHistory : [CreditHistory]!
	var pastEventCurrentYear : [PastEventCurrentYear]!
	var pastEvents : [EventInfo]!
	var upcomingEvents : [EventInfo]!
    var creditHistoryCount : Int!
    var pastEventCount : Int!
    var pastEventCurrentYearCount : Int!
    var upcomingEventCount : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        creditHistoryCount = json["creditHistoryCount"].intValue
        pastEventCount = json["pastEventCount"].intValue
        pastEventCurrentYearCount = json["pastEventCurrentYearCount"].intValue
        upcomingEventCount = json["upcomingEventCount"].intValue

		creditHistory = [CreditHistory]()
		let creditHistoryArray = json["creditHistory"].arrayValue
		for creditHistoryJson in creditHistoryArray{
			let value = CreditHistory(fromJson: creditHistoryJson)
			creditHistory.append(value)
		}
		pastEventCurrentYear = [PastEventCurrentYear]()
		let pastEventCurrentYearArray = json["pastEventCurrentYear"].arrayValue
		for pastEventCurrentYearJson in pastEventCurrentYearArray{
			let value = PastEventCurrentYear(fromJson: pastEventCurrentYearJson)
			pastEventCurrentYear.append(value)
		}
		pastEvents = [EventInfo]()
		let pastEventsArray = json["pastEvents"].arrayValue
		for pastEventsJson in pastEventsArray{
			let value = EventInfo(fromJson: pastEventsJson)
			pastEvents.append(value)
		}
		upcomingEvents = [EventInfo]()
		let upcomingEventsArray = json["upcomingEvents"].arrayValue
		for upcomingEventsJson in upcomingEventsArray{
            let value = EventInfo(fromJson: upcomingEventsJson)
            upcomingEvents.append(value)
		}
	}

}
