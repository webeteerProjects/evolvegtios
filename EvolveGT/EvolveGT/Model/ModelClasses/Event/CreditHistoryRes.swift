//
//	CreditHistoryRes.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CreditHistoryRes{

	var msg : String!
	var result : [CreditHistory]!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		msg = json["msg"].stringValue
		result = [CreditHistory]()
		let resultArray = json["result"].arrayValue
		for resultJson in resultArray{
			let value = CreditHistory(fromJson: resultJson)
			result.append(value)
		}
		status = json["status"].intValue
	}

}
