//
//	RolePrice.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class RolePrice{

	var apex : Apex!
	var coach : Apex!
	var dealer : Apex!
	var grip : Apex!
	var guest : Apex!
	var military : Apex!
	var motogirl : Apex!
	var nycr : Apex!
	var racer : Apex!
	var vip : Apex!
	var yg : Apex!
    var currentUserTypePrice : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let apexJson = json["apex"]
		if !apexJson.isEmpty{
			apex = Apex(fromJson: apexJson)
		}
		let coachJson = json["coach"]
		if !coachJson.isEmpty{
			coach = Apex(fromJson: coachJson)
		}
		let dealerJson = json["dealer"]
		if !dealerJson.isEmpty{
			dealer = Apex(fromJson: dealerJson)
		}
		let gripJson = json["grip"]
		if !gripJson.isEmpty{
			grip = Apex(fromJson: gripJson)
		}
		let guestJson = json["guest"]
		if !guestJson.isEmpty{
			guest = Apex(fromJson: guestJson)
		}
		let militaryJson = json["military"]
		if !militaryJson.isEmpty{
			military = Apex(fromJson: militaryJson)
		}
		let motogirlJson = json["motogirl"]
		if !motogirlJson.isEmpty{
			motogirl = Apex(fromJson: motogirlJson)
		}
		let nycrJson = json["nycr"]
		if !nycrJson.isEmpty{
			nycr = Apex(fromJson: nycrJson)
		}
		let racerJson = json["racer"]
		if !racerJson.isEmpty{
			racer = Apex(fromJson: racerJson)
		}
		let vipJson = json["vip"]
		if !vipJson.isEmpty{
			vip = Apex(fromJson: vipJson)
		}
		let ygJson = json["yg"]
		if !ygJson.isEmpty{
			yg = Apex(fromJson: ygJson)
		}
        
        if let userType = UserInfo.currentUser()?.role{
            let racerJson1 = json[userType]
            if !racerJson1.isEmpty{
                if let tmpVal = racerJson1["regular_price"].string{
//                    let totalPriceStr = String(format: "%.2f", tmpVal)
                    currentUserTypePrice = tmpVal
                }
            }else{
                
            }
        }else{
            if let tmpVal = guest.regularPrice{
//                let totalPriceStr = String(format: "%.2f", tmpVal)
                currentUserTypePrice = tmpVal
            }
        }
	}

}
