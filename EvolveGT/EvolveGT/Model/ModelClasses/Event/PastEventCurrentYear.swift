//
//	PastEventCurrentYear.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PastEventCurrentYear{

	var eventDate : String!
	var eventImage : String!
	var eventMonth : String!
	var orderDate : String!
	var orderItemId : String!
	var productName : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		eventDate = json["event_date"].stringValue
		eventImage = json["event_image"].stringValue
		eventMonth = json["event_month"].stringValue
		orderDate = json["order_date"].stringValue
		orderItemId = json["order_item_id"].stringValue
		productName = json["product_name"].stringValue
	}

}