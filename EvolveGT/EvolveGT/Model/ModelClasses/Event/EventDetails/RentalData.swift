//
//	RentalData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class RentalData{

	var image : String!
	var isVariant : String!
	var price : String!
	var productId : String!
	var slug : String!
	var title : String!
	var variations : [Variation]!
    var isSelected = false


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		image = json["image"].stringValue
		isVariant = json["is_variant"].stringValue
		price = json["price"].stringValue
		productId = json["product_id"].stringValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
		variations = [Variation]()
		let variationsArray = json["variations"].arrayValue
		for variationsJson in variationsArray{
			let value = Variation(fromJson: variationsJson)
			variations.append(value)
		}
	}

}
