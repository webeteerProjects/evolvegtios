//
//	RoleBasedPrice.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class RoleBasedPrice{

	var apex : String!
	var coach : String!
	var dealer : String!
	var grip : String!
	var guest : String!
	var military : String!
	var motogirl : Int!
	var ocp : Int!
	var racer : Int!
	var vip : Int!
	var yg : String!
    var currentUserTypePrice : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		apex = json["apex"].stringValue
		coach = json["coach"].stringValue
		dealer = json["dealer"].stringValue
		grip = json["grip"].stringValue
		guest = json["guest"].stringValue
		military = json["military"].stringValue
		motogirl = json["motogirl"].intValue
		ocp = json["ocp"].intValue
		racer = json["racer"].intValue
		vip = json["vip"].intValue
		yg = json["yg"].stringValue
        if let userType = UserInfo.currentUser()?.role{
            currentUserTypePrice =  json[userType].stringValue
        }else{
            currentUserTypePrice = guest
        }
        
	}

}
