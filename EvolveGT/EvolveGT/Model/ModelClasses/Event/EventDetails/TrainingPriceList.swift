//
//	TrainingPriceList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class TrainingPriceList{

	var youvsyoulevel1 :TrainingInfo!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let youvsyoulevel1Json = json["you-vs-you-level-1"]
		if !youvsyoulevel1Json.isEmpty{
			youvsyoulevel1 = TrainingInfo(fromJson: youvsyoulevel1Json)
		}
	}

}
