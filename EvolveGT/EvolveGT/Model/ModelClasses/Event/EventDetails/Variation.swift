//
//	Variation.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Variation{

	var price : String!
	var size : String!
	var stockStatus : String!
    var isSelected = false

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		price = json["price"].stringValue
		size = json["size"].stringValue
		stockStatus = json["stock_status"].stringValue
	}

}
