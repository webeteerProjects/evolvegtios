//
//	TrainingData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class TrainingData{

	var image : String!
	var price : String!
	var slug : String!
	var title : String!
	var trainingId : String!
    var isSelected = false


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		image = json["image"].stringValue
		price = json["price"].stringValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
		trainingId = json["training_id"].stringValue
	}

}
