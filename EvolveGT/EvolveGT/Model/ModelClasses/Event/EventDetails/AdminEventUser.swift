//
//  AdminEventUser.swift
//  EvolveGT
//
//  Created by Ajeesh T S on 16/07/19.
//  Copyright © 2019 com.dev.evolve. All rights reserved.
//

import Foundation
import SwiftyJSON
import AFDateHelper

class AdminEventUser {
    var signId: String?
    var hasSign: Bool?
    var fourSeries: String?
    var renewOnTen: String?
    var signed: String?
    var updated: String?
    var status: String?
    var title: String?
    var displayName: String!
    var email: String?
    var skillLevel: String?
    var userId: String?
    var userRole: String?
    var eventId: String?
    var role: String?
    var training: [String]?
    var evDOB: String?
    var orderId: String?
    var dobFormatted: String?
    var rentals : [Rental]!
    var isSignEnabled: String?
    
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        rentals = [Rental]()
        let rentalsArray = json["rentals"].arrayValue
        for rentalsJson in rentalsArray{
            let value = Rental(fromJson: rentalsJson)
            rentals.append(value)
        }
        signId = json["signature_id"].stringValue
        hasSign = json["signature"].boolValue
        fourSeries = json["four_series"].stringValue
        renewOnTen = json["renew_on_ten"].stringValue
        signed = ""//json["signed"].stringValue
        updated = json["updated"].stringValue
        status = json["status"].stringValue
        title = json["title"].stringValue
        displayName = json["display_name"].stringValue
        email = json["email"].stringValue
        skillLevel = json["skill_level"].stringValue
        userId = json["user_id"].stringValue
        userRole = json["user_role"].stringValue
        eventId = json["event_id"].stringValue
        role = json["role"].stringValue
        training = json["training"].arrayObject as? [String]
        evDOB = json["ev_dob"].stringValue
        orderId = json["order_id"].stringValue
        isSignEnabled = json["sign_enabled"].stringValue
        if evDOB?.isValidString ?? false {
            let eventdateVal = Date(fromString: evDOB!, format: .isoDate)
            let string = eventdateVal?.toString(format: .custom("dd MMM YYYY"))
            dobFormatted = string
        }
    }
}


class AdminEventUserList {
    
    var msg : String!
    var results : [AdminEventUser]!
    var status : Int!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty {
            return
        }
        msg = json["msg"].stringValue
        results = [AdminEventUser]()
        let resultsArray = json["result"].arrayValue
        for resultsJson in resultsArray {
            let value = AdminEventUser(fromJson: resultsJson)
            results.append(value)
        }
        status = json["status"].intValue
    }
    
}
