//
//	EventDetails.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class EventDetails{

	var couponBasedPrice : String!
	var couponCode : String!
	var eventBanner : String!
	var eventDate : String!
	var eventId : String!
	var eventImage : String!
	var eventType : String!
	var haveRentalItems : String!
	var isCouponEnabled : String!
	var isRoleBasedPricing : String!
	var logoIcon : String!
	var maxQuantityOfE1People : String!
	var maxQuantityOfE2People : String!
	var maxQuantityOfE3People : String!
	var maxQuantityOfE4People : String!
	var maxQuantityOfGt1People : String!
	var msg : String!
	var price : String!
	var productInfo : String!
	var rentalData : [RentalData]!
	var rentalEnable : Bool!
	var rentalMsg : String!
	var rentalStatus : Int!
	var roleBasedPrice : RoleBasedPrice!
	var slug : String!
	var status : Int!
	var title : String!
	var trainingData : [TrainingData]!
	var trainingMsg : String!
	var trainingPriceList : TrainingPriceList!
	var trainingStatus : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		couponBasedPrice = json["coupon_based_price"].stringValue
		couponCode = json["coupon_code"].stringValue
		eventBanner = json["event_banner"].stringValue
		eventDate = json["event_date"].stringValue
		eventId = json["event_id"].stringValue
		eventImage = json["event_image"].stringValue
		eventType = json["event_type"].stringValue
		haveRentalItems = json["have_rental_items"].stringValue
		isCouponEnabled = json["is_coupon_enabled"].stringValue
		isRoleBasedPricing = json["is_role_based_pricing"].stringValue
		logoIcon = json["logo_icon"].stringValue
		maxQuantityOfE1People = json["max_quantity_of_e1_people"].stringValue
		maxQuantityOfE2People = json["max_quantity_of_e2_people"].stringValue
		maxQuantityOfE3People = json["max_quantity_of_e3_people"].stringValue
		maxQuantityOfE4People = json["max_quantity_of_e4_people"].stringValue
		maxQuantityOfGt1People = json["max_quantity_of_gt1_people"].stringValue
		msg = json["msg"].stringValue
		price = json["price"].stringValue
		productInfo = json["product_info"].stringValue
		rentalData = [RentalData]()
		let rentalDataArray = json["rentalData"].arrayValue
		for rentalDataJson in rentalDataArray{
			let value = RentalData(fromJson: rentalDataJson)
			rentalData.append(value)
		}
		rentalEnable = json["rentalEnable"].boolValue
		rentalMsg = json["rentalMsg"].stringValue
		rentalStatus = json["rentalStatus"].intValue
		let roleBasedPriceJson = json["roleBasedPrice"]
		if !roleBasedPriceJson.isEmpty{
			roleBasedPrice = RoleBasedPrice(fromJson: roleBasedPriceJson)
		}
		slug = json["slug"].stringValue
		status = json["status"].intValue
		title = json["title"].stringValue
		trainingData = [TrainingData]()
		let trainingDataArray = json["trainingData"].arrayValue
		for trainingDataJson in trainingDataArray{
			let value = TrainingData(fromJson: trainingDataJson)
			trainingData.append(value)
		}
		trainingMsg = json["trainingMsg"].stringValue
		let trainingPriceListJson = json["trainingPriceList"]
		if !trainingPriceListJson.isEmpty{
			trainingPriceList = TrainingPriceList(fromJson: trainingPriceListJson)
		}
		trainingStatus = json["trainingStatus"].intValue
	}

}
