//
//	You-vs-you-level-1.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class TrainingInfo{

	var id : String!
	var image : String!
	var price : String!
	var slug : String!
    var isSelected = false


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		id = json["id"].stringValue
		image = json["image"].stringValue
		price = json["price"].stringValue
		slug = json["slug"].stringValue
	}

}
