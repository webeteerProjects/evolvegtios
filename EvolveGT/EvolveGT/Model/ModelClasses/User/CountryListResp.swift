//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CountryListResp{

	var country : [Country]!
	var msg : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		country = [Country]()
		let countryArray = json["result"].arrayValue
		for countryJson in countryArray{
			let value = Country(fromJson: countryJson)
			country.append(value)
		}
		msg = json["msg"].stringValue
		status = json["status"].intValue
	}
    

}
