//
//	Country.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Country{

	var country : String!
	var countryId : String!
	var value : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		country = json["country"].stringValue
		countryId = json["country_id"].stringValue
		value = json["value"].stringValue
	}

}