//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CityListResp{

	var city : [City]!
	var msg : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		city = [City]()
		let cityArray = json["result"].arrayValue
		for cityJson in cityArray{
			let value = City(fromJson: cityJson)
			city.append(value)
		}
		msg = json["msg"].stringValue
		status = json["status"].intValue
	}

}
