//
//	CartResponse.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CartResponse{

	var data : [CartItem]!
	var formStatus : Int!
	var msg : String!
	var status : Int!
	var wallet : String!
    var walletEnabled : Bool!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		data = [CartItem]()
		let dataArray = json["data"].arrayValue
		for dataJson in dataArray{
			let value = CartItem(fromJson: dataJson)
			data.append(value)
		}
		formStatus = json["form_status"].intValue
		msg = json["msg"].stringValue
		status = json["status"].intValue
		wallet = json["wallet"].stringValue
        walletEnabled = false
        let tmp = json["walletEnabled"].intValue
        if tmp == 1{
            walletEnabled = true
        }
        
	}

}
