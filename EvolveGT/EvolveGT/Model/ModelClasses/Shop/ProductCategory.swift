//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ProductCategory{

    var children : [CategoryItems]!
	var id : String!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        children = [CategoryItems]()
        let childrenArray = json["children"].arrayValue
        for childrenJson in childrenArray{
            let value = CategoryItems(fromJson: childrenJson)
            children.append(value)
        }
		id = json["id"].stringValue
		title = json["title"].stringValue
	}

}

class CategoryItems{
    
    //    var children : [AnyObject]!
    var id : String!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        title = json["title"].stringValue
    }
    
}


