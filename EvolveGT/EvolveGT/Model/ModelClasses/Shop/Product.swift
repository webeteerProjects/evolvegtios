//
//	Result.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Product{

	var categoryId : String!
	var image : String!
	var productId : String!
	var slug : String!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		categoryId = json["category_id"].stringValue
		image = json["image"].stringValue
		productId = json["product_id"].stringValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
	}

}
