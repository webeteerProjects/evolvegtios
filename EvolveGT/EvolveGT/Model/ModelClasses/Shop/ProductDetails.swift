//
//	ProductDetialsRes.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ProductDetails{

	var attribute : [Attribute]!
	var categoryId : String!
	var image : String!
	var isRental : String!
	var isVariant : String!
	var msg : String!
	var price : String!
	var productId : String!
	var slug : String!
	var status : Int!
	var title : String!
	var variations : [ProductVariation]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		attribute = [Attribute]()
		let attributeArray = json["attribute"].arrayValue
		for attributeJson in attributeArray{
			let value = Attribute(fromJson: attributeJson)
			attribute.append(value)
		}
		categoryId = json["category_id"].stringValue
		image = json["image"].stringValue
		isRental = json["is_rental"].stringValue
		isVariant = json["is_variant"].stringValue
		msg = json["msg"].stringValue
		price = json["price"].stringValue
		productId = json["product_id"].stringValue
		slug = json["slug"].stringValue
		status = json["status"].intValue
		title = json["title"].stringValue
		variations = [ProductVariation]()
		let variationsArray = json["variations"].arrayValue
		for variationsJson in variationsArray{
			let value = ProductVariation(fromJson: variationsJson)
			variations.append(value)
		}
	}

}
