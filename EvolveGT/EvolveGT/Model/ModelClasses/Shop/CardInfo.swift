//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CardInfo{

	var content : String!
	var image : String!
	var price : String!
	var slug : String!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		content = json["content"].stringValue
		image = json["image"].stringValue
		price = json["price"].stringValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
	}
    
    init() {
        content = ""
        image = ""
        price = ""
        slug = ""
        title = ""
        
    }

}
