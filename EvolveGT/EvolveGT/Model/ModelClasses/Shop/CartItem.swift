//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON
import AFDateHelper

let k_lateFee : Float = 15.0

class CartItem{

	var cartId : String!
	var evtype : String!
	var feeAmount : String!
	var image : String!
	var objectId : String!
	var parentId : String!
    var parentSlug : String!
	var parentTitle : String!
	var postDate : String!
	var postModified : String!
	var price : String!
	var quantity : String!
	var slug : String!
	var source : String!
	var stockStatus : String!
	var title : String!
	var userId : String!
    var unAttributes : [GiftcardDetails]!
    var totalAmount :Float = 0.0



	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		cartId = json["cart_id"].stringValue
		evtype = json["evtype"].stringValue
		feeAmount = json["fee_amount"].stringValue
		image = json["image"].stringValue
		objectId = json["object_id"].stringValue
		parentId = json["parent_id"].stringValue
        parentSlug = json["parent_slug"].stringValue
		parentTitle = json["parent_title"].stringValue
		postDate = json["post_date"].stringValue
		postModified = json["post_modified"].stringValue
		price = json["price"].stringValue
		quantity = json["quantity"].stringValue
		slug = json["slug"].stringValue
		source = json["source"].stringValue
		stockStatus = json["stock_status"].stringValue
		title = json["title"].stringValue
		userId = json["user_id"].stringValue
        unAttributes = [GiftcardDetails]()
        let unAttributesArray = json["un_attributes"].arrayValue
        for unAttributesJson in unAttributesArray{
            let value = GiftcardDetails(fromJson: unAttributesJson)
            unAttributes.append(value)
        }
        if let feeVal = Float(self.feeAmount ?? "0.0o"){
            if feeVal > 0 {
                if let priceVal = Float(self.price ?? "0.00"){
                    totalAmount =  priceVal + feeVal
                }
            }else{
                if let priceVal = Float(self.price ?? "0.00"){
                    totalAmount =  priceVal
                }
            }
        }
        
//        if let dateStr = postDate{
//            let joinDate = Date(fromString: dateStr, format: .custom("yyyy-MM-dd HH:mm:ss"))
//            let today = Date()
//            let nodays = joinDate?.since(today, in: .day)
//            if let daysre = nodays{
//                if daysre < 15 {
//                    lateFee = "15.0"
//                }
//            }
//        }
        

	}

}

class GiftcardDetails{
    
    var email : String!
    var name : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        email = json["email"].stringValue
        name = json["name"].stringValue
        let sizeStr = json["size"].stringValue
        if sizeStr.isValidString{
            name = sizeStr
        }
            
    

    }
    
}
