//
//	Attribute.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Attribute{

	var title : String!
	var values : [String]!
    var selectedValue : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		title = json["title"].stringValue
		values = [String]()
		let valuesArray = json["values"].arrayValue
		for valuesJson in valuesArray{
			values.append(valuesJson.stringValue)
		}
	}

}
