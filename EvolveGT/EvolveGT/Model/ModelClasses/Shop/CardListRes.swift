//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CardListRes{

	var data : [CardInfo]!
	var msg : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		data = [CardInfo]()
		let dataArray = json["data"].arrayValue
		for dataJson in dataArray{
			let value = CardInfo(fromJson: dataJson)
			data.append(value)
		}
		msg = json["msg"].stringValue
		status = json["status"].intValue
	}

}
