//
//	ProductListRes.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ProductListRes{

	var msg : String!
	var results : [Product]!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		msg = json["msg"].stringValue
		results = [Product]()
		let resultsArray = json["results"].arrayValue
		for resultsJson in resultsArray{
			let value = Product(fromJson: resultsJson)
			results.append(value)
		}
		status = json["status"].intValue
	}

}
