//
//	Variation.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ProductVariation{

	var price : String!
	var stockStatus : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		price = json["price"].stringValue
		stockStatus = json["stock_status"].stringValue
	}

}
