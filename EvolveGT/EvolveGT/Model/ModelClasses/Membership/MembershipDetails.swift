//
//	Membership.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class MembershipDetails{

	var descriptionField : String!
	var image : String!
	var membershipId : String!
	var oldPostId : String!
	var packages : String!
	var postAuthor : String!
	var postDate : String!
	var postModified : String!
	var postStatus : String!
	var price : String!
	var slug : String!
	var stockStatus : String!
	var title : String!
    var stockStatusStr: String?
    var memberShipIdValue:Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        stockStatusStr = json["stock_status"].string
		descriptionField = json["description"].stringValue
		image = json["image"].stringValue
		membershipId = json["membership_id"].stringValue
		oldPostId = json["old_post_id"].stringValue
		packages = json["packages"].stringValue
		postAuthor = json["post_author"].stringValue
		postDate = json["post_date"].stringValue
		postModified = json["post_modified"].stringValue
		postStatus = json["post_status"].stringValue
		price = json["price"].stringValue
		slug = json["slug"].stringValue
		stockStatus = json["stock_status"].stringValue
		title = json["title"].stringValue
        memberShipIdValue = Int(membershipId) ?? 0
	}

}
