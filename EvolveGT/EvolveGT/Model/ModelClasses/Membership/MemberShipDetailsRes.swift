//
//	MemberShipDetailsRes.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class MemberShipDetailsRes{

	var membership : MembershipDetails!
	var msg : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let membershipJson = json["membership"]
		if !membershipJson.isEmpty{
			membership = MembershipDetails(fromJson: membershipJson)
		}
		msg = json["msg"].stringValue
		status = json["status"].intValue
	}

}
