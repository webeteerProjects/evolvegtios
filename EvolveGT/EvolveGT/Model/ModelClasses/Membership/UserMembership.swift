//
//	UserMembership.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class UserMembership{

	var membership : String!
	var membershipId : String!
	var msg : String!
	var status : Int!
    var memberShipIdValue:Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		membership = json["membership"].stringValue
		membershipId = json["membership_id"].stringValue
		msg = json["msg"].stringValue
		status = json["status"].intValue
        memberShipIdValue = Int(membershipId) ?? 0
	}

}
