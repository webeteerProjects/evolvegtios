//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class MembershipListRes{

	var memberships : [Membership]!
	var msg : String!
	var season : String!
	var status : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		memberships = [Membership]()
		let membershipsArray = json["memberships"].arrayValue
		for membershipsJson in membershipsArray{
			let value = Membership(fromJson: membershipsJson)
			memberships.append(value)
		}
		msg = json["msg"].stringValue
		season = json["season"].stringValue
		status = json["status"].intValue
	}

}
