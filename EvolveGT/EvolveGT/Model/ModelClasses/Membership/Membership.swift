//
//	Membership.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Membership{

	var image : String!
	var price : String!
	var slug : String!
	var stockStatus : String!
	var title : String!
    var membershipId : String!
    var memberShipIdValue:Int!



	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		image = json["image"].stringValue
		price = json["price"].stringValue
		slug = json["slug"].stringValue
		stockStatus = json["stock_status"].stringValue
		title = json["title"].stringValue
        membershipId = json["membership_id"].stringValue
        memberShipIdValue = Int(membershipId) ?? 0
	}

}
