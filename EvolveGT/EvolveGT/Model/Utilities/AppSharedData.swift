//
//  AppSharedData.swift
//  FnB
//
//  Created by Ajeesh T S on 06/01/17.
//  Copyright © 2017 SimStream. All rights reserved.
//

import UIKit
import Reachability


class AppSharedData: NSObject {
    static let sharedInstance = AppSharedData()
    var reachability = Reachability()!
    var cartCount = 0
    var email: String?
    var password: String?
    
    var isReachableInternet : Bool = true
    var isRefreshSideMenu : Bool = false
    var isLoggedIn : Bool = false
    var isRefreshNews = false
    var isRefreshEvents = false
    var isConfiguredFireBase = false
    
    
    var selectedSort = ""
    var selectedFilter = ""
    var selectedTabIndex = 0
    var pushcount = 0
    var toatlPrice : Float = 0.0
    var userDetails: UserDetails?
    var country : [Country]?
    var state : [City]!
    var isCurrentSwitchModuleUser = false
    var isCurrentSwitchModuleAdmin = false

    private override init() {
        
    }
    
    //    class var sharedInstance : AppSharedData {
    //        return self.sharedInstance
    //    }
    
    func stopRechabilityNotfication(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: Notification.Name.reachabilityChanged,
                                                  object: reachability)
    }
    
    func startReachabilityNotification(){
        //        if self.reachability == nil{
        //            self.reachability = Reachability()
        //        }
        if reachability.connection != .none {
            self.isReachableInternet = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: Notification.Name.reachabilityChanged,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: NSNotification) {
        let reachability = note.object as! Reachability
        if reachability.connection == .none {
            self.isReachableInternet = false
            print("Network not reachable")
        }else{
            //            NotificationCenter.default.post(name: Notification.Name.reachabilityChanged, object: nil)
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                self.isReachableInternet = true
            } else {
                self.isReachableInternet = true
                print("Reachable via Cellular")
            }
        }
    }
    
    
    
}

