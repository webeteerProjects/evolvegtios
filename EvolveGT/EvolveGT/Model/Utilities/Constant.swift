//
//  Constant.swift
//  FnB
//
//  Created by Apple01 on 2/14/17.
//  Copyright © 2017 SimStream. All rights reserved.
//

import Foundation
import UIKit



struct Constant {
    
    
    enum ProductListType{
        case NormalProductList
        case FeautredProduct
        case ShowCaseProduct
        case WhatsNew
        case WhatsTrending
        case WhatsCelling
        case VendorsProducts
        case myVendorDealProduct
        case DealProduct
    }

    
    struct AppSetupConstant {
        static let KSavingUSerInfoUserDefaultKey = "UserSession"
        static let KAppName = "Evolve GT"
    }
    
    struct TosterMsg {
        static let KNoPhoneNumber = "Phone number not available"
        static let KNoLocationCoordinates = "Location not available"

    }
    
    struct DeviceToken {
        static let KUserDeviceTokenKey = "UserDeviceToken"
    }
    
    /// This is an **Notification** Constant *Notification Names*.
    
    struct NotificationName {
        static let KLogoutNotification = Notification.Name("UserDoneLoggout")
        static let KLoggedInNotification =  Notification.Name("UserLoggedInNow")
        static let KSideMenuNotificationName = Notification.Name("SideMenuNotificationForPushVC")
    }
    
    /// This is an **StoryBoard** Constant *Story board identifier*.
    
    struct StoryBoard {
        static let KDropdownIdentifier = "Login&SignUp"
        static let KLoginStoryBoardName = "Login&SignUp"
    }
    
    /// This is an **ScreenSize** Constant.
    
    struct ScreenSize{
        
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    /// This is an **TableView** empty *count*.
    
    
    struct TableView{
        static let EmptyCount = 0
    }
    
    /// This is an **WebService** Constant *default values*.
    
    struct UserDependencyInWebService {
        static let KDefaultCustomerID = "0"
        static let KDefaultCustomerType = "customer"
    }
    
    /// This is an **ProductListWebService** Constant *User Relates*.
    
    struct ProductListWebService {
        static let KProductListPaginationCount = "25"
        static let KProductCategoryDefault = "0"
        static let KProductCategoryWhatsNew = "4"
        static let KProductCategoryWhatsSelling = "2"
        static let KProductCategoryWhatsTrending = "3"
        static let KProductCategoryShowCase = "5"
        static let KProductCategoryTodaysDeal = "999"
        static let KReviewListPaginationCount = "3"

    }
    
    /// This is an **Product** Constant *Product related*.
    
    struct Product{
        static let KQuantityInputLimit = 3
    }
    
    
    struct ResponseKey{
        
        static let KCustomerType = "customerType"
        static let KCustomerId = "customerId"
        static let kData = "data"
        
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DEVICE TYPE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct DeviceType
    {
        
        static let IS_IPHONE_5s = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        
        static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        
        static let IS_IPHONE_6p = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        
        static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
        static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad

        
    }
    
    
  
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ COMMON UICOLORS USED~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct Color {
        
        static let APP_THEME_Title = UIColor(red:0.33, green:0.33, blue:0.33, alpha:1)
            //UIColor(red:0.38, green:0.38, blue:0.39, alpha:1)
        
        static let UI_COLOR_LOGINBTN_BORDER = UIColor(red:0.55, green:0.55, blue:0.56, alpha:1.00).cgColor
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~COMMON ALERT MESSAGE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct Alert {
        
        static let KPromptSomeThingWentWrong = "Something went wrong"

        static let KPromptMsgEnterWishlistName = "Please Enter Wishlist Name"
        static let KPromptMsgEnterEmployeeCode = "Please Enter Employee Code"
        static let KPromptMsgEnterAreaCode = "Please Enter Area Code"
        static let KPromptMsgEnterAreaName = "Please Enter Area Name"
        static let KPromptMsgEnterAreaManagerName = "Please Enter Area Manager Name"
        
        static let KPromptMsgEnterEmail = "Please Enter Email"
        static let KPromptMsgEnterValidEmail = "Please Enter Valid Email"
        static let KPromptMsgEnterPassword = "Please Enter Password"
        static let KPromptMsgEnterAddress = "Please Enter Address"
        static let KPromptMsgEnterCity = "Please Enter City"
        static let KPromptMsgEnterCountry = "Please Enter Country"
        static let KPromptMsgEnterZipcode = "Please Enter Zipcode"
        static let KPromptMsgEnterFirstName = "Please Enter First Name"
        static let KPromptMsgEnterLastName = "Please Enter Last Name"
        static let KPromptMsgEnterLandmark = "Please Enter Landmark"
        static let KPromptMsgEnterState = "Please Enter State"
        static let KPromptMsgEnterValidPassword = "Password should be atleast 6 characters"
        static let KPromptMsgEnterMobileNumber = "Please Enter Mobile Number"
        static let KPromptMsgEnterValidMobileNumber = "Please Enter 10 digit Mobile Number"
        static let KPromptMsgSelectVendorReview = "Please Fill All the feilds"
        
         static let KPromptMsgEnterMessage = "Please enter the message"
         static let KPromptServerConectError = "Server connection error"

        
        
        
        
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MANAGER CELL ORDER~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct ManagerCellOrder{
        
        static let KTextViewCell = "TextViewCell"
        static let KPermissionCell = "PermissionCell"
        static let KSwitchCell = "SwitchCell"
    }
    
    
    
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CATEGORY~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct Category {
        
        static let KsubcategoryStoryboardId = "subCategoryScene"
        
        static  let KinnerCategoryStorybordId = "innerCategoryScene"
        
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ WISHLIST ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    
    struct Wishlist {
        
        static let KWishlistCell = "mywishlistCell"
        static let KCreateWishlistScene = "CreateWishlistScene"
        static let KWishlistItemScene = "WishlistItemScene"
        static let KmyWishlistMainScene = "MyWishListMainScene"
    }
    
    enum wishlistType {
        case EditWishlist
        case CreateWishlist
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  MANAGER  LIST ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct ManagerList{
        
        static let KManagerListCell = "managerListCell"
        static let KManagerListStoryboardID = "managerListSceneID"
        static let KStorybordName = "ManagerListing"
        
        
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ AREA MANAGER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct AreaManager{
        
        static let KAreanManagerCell = "areaManagerCell"
        static let KAreaManagerEditAndAddScene = "areaManagerAddAndEditScene"
        
        static let KAreaManagerListID = "AreaManagerListSecne"
        
        static let KStorybordName = "AreaManager"
        static let KMainNavigationID = "AreaManagerNavigation"
        
        static let AreaManagerListJsonKey = "areaList"
        
        static let KPermissionJsonKey = "allPermissions"
        
        
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BRANCH MANAGER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct BranchManager{
        
        static let KStorybordName = "BranchManager"
        static let KMainNavigationID = ""
        
        static let KBranchListID = "branchMangerListScene"
        static let KBranchCreateID  = "branchManagerAdd&EditScene"
        
        static let KListCellID = "listCellID"
        
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ STAFF MANAGER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct StaffManager{
        
        static let KStorybordName = "StaffManager"
        static let KMainNavigationID = ""
        
        static let KStaffList = "StaffManagerViewControllerListScene"
        
        static let KStaffManagerCreateID = "staffManagerAdd&EditScene"
        
        static let KListCellID = "listCellID"
        static let KStaffListJsonKey = "staffList"
        
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ STAFFROLE MANAGER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    
    struct CustomTabelCell{
        static let KSideMenuCellID = "SideMenuCell"
        static let KRewardsCellID = "RewardsCell"

        static let KMoreCellID = "moreCell"
        static let KEventCellID = "EventCell"
        static let KProfileCellID = "UserProfileCell"
        static let KLoggedInProfileCellID = "LoggedInUserProfileCell"
        static let KSubcribedHeaderCellID = "SubcribedHeaderCell"
        static let KAddOrgBtnCellID = "AddOrgBtnCell"
        static let KRecenlyAddOrgBtnCellID = "RecentlyAddedHeaderCell"
        static let KRecenlyAddOrgCellID = "RecentlyAddedOrgCell"
        static let KOrgTypeCellID = "OrganisationTypeTableViewCell"
        static let KOrgDetailedImageCellID = "ImageCell"
        static let KOrgDetailedOverViewCellID = "OverViewCell"
        static let KOrgDetailedConsentCellID = "ConsentCell"
        static let KOrgDetailedConsentHeaderCellID = "HeaderCell"
        static let KOrgDetailedConsentAllowCellID = "AllowCell"
        static let KOrgDetailedConsentDisAllowCellID = "DisallowCell"
        static let KOrgDetailedConsentAskMeCellID = "AskMeCell"
        static let KOrgSuggestionCellID = "SuggestionCell"


        
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MY VENDOR ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    struct MyVendor {
        
        static let KMyVendorCell = "myVendorCell"
        static let KProductCell = "myvendorProductCell"
        static let kProductListScene = "productListSceneID"
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SIDEMENU ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    struct Sidemenu {
        
        static let KSidemenuCell = "sidemenuCellId"
        static let KSidemenuProfileCell = "ProfileCell"
        
        
    }
    
    /// This is an **Order** Constant *identifier*.
    
    struct Order {
        static let KOrderListCellIdentifier = "ListCell"
        static let KOrderListJsonKey = "orderList"
        static let KOrderDetailListCellIdentifier = "DetailsListCell"
        static let KOrderDetailOrderInfoCellIdentifier = "OrderInfoCell"
        static let KOrderDetailVendorInfoCellIdentifier = "VendorDetailsCell"
        static let KOrderDetailBuyerInfoCellIdentifier = "BuyerDetailsCell"
        static let KOrderDetailShippingInfoCellIdentifier = "ShippingAddressCell"
        static let KOrderDetailBillingInfoCellIdentifier = "BillingAddressCell"

        static let KOrderDetailPaymentsInfoCellIdentifier = "PeymentInfoCell"
        static let KOrderDetailOrderedItemHeaderCellIdentifier = "OrderedHeaderCell"
        static let KOrderDetailTotalCostCellIdentifier = "TotalCostnfoCell"
        static let KOrderItemCellIdentifier = "ItemDetailsCell"

        


    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MY ADDRESS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    struct MyAddress {
        
        static let KMyAddressCell = "MyAddressListCell"
        
        static let KStorybordName = "MyAddress"
        
        static let KMyAddresCreateID  = "myAddressAddAndEditScene"
        
        static let KMyAddressListID = "MyAddressListScene"
    }
    
    struct SideMenuTitle{
        static let KMyAccount = "My Account"
        static let KMyOrder = "My Order"
        static let KMyReviewsAndRatings = "MyReviews And Ratings"
        static let KQuotes = "Quotes"
        static let KManage = "Management"
        static let KHelpDesk = "Help Desk"
        static let KTodaysDeal = "Todays Deal"
        static let KSignOut = "SignOut"
        static let KContactUs = "Contact Us"
        static let KAboutUs = "About Us"
        static let KSettings = "Settings"
        static let KTermsAndConditions = "Terms And Conditions"
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------//
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MY ORDER LIST ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //------------------------------------------------------------------------------------------------------------------------------------//
    struct MyOrderList {
        
        static let KMyOrderListPaginationCount = "5"
    }
}
