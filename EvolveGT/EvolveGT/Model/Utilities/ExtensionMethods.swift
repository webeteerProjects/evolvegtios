
//
//  ExtensionMethods.swift
//  FnB
//
//  Created by Ajeesh T S on 06/01/17.
//  Copyright © 2017 SimStream. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftEntryKit

//import Toaster

let internetAalertMsg = "The Internet connection appears to be offline."
let serviceError = "Failed Operation!!!"


extension UIApplication {
    class func topViewControllerInApp(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(base: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(base: presented)
        }
        return controller
    }
}

extension UIViewController {
   
    func showfloatingAlert(message: String){
        var attributes = EKAttributes.topFloat
        attributes.entryBackground = .color(color: UIColor(red:0.19, green:0.23, blue:0.29, alpha:1))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
        attributes.statusBar = .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.positionConstraints.maxSize = .init(width: .constant(value: Constant.ScreenSize.SCREEN_WIDTH), height: .intrinsic)
//        let cFont = UIFont(name: "System", size: 14)
        let cFont = UIFont.systemFont(ofSize: 18, weight: .regular)
        let title = EKProperty.LabelContent(text: Constant.AppSetupConstant.KAppName, style: .init(font:cFont, color: UIColor.white))
        let description = EKProperty.LabelContent(text: message, style: .init(font: cFont, color: UIColor.white))
        let image = EKProperty.ImageContent(image: UIImage(named: "close")!, size: CGSize(width: 35, height: 35))
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    
    func showWhiteTitleNavigationBar(){
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont(name: "OpenSans", size: 18)!
        ]
    }
    
    func showBlackTitleNavigationBar(){
        navigationController?.navigationBar.tintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "OpenSans", size: 18)!
        ]
    }
    
    func showAlert(title: String?, message: String?) {
        let alerController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alerController.addAction(cancelAction)
        present(alerController, animated: true, completion: nil)
    }
    
    func showAlertwithTitle(title: String, message: String) {
        let alerController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alerController.addAction(cancelAction)
        present(alerController, animated: true, completion: nil)
    }
    
    func showWarningAlert(message: String){
        self.showAlertwithTitle(title: Constant.AppSetupConstant.KAppName, message: message)
    }
    
    func addLoadingIndicator(){
        DispatchQueue.main.async(execute: { () -> Void in
            MBProgressHUD.showAdded(to: self.view, animated: true)

        })
    }

    func removeLoadingIndicator(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func showErrorAlert(message: String){
        self.showAlertwithTitle(title: Constant.AppSetupConstant.KAppName, message: message)
    }
    
    func showSucessAlert(message: String){
        self.showAlertwithTitle(title: Constant.AppSetupConstant.KAppName, message: message)
    }
    
    func showNoPermissionAlertWithMessage(message: String){
        self.showAlertwithTitle(title: Constant.AppSetupConstant.KAppName, message: message)
    }
    
    func showNoPermissionAlert(){
        self.showAlertwithTitle(title: Constant.AppSetupConstant.KAppName, message: "You dont have permission")
    }

    
    
    func checkNetWorkAvailability() -> Bool{
        if AppSharedData.sharedInstance.isReachableInternet{
            return true
        }else{
            self.showAlertwithTitle(title: Constant.AppSetupConstant.KAppName, message: internetAalertMsg)
            return false
        }
    }
    
    var isNetWorkAvailable: Bool {
        get {
            if AppSharedData.sharedInstance.isReachableInternet{
                return true
            }else{
                self.showAlertwithTitle(title: Constant.AppSetupConstant.KAppName, message: internetAalertMsg)
                return false
            }
        }
    }
//

    var isLoggedInUser: Bool {
        if AppSharedData.sharedInstance.isReachableInternet{
            if UserInfo.currentUser()?.token != nil{
                return true
//                else if UserInfo.currentUser()?.userStatus == .OTPVerificationPending{
//                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Login&SignUp", bundle: nil)
//                     let optVC  = mainStoryboard.instantiateViewController(withIdentifier: "OTPVC") as! OTPViewController
//                    optVC.isShowingAsPresent = true
//                    let navController = UINavigationController(rootViewController: optVC)
//                    self.present(navController, animated: true, completion: nil)
//                    return false
//                }
            }else{
                let loginStoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let loginVC = loginStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                let nav = UINavigationController.init(rootViewController: loginVC )
                loginVC.isGuestUserLogin = true
                self.present(nav, animated: true, completion: nil)
                return false
            }
        }else{
            self.showAlertwithTitle(title: Constant.AppSetupConstant.KAppName, message: internetAalertMsg)
            return false
        }
    }
    
 
//
//    var isLoggedInUserWithoutLoginPromt: Bool {
//        if AppSharedData.sharedInstance.isReachableInternet{
//            if UserInfo.currentUser()?.customerID != nil{
//                if UserInfo.currentUser()?.userStatus == .Verfied{
//                    return true
//                }else{
//                    return false
//                }
//            }else{
//                return false
//            }
//        }else{
//            return false
//        }
//    }
//    
//    var isLoggedInUserWithoutReachablity: Bool {
//            if UserInfo.currentUser()?.customerID != nil{
//                if UserInfo.currentUser()?.userStatus == .Verfied{
//                    return true
//                }else{
//                    return false
//                }
//            }else{
//                return false
//            }
//        
//    }
//
//    
//    func checkNetWorkAvailabilityAndUserAuthentication() -> Bool{
//        if AppSharedData.sharedInstance.isReachableInternet{
//            if UserInfo.currentUser()?.customerID != nil{
//                return true
//            }else{
//                /*
//                 let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                 let loginVC  = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
//                 //                loginVC.isDirectLogin = true
//                 let navController = UINavigationController(rootViewController: loginVC)
//                 self.present(navController, animated: true, completion: nil)
//                 */
//                return false
//            }
//        }else{
//            self.showAlertwithTitle(title: "FnB!", message: internetAalertMsg)
//            return false
//        }
//    }
//    
    func presentShareView(content: AnyObject){
        // text to share
//        let text = "This is some text that I want to share."
//        
        // set up activity view controller
        let textToShare = [ content ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: [])
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        self.present(activityViewController, animated: true, completion: nil)

    }
    
   

}

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}


extension UIView {
    var cornerRadius:CGFloat! {
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
        get {
            return layer.cornerRadius
        }
    }
    
    
    
    var borderWidth:CGFloat! {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    var borderColor:UIColor! {
        set {
            layer.borderColor = newValue.cgColor
        }
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}


extension UITextView{
    func adjustUITextViewHeight() {
        self.translatesAutoresizingMaskIntoConstraints = true
        self.sizeToFit()
        self.isScrollEnabled = false
    }
}

extension UIColor{
    class func getThemColor() -> UIColor{
       return UIColor(red:1, green:0.28, blue:0.09, alpha:1)
    }
}


extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension String {
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    var isValidString: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return !trimmed.isEmpty
        }
    }
    
    func trimSpace() -> String? {
        let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
        return trimmed
    }
    
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
    var isValidEmail: Bool{
        get {
            
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            let result = emailTest.evaluate(with: self)
            return result
        }
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
}

extension UIFont {
    static func printFontNames() {
        for familyName in UIFont.familyNames {
            print("Family name: \(familyName)")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print("Font name: \(fontName)")
            }
        }
    }
}

extension UIButton{
 
    func showThemeBorder(){
        self.layer.cornerRadius = 0
        self.layer.borderWidth = 0.5
        self.layer.borderColor =     UIColor(red:0.47, green:0.48, blue:0.48, alpha:1).cgColor
    }
    
}
extension UIView{
    
    func showRoundCorner(roundCorner: CGFloat){
        self.layer.cornerRadius = roundCorner
        self.clipsToBounds = true
    }
    
    func showRoundCorner(){
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }
    
    func showRoundBorder(){
        self.clipsToBounds = true
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 3.0
    }
    
    
}

extension UIViewController {
    
    func setTabBarVisible(visible: Bool, animated: Bool) {
        //* This cannot be called before viewDidLayoutSubviews(), because the frame is not set before this time
        
        // bail if the current state matches the desired state
        if (isTabBarVisible == visible) { return }
        
        // get a frame calculation ready
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
        
        // zero duration means no animation
        let duration: TimeInterval = (animated ? 0.3 : 0.0)
        
        //  animate the tabBar
        if frame != nil {
            UIView.animate(withDuration: duration) {
                self.tabBarController?.tabBar.frame = frame!.offsetBy(dx: 0, dy: offsetY!)
                return
            }
        }
    }
    
    var isTabBarVisible: Bool {
        return (self.tabBarController?.tabBar.frame.origin.y ?? 0) < self.view.frame.maxY
    }
}

extension NSAttributedString {
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        return boundingBox.width
    }
}

extension NSMutableAttributedString{
//    func strikeTextWithColor(text:String, color:UIColor) -> NSAttributedString{
//        let strikeValueRange = NSMakeRange(0,text.characters.count)
//        self.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: strikeValueRange)
//        self.addAttribute(NSForegroundColorAttributeName, value: color, range:strikeValueRange)
//        return self
//    }
}

extension UIColor {
    class func themeBlueColor() -> UIColor{
        return UIColor(red:0, green:0.2, blue:0.55, alpha:1)
    }
    
    class func themeTextBlackColor() -> UIColor{
        return UIColor(red:0.46, green:0.46, blue:0.47, alpha:1)
    }
    
    class func offerPriceColor() -> UIColor{
        return UIColor(red:0.63, green:0.63, blue:0.63, alpha:1)
    }
    
    class func addressColor() -> UIColor{
        return UIColor(red:113/255, green:113/255, blue:113/255, alpha:1)
    }
    
    class func greyPlaceholderColor() -> UIColor {
        return UIColor(red: 0.78, green: 0.78, blue: 0.80, alpha: 1.0)
    }
}

extension UIFont{
    func themeFontRomanWithSize(fontSize : CGFloat) -> UIFont {
        return UIFont.init(name: "Avenir-Roman", size: fontSize)!
    }
}


extension UIColor{
    struct AppThemeColors {
        static let AppBaseThemseRedColor =  UIColor(red:0.99, green:0.29, blue:0, alpha:1)
    }
    
}


class ExtensionMethods: NSObject {
    class func themeColor() -> UIColor {
        return UIColor(red:0.52, green:0.79, blue:0.07, alpha:1)
    }
    
    class func themeTextColor() -> UIColor {
        return UIColor(red:0.11, green:0.11, blue:0.11, alpha:1)
    }

    class func themeBorderColor() -> UIColor{
        return UIColor(red:0.8, green:0.8, blue:0.8, alpha:1)
    }
}

extension UILabel {
    convenience init(badgeText: String, color: UIColor = UIColor.red, fontSize: CGFloat = UIFont.smallSystemFontSize) {
        self.init()
        text = " \(badgeText) "
        textColor = UIColor.white
        backgroundColor = color
        
        font = UIFont.systemFont(ofSize: fontSize)
        layer.cornerRadius = fontSize * CGFloat(0.6)
        clipsToBounds = true
        
        translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: self, attribute: .height, multiplier: 1, constant: 0))
    }
}

extension UIBarButtonItem {
    convenience init(badge: String?, button: UIButton, target: AnyObject?, action: Selector) {
        button.addTarget(target, action: action, for: .touchUpInside)
        button.sizeToFit()
        
        let badgeLabel = UILabel(badgeText: badge ?? "")
        button.addSubview(badgeLabel)
        button.addConstraint(NSLayoutConstraint(item: badgeLabel, attribute: .top, relatedBy: .equal, toItem: button, attribute: .top, multiplier: 1, constant: 0))
        button.addConstraint(NSLayoutConstraint(item: badgeLabel, attribute: .centerX, relatedBy: .equal, toItem: button, attribute: .trailing, multiplier: 1, constant: 0))
        if nil == badge {
            badgeLabel.isHidden = true
        }
        badgeLabel.tag = UIBarButtonItem.badgeTag
        self.init(customView: button)
    }
    
    convenience init(badge: String?, image: UIImage, target: AnyObject?, action: Selector) {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y:0, width:image.size.width, height:image.size.height)
        button.setBackgroundImage(image, for: .normal)
        
        self.init(badge: badge, button: button, target: target, action: action)
    }
    
    convenience init(badge: String?, title: String, target: AnyObject?, action: Selector) {
        let button = UIButton(type: .system)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: UIFont.buttonFontSize)
        
        self.init(badge: badge, button: button, target: target, action: action)
    }
    
    var badgeLabel: UILabel? {
        return customView?.viewWithTag(UIBarButtonItem.badgeTag) as? UILabel
    }
    
    
    var badgedButton: UIButton? {
        return customView as? UIButton
    }
    
    var badgeString: String? {
        get { return badgeLabel?.text?.trimmingCharacters(in: NSCharacterSet.whitespaces) }
        set {
            if let badgeLabel = badgeLabel {
                badgeLabel.text = nil == newValue ? nil : " \(newValue!) "
                badgeLabel.sizeToFit()
                badgeLabel.isHidden = nil == newValue
            }
        }
    }
    
    var badgedTitle: String? {
        get { return badgedButton?.title(for: .normal) }
        set { badgedButton?.setTitle(newValue, for: .normal); badgedButton?.sizeToFit() }
    }
    
    
    
    private static let badgeTag = 7373
}

extension UIImage {
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in PNG format
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the PNG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
//    var png: Data? { return UIImagePNGRepresentation(self) }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
//    func jpeg(_ quality: JPEGQuality) -> Data? {
//        return UIImageJPEGRepresentation(self, quality.rawValue)
//    }
    
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    
}

extension Notification.Name {
    static let consentChange = Notification.Name(
        rawValue: "ConsentUpdated")
}

extension UILabel {
    func from(html: String) {
        if let htmlData = html.data(using: String.Encoding.unicode) {
            do {
                let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Chalkduster", size: 18.0)! ]
                self.attributedText = try?  NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)

            } catch let e as NSError {
//                print("Couldn't parse \(html): \(e.localizedDescription)")
            }
        }
    }
}

extension UILabel {
    func setHTMLFromString(text: String) {
        let modifiedFont = NSString(format:"<span style=\"font-family: \(self.font!.fontName); font-size: \(self.font!.pointSize); color: #737477 \">%@</span>" as NSString, text)
        
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        self.attributedText = attrStr
    }
}

extension UINavigationBar {
    private struct AssociatedKeys {
        static var overlayKey = "overlayKey"
    }
    
    var overlay: UIView? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.overlayKey) as? UIView
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.overlayKey, newValue as UIView?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC
            )
        }
    }
}


extension UINavigationBar {
    
    func lt_setBackgroundColor(backgroundColor: UIColor) {
        if overlay == nil {
            self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            overlay = UIView.init(frame: CGRect.init(x: 0, y: 0, width: bounds.width, height: bounds.height+20))
            overlay?.isUserInteractionEnabled = false
            overlay?.autoresizingMask = UIView.AutoresizingMask.flexibleWidth
            subviews.first?.insertSubview(overlay!, at: 0)
        }
        overlay?.backgroundColor = backgroundColor
    }
    
    
    func lt_setTranslationY(translationY: CGFloat) {
        transform = CGAffineTransform.init(translationX: 0, y: translationY)
    }
    
    
    func lt_setElementsAlpha(alpha: CGFloat) {
        for (_, element) in subviews.enumerated() {
            if element.isKind(of: NSClassFromString("UINavigationItemView") as! UIView.Type) ||
                element.isKind(of: NSClassFromString("UINavigationButton") as! UIButton.Type) ||
                element.isKind(of: NSClassFromString("UINavBarPrompt") as! UIView.Type)
            {
                element.alpha = alpha
            }
            
            if element.isKind(of: NSClassFromString("_UINavigationBarBackIndicatorView") as! UIView.Type) {
                element.alpha = element.alpha == 0 ? 0 : alpha
            }
        }
        
        items?.forEach({ (item) in
            if let titleView = item.titleView {
                titleView.alpha = alpha
            }
            for BBItems in [item.leftBarButtonItems, item.rightBarButtonItems] {
                BBItems?.forEach({ (barButtonItem) in
                    if let customView = barButtonItem.customView {
                        customView.alpha = alpha
                    }
                })
            }
        })
    }
    
    
    func lt_reset() {
        setBackgroundImage(nil, for: UIBarMetrics.default)
        overlay?.removeFromSuperview()
        overlay = nil
    }
}

extension UIViewController {
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}
extension UIImage {
    
    func fixedOrientation() -> UIImage? {
        
        guard imageOrientation != UIImage.Orientation.up else {
            //This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            //CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil //Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}
